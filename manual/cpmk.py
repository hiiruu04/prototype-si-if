from matkul.models import CPMK, Kurikulum

def create_cpmk_all():
    list_cpmk = text.splitlines()exi
    for i in list_cpmk:
        CPMK.objects.create(
            kurikulum = Kurikulum.objects.get(tahun=2017),
            cpmk = i
        )

def delete_all():
    CPMK.objects.delete(kurikulum=Kurikulum.objects.get(tahun=2017))


text = '''Merancang model matematika formal sederhana suatu situasi nyata dan menerapkan model tersebut dalam suatu simulasi
Menerapkan teknik kriptografi yang tepat pada kasus tertentu
Menerapkan konsep pemrograman Algoritma Evolusi dan menerapkan paling tidak salah satu metode tersebut dalam pemrograman untuk penyelesaian masalah
Mengimplementasikan algoritma klasifikasi yang membagi persepsi masukan ke kategori keluaran dan mengevaluasi hasil klasifikasi tersebut secara kuantitatif
Mengimplementasikan algoritma perencanaan gerak (motion planning) dasar dalam ruang konfigurasi robot Mengintegrasikan sensor, aktuator, dan perangkat lunak dalam robot yang dirancang untuk mengerjakan tugas tertentu
Menerapkan metode dan teknik dalam bioinformatika dalam aplikasi sederhana
Mengimplementasikan pengenalan objek 2 dimensi berdasarkan representasi bentuk berbasis kontur dan atau berbasis region
Menganalisis berbagai metode pencarian heuristik beserta penerapannya dalam kasus tertentu
Menerapkan algoritma klasik dan stokastik untuk pemrosesan bahasa alami
Menganalisis strategi pencarian alternatif beserta alasan kesesuaian strategi pencarian tertentu untuk suatu aplikasi
Menganalisis perkembangan penerapan komputasi, grafika, dan sistem cerdas dalam penelitian bidang-bidang terkait isu terkini
Menerapkan teknik dan piranti audit sistem informasi
Merancang program sederhana untuk penerapan sistem tertanam
Mengevaluasi metodologi yang berbeda untuk penerapan penambangan data secara efektif
Mengaplikasikan berbagai metode untuk mengembangkan estimasi keandalan sistem perangkat lunak
Membuat aplikasi yang menggunakan infrastruktur awan untuk sumber daya komputasi dan atau data
Mengidentifikasi elemen data, informasi, dan pengetahuan dan organisasi terkait untuk penerapan sains komputasi
Merancang dan mengimplementasikan sistem penyimpanan dan pemerolehan informasi atau perpustakaan digital
Menerapkan refactoring dalam proses memodifikasi komponen perangkat lunak
Mengidentifikasi metode yang menerapkan arsitektur perangkat lunak dengan capaian tingkat keandalan tertentu
Menerapkan teknik analisis dan spesifikasi pada perancangan perangkat lunak dan program dengan kompleksitas rendah
Menganalisis perkembangan perangkat lunak, sistem dan teknologi informasi terkini
Menunjukkan sikap religius dan bertaqwa terhadap Tuhan Yang Maha Esa 
Menerapkan nilai kemanusiaan dalam menjalankan tugas berdasarkan agama, moral, dan etika
Berkontribusi dalam peningkatan mutu kehidupan bermasyarakat, berbangsa, bernegara, dan kemajuan peradaban berdasarkan Pancasila
Mendemonstrasikan kemampuan komunikasi yang berkaitan dengan aspek teknis dan non-teknis untuk menganalisis informasi ilmiah dan non-imiah secara mandiri dan kritis
Beradaptasi terhadap situasi yang dihadapi dan menangani berbagai kegiatan secara simultan pada berbagai kondisi
Menginternalisasi semangat kemandirian, kejuangan, dan kewirausahaan
Menerapkan sikap belajar sepanjang hayat (life-long learning)
Bekerja sama dengan individu yang memiliki latar belakang sosial dan budaya yang beragam
Memahami konsep kalkulus untuk menyelesaikan persoalan dengan berpikir analitis
Memahami metode kuantitatif untuk pemodelan dan penyelesaian persoalan
Memahami konsep aljabar untuk penyelesaian persoalan terkait pengolahan matriks data dan grafika komputer
Mengaplikasikan konsep dan teori struktur diskrit, yang meliputi materi dasar matematika yang digunakan untuk memodelkan dan menganalisis sistem komputasi
Mengaplikasikan konsep matematika untuk memecahkan berbagai masalah yang berkaitan dengan logika
Mengaplikasikan konsep probabilitas dan statistika untuk mendukung dan menganalisis sistem komputasi
Mengaplikasikan prinsip metode numerik untuk pemecahan masalah komputasi
Menerapkan paradigma fungsional dalam membangun program sederhana untuk menyelesaikan persoalan komputasi
Memahami konsep-konsep bahasa pemrograman, mengidentikasi model-model bahasa pemrograman, serta membandingkan berbagai solusi
Menganalisis  menggunakan pemrograman berorientasi objek untuk menyelesaikan masalah suatu sistem berbasis komputer secara efisien 
Menerapkan konsep abstraksi dan struktur data dalam merancang program komputer
Menganalisis algoritma dan kompleksitas, meliputi konsep-konsep sentral dan kecakapan yang dibutuhkan untuk merancang, menerapkan dan menganalisis algoritma untuk menyelesaikan masalah
Memahami konsep dan prinsip algoritma serta teori ilmu komputer yang dapat digunakan dalam pemodelan dan desain sistem berbasis komputer
Menerapkan konsep-konsep yang berkaitan dengan pengembangan berbasis platform serta mampu mengembangkan program aplikasi berbasis platform untuk berbagai area
Mengaplikasikan pengetahuan yang dimiliki berkaitan dengan konsep-konsep dasar pengembangan perangkat lunak dan kecakapan yang berhubungan dengan proses pengembangan perangkat lunak, serta mampu membuat program untuk meningkatkan efektivitas penggunaan komputer untuk memecahkan masalah tertentu
Membangun aplikasi perangkat lunak yang berkaitan dengan pengetahuan ilmu komputer sesuai standar internasional
Menganalisis proyek perangkat lunak berdasarkan metodologi pengembangan sistem, yaitu perencanaan, desain, penerapan, pengujian dan pemeliharaan sistem sesuai standar internasional
Menerapkan prinsip dan strategi pengujian untuk menjamin produk perangkat lunak bermutu tinggi
Menerapkan konsep-konsep yang berkaitan dengan arsitektur dan organisasi komputer serta memanfaatkannya untuk menunjang aplikasi komputer
Menganalisis sistem serta prosedur yang berkaitan dengan sistem komputer serta memberikan rekomendasi yang berkaitan dengan sistem komputer yang lebih efisien dan efektif
Mengaplikasikan sistem jaringan komputer dan sistem keamanannya serta melakukan pengelolaan secara kontinu terhadap proteksi profil yang ada
Memahami teori dan konsep yang mendasari ilmu komputer terkait dengan infrastuktur sistem
Memahami teori dasar arsitektur komputer, termasuk perangkat keras komputer dan jaringan terkait pemrosesan tersebar dan paralel
Mengaplikasikan model data konseptual dan fisik menggunakan metode dan teknik yang tepat sesuai persoalan
Menerapkan konsep yang berkaitan dengan manajemen informasi, termasuk menyusun pemodelan dan abstraksi data serta membangun aplikasi perangkat lunak untuk pengorganisasian data dan penjaminan keamanan akses data
Menerapkan solusi manajemen informasi yang tepat dengan pertimbangan rancangan yang relevan, meliputi kemelaran (scalability), keterjangkauan, dan kegunaan
Menerapkan konsep perlindungan dan pertahanan informasi dan sistem informasi serta konsep penjaminan keadaan proses dan data pada masa kini dan masa sebelumnya
Mengaplikasikan metode sistem cerdas dalam membangun aplikasi perangkat lunak dalam berbagai area yang berkaitan dengan bidang robotika, multimedia, dan bahasa natural
Mengevaluasi kinerja dari penerapan sistem cerdas yang sesuai dengan problem yang dihadapi, termasuk dalam pemilihan representasi pengetahuan  dan mekanisme penalarannya.
Mengevaluasi perangkat lunak dalam berbagai area, termasuk yang berkaitan dengan interaksi antara manusia dan komputer
Merancang program aplikasi untuk memanipulasi dan memvisualisasikan model grafis
Mendemonstrasikan kemampuan komunikasi yang berkaitan dengan aspek teknis dan non-teknis untuk menganalisis informasi ilmiah dan non-imiah secara mandiri dan kritis dalam bahasa internasional
Menerapkan integritas profesional dan berkomitmen terhadap nilai-nilai etika
Menerapkan sikap kepemimpinan dan kerja sama dalam tim secara mandiri dan bertanggung jawab terhadap pekerjaannya
Memahami bidang fokus pengetahuan ilmu komputer untuk dapat beradaptasi dengan perkembangan ilmu pengetahuan dan teknologi
Mengevaluasi akar masalah dan pemecahannya secara komprehensif, serta mengambil keputusan yang tepat berdasarkan analisis informasi dan data'''