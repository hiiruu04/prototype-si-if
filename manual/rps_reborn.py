from rps.models import RPS
from matkul.models import Matkul

def reborn():
    RPS.objects.filter(matkul__kurikulum__tahun=2017).delete()
    matkul_list = Matkul.objects.filter(kurikulum__tahun=2017)  
    for m in matkul_list:
        RPS.objects.create(
            matkul=m
        )
    