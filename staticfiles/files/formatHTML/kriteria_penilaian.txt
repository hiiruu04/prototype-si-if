<p>Penilaian ini bertujuan untuk mengetahui penyerapan pengetahuan yang telah diperoleh oleh mahasiswa yang dilakukan dengan mengadakan 5 cara penilaian yaitu :</p>

<ol>
    <li>Tugas Kecil (10%)</li>
    <li>Tugas Besar (20%)</li>
    <li>Praktikum (10%)</li>
    <li>UTS ( 30 %)</li>
    <li>UAS ( 30 %)</li>
</ol>

<p>&nbsp;</p>

<p>Rentang Penilaian</p>

<table border="1" width="30%">
    <tbody>
        <tr>
            <td style="text-align:center; width:50px;"><p>100</p></td>
            <td style="text-align:center; width:80px;"><p>&gt; NA &gt;=</p></td>
            <td style="text-align:center; width:50px;"><p>80</p></td>
            <td style="text-align:center; width:30px;"><p>A</p></td>
        </tr>
        <tr>
            <td style="text-align:center"><p>80</p></td>
            <td style="text-align:center"><p>&gt; NA &gt;=</p></td>
            <td style="text-align:center"><p>70</p></td>
            <td style="text-align:center"><p>B</p></td>
        </tr>
        <tr>
            <td style="text-align:center"><p>70</p></td>
            <td style="text-align:center"><p>&gt; NA &gt;=</p></td>
            <td style="text-align:center"><p>60</p></td>
            <td style="text-align:center"><p>C</p></td>
        </tr>
        <tr>
            <td style="text-align:center"><p>60</p></td>
            <td style="text-align:center"><p>&gt; NA &gt;=</p></td>
            <td style="text-align:center"><p>50</p></td>
            <td style="text-align:center"><p>D</p></td>
        </tr>
        <tr>
            <td style="text-align:center"><p>50</p></td>
            <td style="text-align:center"><p>&gt; NA &gt;=</p></td>
            <td style="text-align:center"><p>0</p></td>
            <td style="text-align:center"><p>E</p></td>
        </tr>
    </tbody>
</table>