/****************************************************************************/
/* Fungsi untuk memvalidasi data dalam field hari                           */
/****************************************************************************/
function validasiHari(i, j) {
  // Pattern regex untuk validasi input pada field hari
  // Input adalah case-sensitive
  var patternHari = /^Senin|Selasa|Rabu|Kamis|Jum'at|Sabtu$/;

  if(patternHari.test(i.val()) !== true) {
    i.labels().addClass("ui-state-error");
    if(j == "add") {
      updateTipsAdd("Field Hari harus diisi dengan hari yang valid\nContoh:Senin, Selasa, Rabu");
      return false;
    }
    else {
      updateTipsUpdate("Field Hari harus diisi dengan hari yang valid\nContoh:Senin, Selasa, Rabu");
      return false;
    }
  }
  else {
    return true;
  }
}

/****************************************************************************/
/* Fungsi untuk memvalidasi data dalam field jam                            */
/****************************************************************************/
function validasiJam(i, j) {
  // Pattern regex untuk validasi input pada field jam
  var patternJam = /^([0-1][0-9]:[0-5][0-9]|[2][0-3]:[0-5][0-9])\s-\s([0-1][0-9]:[0-5][0-9]|[2][0-3]:[0-5][0-9])$/;

  if(patternJam.test(i.val()) !== true) {
    i.labels().addClass("ui-state-error");
    if(j == "add") {
      updateTipsAdd("Field Jam harus diisi dalam format 'JJ:MM - JJ:MM'");
      return false;
    }
    else {
      updateTipsUpdate("Field Jam harus diisi dalam format 'JJ:MM - JJ:MM'");
      return false;
    }
  }
  else {
    // Mengubah input field jam menjadi integer untuk divalidasi jangkauannya
    // misal 11:30 - 10:30 adalah jangkauan yang tidak valid, sedangkan
    // 11:30 - 12:30 adalah jangkauan yang valid
    var a = i.val().split("-");
    var b = [a[0].split(":"), a[1].split(":")];

    if((parseInt(b[0][0]) > parseInt(b[1][0])) || ((parseInt(b[0][0]) == parseInt(b[1][0])) && (parseInt(b[0][1]) > parseInt(b[1][1])))) {
      i.labels().addClass("ui-state-error");
      if(j == "add") {
        updateTipsAdd("Jam mulai tidak boleh melebihi Jam selesai");
        return false;
      }
      else {
        updateTipsUpdate("Jam mulai tidak boleh melebihi Jam selesai");
        return false;
      }
    }
    else {
      if((parseInt(b[0][0]) == parseInt(b[1][0])) && (parseInt(b[0][1]) == parseInt(b[1][1]))) {
        i.labels().addClass("ui-state-error");
        if(j == "add") {
          updateTipsAdd("Jam mulai tidak boleh sama dengan Jam selesai");
          return false;
        }
        else {
          updateTipsUpdate("Jam mulai tidak boleh sama dengan Jam selesai");
          return false;
        }
      }
      else {
        return true;
      }
    }
  }
}

/****************************************************************************/
/* Fungsi untuk memvalidasi data dalam form tambah slot baru                */
/****************************************************************************/
function validasiAdd(i, l) {
  if((i.val() == null) || (i.val() == "")) {
    i.labels().addClass("ui-state-error");
    updateTipsAdd("Field " + l + " tidak boleh kosong");
    return false;
  }
  else {
    if(jadwal["rowManager"]["activeRowsCount"] < 75) {
      return true;
    }
    else {
      updateTipsAdd("Slot sudah penuh dan tidak bisa ditambah lagi");
      return false;
    }
  }
}

/****************************************************************************/
/* Fungsi untuk memvalidasi data dalam form update slot                     */
/****************************************************************************/
function validasiUpdate(i, l) {
  if((i.val() == null) || (i.val() == "")) {
    i.labels().addClass("ui-state-error");
    updateTipsUpdate("Field " + l + " tidak boleh kosong");
    return false;
  }
  else {
    return true;
  }
}

/****************************************************************************/
/* Fungsi untuk mengecek apakah jam sesuai dengan sks sebuah mata kuliah    */
/****************************************************************************/
function isJamSesuaiSks(jamMulai, jamSelesai, sks) {
  var a = new Date(1970, 0, 2, parseInt(jamMulai.split(":")[0]), parseInt(jamMulai.split(":")[1]), 0);
  var b = new Date(1970, 0, 2, parseInt(jamSelesai.split(":")[0]), parseInt(jamSelesai.split(":")[1]), 0);

  if(sks == 4) {
    sks = sks / 2;
  }

  a.setMinutes(a.getMinutes() + (sks * 50));
  if(a <= b) {
    return true;
  }
  else {
    return false;
  }
}

/****************************************************************************/
/* Fungsi untuk mengecek apakah jam saling overlap                          */
/****************************************************************************/
function isJamOverlap(jam1Mulai, jam1Selesai, jam2Mulai, jam2Selesai) {
  var a = new Date(1970, 0, 2, parseInt(jam1Mulai.split(":")[0]), parseInt(jam1Mulai.split(":")[1]), 0);
  var b = new Date(1970, 0, 2, parseInt(jam1Selesai.split(":")[0]), parseInt(jam1Selesai.split(":")[1]), 0);
  var c = new Date(1970, 0, 2, parseInt(jam2Mulai.split(":")[0]), parseInt(jam2Mulai.split(":")[1]), 0);
  var d = new Date(1970, 0, 2, parseInt(jam2Selesai.split(":")[0]), parseInt(jam2Selesai.split(":")[1]), 0);

  if(!((b <= c) || (a >= d))) {
    return true;
  }
  else {
    return false;
  }
}

/****************************************************************************/
/* Fungsi untuk memvalidasi slot yang akan dimasukkan ke dalam tabel untuk  */
/* melihat apakah sudah ada slot yang sama pada tabel                       */
/****************************************************************************/
function validasiSlot(slot, j) {
  // ambil data tabel
  var dataTabel = jadwal.rowManager.rows;
  // deklarasi array untuk menyimpan data tabel
  var slotTabel = [];
  // mulai count untuk kesalahan
  var fCount = 0;
  // counter untuk mata kuliah 4 sks
  var empatSks = 0;

  // untuk setiap data pada tabel, masukkan ke dalam array
  for(var i = 0; i < dataTabel.length; i++) {
    var slotTemp = new Slot();
    slotTemp.setId(dataTabel[i].data.id);
    slotTemp.setHari(dataTabel[i].data.hari);
    slotTemp.setJam(dataTabel[i].data.jam);
    slotTemp.setRuang(dataTabel[i].data.ruang);
    slotTemp.setKode(dataTabel[i].data.kode);
    slotTemp.setMatkul(dataTabel[i].data.matkul);
    slotTemp.setSks(dataTabel[i].data.sks);
    slotTemp.setSmt(dataTabel[i].data.smt);
    slotTemp.setKls(dataTabel[i].data.kls);
    slotTemp.setDosenPengampu(dataTabel[i].data.dosen);
    slotTabel.push(slotTemp);
  }

  // cek apakah jam sesuai dengan sks
  if(isJamSesuaiSks(slot.getJamMulai(), slot.getJamSelesai(), slot.getSks())) {
    null;
  }
  else {
    if(j == "add") {
      matkul.labels().addClass("ui-state-error");
      jam.labels().addClass("ui-state-error");
      updateTipsAdd("Jam tidak sesuai dengan jumlah sks");
      return false;
    }
    else {
      matkulUp.labels().addClass("ui-state-error");
      jamUp.labels().addClass("ui-state-error");
      updateTipsUpdate("Jam tidak sesuai dengan jumlah sks");
      return false;
    }
  }

  // cek slot dengan setiap slot pada tabel
  for(var i = 0; i < slotTabel.length; i++) {
    // jika hari pada slot sama dengan hari pada slot di tabel dan id slot
    // tidak sama dengan id slot di tabel
    if((slot.getHari() == slotTabel[i].getHari()) && (slot.getId() !== slotTabel[i].getId())) {
      console.log(slot.getHari() + " " + slotTabel[i].getHari() + " " + i);
      // jika jam pada slot overlap dengan jam pada slot di tabel
      if(isJamOverlap(slot.getJamMulai(), slot.getJamSelesai(), slotTabel[i].getJamMulai(), slotTabel[i].getJamSelesai())) {
        console.log("jam overlap " + slot.getJam() + " " + slotTabel[i].getJam());
        // jika terdapat ruang yang sama pada jam yang overlap, maka bernilai false
        if(slot.getRuang().replace(/\s/g, '').toLowerCase() == slotTabel[i].getRuang().replace(/\s/g, '').toLowerCase()) {
          console.log(slot.getRuang() + slotTabel[i].getRuang());
          if(j == "add") {
            ruang.labels().addClass("ui-state-error");
            jam.labels().addClass("ui-state-error");
            updateTipsAdd("Ruang sedang digunakan pada waktu tersebut");
            fCount = fCount + 1;
          }
          else {
            ruangUp.labels().addClass("ui-state-error");
            jamUp.labels().addClass("ui-state-error");
            updateTipsUpdate("Ruang sedang digunakan pada waktu tersebut");
            fCount = fCount + 1;
          }
        }
        // jika ruang berbeda
        else {
          console.log("lewat cek ruang");
          // jika terdapat dosen yang sama mengajar sebuah matakuliah pada jam
          // yang overlap, maka bernilai false
          console.log(slot.getDosen1() == slotTabel[i].getDosen1());
          console.log(slot.getDosen1() + " " + slotTabel[i].getDosen1());
          console.log(slot.getDosen1() == slotTabel[i].getDosen2());
          console.log(slot.getDosen1() + " " + slotTabel[i].getDosen2());
          console.log(slot.getDosen2() == slotTabel[i].getDosen1());
          console.log(slot.getDosen2() + " " + slotTabel[i].getDosen1());
          console.log(slot.getDosen2() == slotTabel[i].getDosen2());
          console.log(slot.getDosen2() + " " + slotTabel[i].getDosen2());
          if((slot.getDosen1() == slotTabel[i].getDosen1()) || (slot.getDosen1() == slotTabel[i].getDosen2()) || (slot.getDosen2() == slotTabel[i].getDosen1()) || (slot.getDosen2() == slotTabel[i].getDosen2())) {
            console.log("dosen ada yang sama");
            if(j == "add") {
              matkul.labels().addClass("ui-state-error");
              jam.labels().addClass("ui-state-error");
              updateTipsAdd("Dosen sedang mengajar mata kuliah pada waktu tersebut");
              fCount = fCount + 1;
            }
            else {
              matkulUp.labels().addClass("ui-state-error");
              jamUp.labels().addClass("ui-state-error");
              updateTipsUpdate("Dosen sedang mengajar mata kuliah pada waktu tersebut");
              fCount = fCount + 1;
            }
          }
          // jika tidak terdapat dosen yang sama mengajar di ruang yang berbeda
          // dengan jam yang overlap, maka bernilai true
          else {
            null;
          }
        }
      }
      // jika jam pada slot tidak overlap dengan jam pada slot di tabel
      else {
        console.log("jam tidak overlap");
        // jika terdapat matakuliah dengan kelas yang sama pada waktu yang
        // berbeda di hari yang sama, maka bernilai false
        if((slot.getMatkul() == slotTabel[i].getMatkul()) && (slot.getKls() == slotTabel[i].getKls())) {
          console.log("dalam hari sama");
          console.log(slot.getMatkul() + slot.getKls());
          console.log(slotTabel[i].getMatkul() + slotTabel[i].getKls());
          if(j == "add") {
            matkul.labels().addClass("ui-state-error");
            updateTipsAdd("Mata kuliah sudah ditambah ke dalam jadwal");
            fCount = fCount + 1;
          }
          else {
            matkulUp.labels().addClass("ui-state-error");
            updateTipsUpdate("Mata kuliah sudah ditambah ke dalam jadwal");
            fCount = fCount + 1;
          }
        }
        // jika terdapat matakuliah dengan kelas yang berbeda pada waktu yang
        // berbeda di hari yang sama, maka bernilai true
        else {
          null;
        }
      }
    }
    // jika hari pada slot dan hari pada slot di tabel tidak sama
    if((slot.getHari() !== slotTabel[i].getHari()) && (slot.getId() !== slotTabel[i].getId())) {
      console.log(slot.getHari() + " " + slotTabel[i].getHari() + " " + i);
      // jika terdapat matakuliah dengan kelas yang sama di hari yang berbeda
      // (kecuali matakuliah dengan 4 sks), maka bernilai false
      if((slot.getMatkul() == slotTabel[i].getMatkul()) && (slot.getKls() == slotTabel[i].getKls())) {
        console.log("dalam hari beda ada matkul dan kelas yang sama");
        // jika mata kuliah selain 4 sks, maka bernilai false
        if(slot.getSks() !== 4) {
          if(j == "add") {
            matkul.labels().addClass("ui-state-error");
            updateTipsAdd("Mata kuliah sudah ditambah ke dalam jadwal");
            fCount = fCount + 1;
          }
          else {
            matkulUp.labels().addClass("ui-state-error");
            updateTipsUpdate("Mata kuliah sudah ditambah ke dalam jadwal");
            fCount = fCount + 1;
          }
        }
        // jika mata kuliah 4 sks
        else {
          console.log("mata kuliah 4 sks");
          empatSks = empatSks + 1;
          if(empatSks >= 2) {
            if(j == "add") {
              matkul.labels().addClass("ui-state-error");
              updateTipsAdd("Mata kuliah sudah ditambah ke dalam jadwal");
              fCount = fCount + 1;
            }
            else {
              matkulUp.labels().addClass("ui-state-error");
              updateTipsUpdate("Mata kuliah sudah ditambah ke dalam jadwal");
              fCount = fCount + 1;
            }
          }
          else {
            null;
          }
        }
      }
    }
  }

  if(fCount == 0) {
    return true;
  }
  else {
    return false
  }
}
