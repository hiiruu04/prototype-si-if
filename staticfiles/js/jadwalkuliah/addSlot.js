/****************************************************************************/
/* Fungsi untuk memberitahu kesalahan data yang dimasukkan di dalam form    */
/* tambah slot                                                              */
/****************************************************************************/
function updateTipsAdd(t) {
  tipsAdd.text(t).addClass("ui-state-highlight");
  setTimeout(function() {
    tipsAdd.removeClass("ui-state-highlight", 1500);
  }, 500 );
}

/****************************************************************************/
/* Fungsi untuk menambahkan slot kedalam tabel menggunakan data yang        */
/* disubmit melalui form tambah slot                                        */
/****************************************************************************/
function addSlot() {
  var valid = true;
  var slot = new Slot();
  allFieldsAdd.removeClass("ui-state-error");

  valid = valid && validasiHari(hari, "add");
  valid = valid && validasiJam(jam, "add");
  valid = valid && validasiAdd(ruang, "Ruang");
  valid = valid && validasiAdd(matkul, "Mata Kuliah");
  valid = valid && validasiAdd(kls, "Kelas");

  // Buat slot sesuai dengan data yang diinput melalui form tambah slot
  // Ini dilakukan dengan cara membandingkan data dari form dengan data
  // yang ada pada mata kuliah yang diambil dari API. Jika data sesuai,
  // maka data selanjutnya yang akan dibandingkan adalah data pengampu
  // di dalam ntuk mendapatkan
  // data
  for(var i = 0; i < matakuliah.length; i++) {
    if(matkul.val() == matakuliah[i].nama_matkul) {
      slot.setId(count);
      slot.setHari(hari.val());
      slot.setJam(jam.val());
      slot.setRuang(ruang.val());
      slot.setKode(matakuliah[i].kd_matkul);
      slot.setMatkul(matakuliah[i].nama_matkul);
      slot.setSks(matakuliah[i].sks);
      slot.setSmt(matakuliah[i].semester);
      for(var x = 0; x < pengampu.length; x++) {
        if((matkul.val() == pengampu[x].mata_kuliah) && (kls.val().toUpperCase() == pengampu[x].kelas)) {
          slot.setKls(pengampu[x].kelas);
          slot.setDosen1(pengampu[x].dosen_pengampu);
          slot.setDosen2(pengampu[x].dosen_pengampu_2);
        }
      }
    }
  };

  valid = valid && validasiSlot(slot, "add");

  if(valid){
    jadwal.addRow({id:slot.getId(), hari:slot.getHari(), jam:slot.getJam(), ruang:slot.getRuang(), kode:slot.getKode(), matkul:slot.getMatkul(), sks:slot.getSks(), smt:slot.getSmt(), kls:slot.getKls(), dosen:slot.getDosenPengampu()}, true);

    count = count + 1;
    dialogAddSlot.dialog("close");
    tipsAdd.text("Semua field wajib diisi.");
  }
  // Sort jadwal setelah update slot
  jadwal.setSort("jam", "asc");
  jadwal.setSort("hari", "dsc");
  return valid;
}
