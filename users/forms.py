from django import forms
from django.contrib.auth.forms import UserCreationForm, UserChangeForm, PasswordResetForm
from .models import User
from urllib import request
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Submit, Row, Column

class CustomUserCreationForm(UserCreationForm):
    class Meta:
        model = User
        fields = ('email','username')

class CustomUserChangeForm(UserChangeForm):
    class Meta:
        model = User
        fields = ('email','username')

class LoginForm(forms.Form):
    email = forms.EmailField(required=True)
    password = forms.CharField(required=True, widget=forms.PasswordInput)

class ResetPasswordForm(PasswordResetForm):
    email = forms.EmailField(max_length=254)

    def clean(self):
        cd = super().clean()
        curr_email = cd.get('email')
        qs = User.objects.filter(email=curr_email)
        if len(qs) == 0:
            raise forms.ValidationError('Alamat Email tidak terdaftar')