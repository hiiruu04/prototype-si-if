import pandas as pd
import csv
import datetime
import re
from dosen.models import Dosen, Ijazah, SKJabfung, Jabfung, LabKBK, Status, Golongan, Strata
from .models import User
from matkul.models import Matkul, Pengampu, KontrakPembelajaran, Kurikulum
from rps.models import RPS

class FileReader():
    """
        Kelas untuk memasukkan data dari file csv ke basis data hanya melalui shell.
        Jika ingin menggunakan kelas ini maka harus memanggil salah satu dari kelas ini, 
        dengan menggunakan parameter / argumen yang tidak boleh kosong.
        Gunakan : 
        from users.csv_reader import FileReader as fr
        fr.namafungsi_csv("csv/namafile.csv")
    """
    nama_data = None

    def dosen_csv(nama_data):
        if not nama_data :
            pass
        else :
            dt = pd.read_csv(nama_data)
            dt = dt.where((pd.notnull(dt)), None)
            for i in range(len(dt.index)):
                slug_name = "{}".format(dt['namaDepan'][i])
                if type(dt['namaBelakang'][i]) is str:
                    slug_name = "{}-{}".format(dt['namaDepan'][i],dt['namaBelakang'][i])
                if type(dt['namaTengah'][i]) is str:
                    slug_name = "{}-{}-{}".format(dt['namaDepan'][i],dt['namaTengah'][i],dt['namaBelakang'][i])
                    
                # Foreignkey check
                try :
                    statuss = Status.objects.filter(status=dt['status'][i])
                    if len(statuss)<1:
                        status = "0"
                    status = statuss[0]     
                except :
                    print("Masukkan status salah, cek data Dosen", slug_name ,"!")
                    status = "0"
                try :
                    golongans = Golongan.objects.filter(golongan=dt['golongan'][i])
                    if len(golongans)<1:
                        golongan = "0"
                    golongan = golongans[0]
                except :
                    print("Masukkan golongan salah, cek data Dosen", slug_name ,"!")
                    golongan = "0"
                try :
                    lab_kbks = LabKBK.objects.filter(namaLab=dt['lab_kbk'][i])
                    if len(lab_kbks)<1:
                        lab_kbk = "0"
                    lab_kbk = lab_kbks[0]
                except :
                    print("Masukkan lab_kbk salah, cek data Dosen", slug_name ,"!")
                    lab_kbk = "0"

                # Date check
                try :
                    inputDate = str(dt['tanggal_lahir'][i])
                    day,month,year = inputDate.split('/')
                    tgl = datetime.datetime(int(year),int(month),int(day))
                except ValueError :
                    print("Masukkan tanggal_lahir tidak sesuai format, cek data Dosen", slug_name ,"!")
                    inputDate = 0
                    
                exist = Dosen.objects.filter(email=dt['email'][i]).count()
                if exist < 1:
                    user = User.objects.create_user(slug_name.lower(), dt['email'][i], "pass1243")
                    user.save()
                    dsn = Dosen.objects.create(
                        user = user,
                        namaDepan = dt['namaDepan'][i],
                        namaTengah = dt['namaTengah'][i],
                        namaBelakang = dt['namaBelakang'][i],
                        gelarDepan = dt['gelarDepan'][i],
                        gelarBelakang = dt['gelarBelakang'][i],
                        jenkel = dt['jenkel'][i],
                        tempat_lahir = dt['tempat_lahir'][i],
                        alamat_rumah = dt['alamat_rumah'][i],
                        no_telp = dt['no_telp'][i],
                        kd_doswal = dt['kd_doswal'][i],
                        alamat_kantor = dt['alamat_kantor'][i],
                        ruangan = dt['ruangan'][i],
                        keahlian = dt['keahlian'][i],
                        email = dt['email'][i],
                        website = dt['website'][i],
                        prodi = dt['prodi'][i],
                        foto = dt['foto'][i],
                    )
                    if type(dt['nip'][i]) is str:
                        dsn.nip = re.findall(r'\"(.+?)\"',dt['nip'][i])[0]
                    if type(dt['nidn'][i]) is str:
                        dsn.nidn = re.findall(r'\"(.+?)\"',dt['nidn'][i])[0]
                    if type(lab_kbk) is not str: 
                        dsn.lab_kbk = lab_kbk  
                    if type(golongan) is not str: 
                        dsn.golongan = golongan
                    if type(status) is not str: 
                        dsn.status = status
                    if type(inputDate) is not int: 
                        dsn.tanggal_lahir = datetime.datetime.strptime(dt['tanggal_lahir'][i], "%d/%m/%Y").date()
                    dsn.save()
                elif exist == 1:
                    user = User.objects.get(email=dt['email'][i])
                    user.username = slug_name.lower()
                    user.email = dt['email'][i]
                    user.save()
                    obj = Dosen.objects.get(email=dt['email'][i])
                    if type(lab_kbk) is not str: 
                        obj.lab_kbk = lab_kbk  
                    if type(golongan) is not str: 
                        obj.golongan = golongan
                    if type(status) is not str: 
                        obj.status = status
                    if type(inputDate) is not int: 
                        obj.tanggal_lahir = datetime.datetime.strptime(dt['tanggal_lahir'][i], "%d/%m/%Y").date()
                    if type(dt['nip'][i]) is str:
                        obj.nip = re.findall(r'\"(.+?)\"',dt['nip'][i])[0]
                    else:
                        obj.nip = dt['nip'][i]
                    if type(dt['nidn'][i]) is str:
                        obj.nidn = re.findall(r'\"(.+?)\"',dt['nidn'][i])[0]
                    else:
                        obj.nidn = dt['nidn'][i]
                    obj.namaDepan = dt['namaDepan'][i]
                    obj.namaTengah = dt['namaTengah'][i]
                    obj.namaBelakang = dt['namaBelakang'][i]
                    obj.gelarDepan = dt['gelarDepan'][i]
                    obj.gelarBelakang = dt['gelarBelakang'][i]
                    obj.foto = dt['foto'][i]
                    obj.jenkel = dt['jenkel'][i]
                    obj.tempat_lahir = dt['tempat_lahir'][i]
                    obj.alamat_rumah = dt['alamat_rumah'][i]
                    obj.no_telp = dt['no_telp'][i]
                    obj.kd_doswal = dt['kd_doswal'][i]
                    obj.alamat_kantor = dt['alamat_kantor'][i]
                    obj.ruangan = dt['ruangan'][i]
                    obj.keahlian = dt['keahlian'][i]
                    obj.email = dt['email'][i]
                    obj.website = dt['website'][i]
                    obj.prodi = dt['prodi'][i]
                    obj.save()


    def matkul_csv(nama_data):
        if not nama_data :
            pass
        else :
            dt = pd.read_csv(nama_data)
            dt = dt.where((pd.notnull(dt)), None)
            list_ids = list()
            for i in range(len(dt.index)):
                try:
                    kur = int(dt['kurikulum'][i])
                except:
                    kur = 1
                kurs = Kurikulum.objects.filter(tahun=dt['kurikulum'][i])
                if len(kurs)<1:
                    print("Matkul", dt['kd_matkul'][i], "tidak dapat dibuat ataupun diupdate!")
                    continue
                kur = kurs[0]

                matkul_prs = dt['prasyarat'][i]
                if type(matkul_prs) is str :
                    matkul_prs = list()
                    prs = dt['prasyarat'][i].split(", ")
                    for pr in prs:
                        matkul_pr = Matkul.objects.filter(kd_matkul=pr)
                        if len(matkul_pr) < 1:
                            continue
                        matkul_prs.append(matkul_pr[0])
                    if len(matkul_prs) < 1:
                        matkul_prs = dt['prasyarat'][i]  

                exist = Matkul.objects.filter(kd_matkul=dt['kd_matkul'][i]).count()
                if exist < 1:
                    new = Matkul.objects.create(
                        kd_matkul = dt['kd_matkul'][i],
                        nama_matkul = dt['nama_matkul'][i], 
                        sks = dt['sks'][i], 
                        kompetensi = dt['kompetensi'][i], 
                        semester = dt['semester'][i], 
                        kurikulum = kur,                        
                    )
                    if type(matkul_prs) is list:
                        list_ids.append(new.id)
                        for pr in matkul_prs:
                            new.prasyarat.add(pr)
                    new.save()
                    KontrakPembelajaran.objects.create(matkul=new)
                    RPS.objects.create(matkul=new)
                elif exist == 1:
                    obj = Matkul.objects.get(kd_matkul=dt['kd_matkul'][i])
                    obj.kd_matkul = dt['kd_matkul'][i]
                    obj.nama_matkul = dt['nama_matkul'][i]
                    if type(matkul_prs) is list:
                        list_ids.append(obj.id)
                        list_id = list()
                        for pr in matkul_prs:
                            obj.prasyarat.add(pr)
                            list_id.append(pr.id)
                        obj.prasyarat.through.objects.filter(from_matkul_id=obj.id).exclude(to_matkul_id__in=list_id).delete()
                    obj.sks = dt['sks'][i]
                    obj.kompetensi = dt['kompetensi'][i] 
                    obj.semester = dt['semester'][i] 
                    obj.kurikulum = kur
                    obj.save()
            Matkul.prasyarat.through.objects.exclude(from_matkul_id__in=list_ids).delete()

    def kurikulum_csv(nama_data):
        if not nama_data :
            pass
        else :
            dt = pd.read_csv(nama_data, encoding = "ISO-8859-1")
            dt = dt.where((pd.notnull(dt)), None)
            for i in range(len(dt.index)):
                exist = Kurikulum.objects.filter(tahun=dt['tahun'][i]).count()
                if exist < 1:
                    Kurikulum.objects.create(
                        tahun = dt['tahun'][i],
                        prodi = dt['prodi'][i],
                        departemen = dt['departemen'][i],
                        kaprodi = dt['kaprodi'][i],
                        dekan = dt['dekan'][i],
                        rektor = dt['rektor'][i],
                        fakultas = dt['fakultas'][i],
                        universitas = dt['universitas'][i],
                        deskripsi = dt['deskripsi'][i],
                    )
                elif exist == 1:
                    obj = Kurikulum.objects.get(tahun=dt['tahun'][i])
                    obj.tahun = dt['tahun'][i]
                    obj.prodi = dt['prodi'][i]
                    obj.departemen = dt['departemen'][i]
                    obj.kaprodi = dt['kaprodi'][i]
                    obj.dekan = dt['dekan'][i]
                    obj.rektor = dt['rektor'][i]
                    obj.fakultas = dt['fakultas'][i]
                    obj.universitas = dt['universitas'][i]
                    obj.deskripsi = dt['deskripsi'][i]
                    obj.save()


    # Dasar    
    def golongan_csv(nama_data):
        if not nama_data :
            pass
        else :
            dt = pd.read_csv(nama_data)
            dt = dt.where((pd.notnull(dt)), None)
            for i in range(len(dt.index)):
                exist = Golongan.objects.filter(golongan=dt['golongan'][i]).count()
                if exist < 1:
                    Golongan.objects.create(
                        golongan = dt['golongan'][i],
                        pangkat = dt['pangkat'][i] 
                    )
                elif exist == 1:
                    obj = Golongan.objects.get(golongan=dt['golongan'][i])
                    obj.golongan = dt['golongan'][i]
                    obj.pangkat = dt['pangkat'][i]
                    obj.save()


    def status_csv(nama_data):
        if not nama_data :
            pass
        else :
            dt = pd.read_csv(nama_data)
            dt = dt.where((pd.notnull(dt)), None)
            for i in range(len(dt.index)):
                exist = Status.objects.filter(status=dt['status'][i]).count()
                if exist < 1:
                    Status.objects.create(
                        status = dt['status'][i]
                    )
                elif exist == 1:
                    obj = Status.objects.get(status=dt['status'][i])
                    obj.status = dt['status'][i]
                    obj.save()


    def labkbk_csv(nama_data):
        if not nama_data :
            pass
        else :
            dt = pd.read_csv(nama_data)
            dt = dt.where((pd.notnull(dt)), None)
            for i in range(len(dt.index)):
                exist = LabKBK.objects.filter(namaLab=dt['namaLab'][i]).count()
                if exist < 1:
                    LabKBK.objects.create(
                        namaLab = dt['namaLab'][i],
                        keterangan = dt['keterangan'][i],
                        deskripsi = dt['deskripsi'][i]
                    )
                elif exist == 1:
                    obj = LabKBK.objects.get(namaLab=dt['namaLab'][i])
                    obj.namaLab = dt['namaLab'][i]
                    obj.keterangan = dt['keterangan'][i]
                    obj.deskripsi = dt['deskripsi'][i]
                    obj.save()
            
    
    def jabfung_csv(nama_data):
        if not nama_data :
            pass
        else :
            dt = pd.read_csv(nama_data)
            dt = dt.where((pd.notnull(dt)), None)
            for i in range(len(dt.index)):
                exist = Jabfung.objects.filter(jabatan=dt['jabatan'][i]).count()
                if exist < 1:
                    Jabfung.objects.create(
                        jabatan = dt['jabatan'][i]
                    )
                elif exist == 1:
                    obj = Jabfung.objects.get(jabatan=dt['jabatan'][i])
                    obj.jabatan = dt['jabatan'][i]
                    obj.save()
    
    def strata_csv(nama_data):
        if not nama_data :
            pass
        else :
            dt = pd.read_csv(nama_data)
            dt = dt.where((pd.notnull(dt)), None)
            for i in range(len(dt.index)):
                exist = Strata.objects.filter(strata=dt['jenjang'][i]).count()
                if exist < 1:
                    Strata.objects.create(
                        strata = dt['jenjang'][i],
                        keterangan = dt['keterangan'][i],
                    )
                elif exist == 1:
                    obj = Strata.objects.get(strata=dt['jenjang'][i])
                    obj.strata = dt['jenjang'][i]
                    obj.keterangan = dt['keterangan'][i]
                    obj.save()
    
    
    def ijazah_csv(nama_data):
        if not nama_data :
            pass
        else :
            dt = pd.read_csv(nama_data)
            dt = dt.where((pd.notnull(dt)), None)
            for i in range(len(dt.index)):
                try :
                    jenjangs = Strata.objects.filter(strata=dt['jenjang'][i])
                    if len(jenjangs)<1:
                        jenjang = "0"
                        continue
                    jenjang = jenjangs[0]     
                except :
                    print("Masukkan jenjang salah, cek kembali data Ijazah!, Ijazah",dt['username'][i],"tidak dibuat!")
                    jenjang = "0"
                    continue

                try :
                    dosens = Dosen.objects.filter(email=dt['username'][i])
                    if len(dosens)<1:
                        dosen = "0"
                        continue
                    dosen = dosens[0]     
                except :
                    print("Masukkan dosen salah, cek kembali data Ijazah! Ijazah",dt['username'][i],"tidak dibuat!")
                    dosen = "0"
                    continue

                exist = Ijazah.objects.filter(dosen__email=dt['username'][i], strata__strata=dt['jenjang'][i]).count()
                # exist = Ijazah.objects.filter(dosen__email=dt['username'][i], strata__strata=dt['jenjang'][i], bidang=dt['bidang'][i], tahunLulus=dt['tahunLulus'][i]).count()
                if exist < 1:
                    if type(dosen) is not str:
                        ij = Ijazah.objects.create(
                            dosen = dosen,
                            bidang = dt['bidang'][i],
                            asalSekolah = dt['asalSekolah'][i],
                            asalNegara = dt['asalNegara'][i],
                            fileIjazah = dt['fileIjazah'][i],
                            keterangan = dt['keterangan'][i],
                        )
                    if type(jenjang) is not str: 
                        ij.strata = jenjang
                    try:
                        ij.tahunLulus = int(dt['tahunLulus'][i])
                    except:
                        ij.tahunLulus = dt['tahunLulus'][i]
                    ij.save()
                elif exist == 1:
                    obj = Ijazah.objects.get(dosen__email=dt['username'][i], strata__strata=dt['jenjang'][i])
                    # obj = Ijazah.objects.get(dosen__email=dt['username'][i], strata__strata=dt['jenjang'][i], bidang=dt['bidang'][i], tahunLulus=dt['tahunLulus'][i])
                    obj.bidang = dt['bidang'][i]
                    obj.asalSekolah = dt['asalSekolah'][i] 
                    obj.asalNegara = dt['asalNegara'][i]
                    obj.fileIjazah = dt['fileIjazah'][i]
                    obj.keterangan = dt['keterangan'][i]
                    print(obj.keterangan, obj.bidang, obj.asalNegara)
                    try:
                        obj.tahunLulus = int(dt['tahunLulus'][i])
                    except:
                        obj.tahunLulus = dt['tahunLulus'][i]
                    if type(jenjang) is not str: 
                        obj.strata = jenjang
                        print(obj.strata)
                    if type(dosen) is not str: 
                        obj.dosen = dosen  
                    obj.save()
    
    def skjabfung_csv(nama_data):
        if not nama_data :
            pass
        else :
            dt = pd.read_csv(nama_data)
            dt = dt.where((pd.notnull(dt)), None)
            for i in range(len(dt.index)):                
                # Date check
                try :
                    inputDate = str(dt['tanggalSK'][i])
                    day,month,year = inputDate.split('/')
                    tgl = datetime.datetime(int(year),int(month),int(day))
                except ValueError :
                    print("Masukkan tanggal_SK tidak sesuai format, cek kembali data SK!")
                    inputDate = 0
                
                try :
                    jabatans = Jabfung.objects.filter(jabatan=dt['jabatan'][i])
                    if len(jabatans)<1:
                        jabatan = "0"
                    jabatan = jabatans[0]     
                except :
                    print("Masukkan jabatan salah, cek kembali data SK!")
                    jabatan = "0"

                try :
                    pemiliks = Dosen.objects.filter(email=dt['username'][i])
                    if len(pemiliks)<1:
                        pemilik = "0"
                        continue
                    pemilik = pemiliks[0]
                except :
                    print("Masukkan pemilik salah, cek kembali data SK! SK",dt['username'][i],"tidak dibuat!")
                    pemilik = "0"
                    continue

                exist = SKJabfung.objects.filter(pemilik__email=dt['username'][i], jabatan__jabatan=dt['jabatan'][i]).count()
                if exist < 1:
                    sk = SKJabfung.objects.create(
                        pemilik = pemilik,
                        fileSK = dt['fileSK'][i],
                    )
                    if type(inputDate) is not int: 
                        sk.tanggalSK = datetime.datetime.strptime(dt['tanggalSK'][i], "%d/%m/%Y").date()
                    if type(jabatan) is not str: 
                        sk.jabatan = jabatan
                    sk.save()
                elif exist == 1:
                    obj = SKJabfung.objects.get(pemilik__email=dt['username'][i], jabatan__jabatan=dt['jabatan'][i])
                    obj.fileSK = dt['fileSK'][i]
                    if type(inputDate) is not int: 
                        obj.tanggalSK = datetime.datetime.strptime(dt['tanggalSK'][i], "%d/%m/%Y").date()
                    if type(jabatan) is not str: 
                        obj.jabatan = jabatan  
                    if type(pemilik) is not str: 
                        obj.pemilik = pemilik
                    obj.save()

    # Tidak sesuai
    def kontrak_csv(nama_data):
        if not nama_data :
            pass
        else :
            dt = pd.read_csv(nama_data)
            dt = dt.where((pd.notnull(dt)), None)
            for i in range(len(dt.index)):
                try :
                    matkuls = Matkul.objects.get(kd_matkul=dt['matkul'][i])
                    if len(matkuls)<1:
                        matkul = "0"
                        continue
                    matkul = matkuls[0]     
                except :
                    print("Masukkan matkul salah, cek kembali data Kontrak Pembelajaran! Kontrak Pembelajaran",dt['matkul'][i],"tidak dibuat!")
                    matkul = "0"
                    continue
                
                exist = KontrakPembelajaran.objects.filter(matkul__kd_matkul=dt['matkul'][i]).count()
                if exist < 1:
                    sl = KontrakPembelajaran.objects.create(
                        deskripsi_matkul = dt['deskripsi_matkul'][i],
                        topik_kajian = dt['topik_kajian'][i],
                        referensi = dt['referensi'][i],
                        capaian_pembelajaran = dt['capaian_pembelajaran'][i],
                        manfaat_pembelajaran = dt['manfaat_pembelajaran'][i],
                    )
                    if type(matkul) is not str: 
                        sl.matkul = matkul
                    sl.save()
                elif exist == 1:
                    obj = KontrakPembelajaran.objects.get(matkul__kd_matkul=dt['matkul'][i])
                    obj.deskripsi_matkul = dt['deskripsi_matkul'][i]
                    obj.topik_kajian = dt['topik_kajian'][i]
                    obj.referensi = dt['referensi'][i]
                    obj.capaian_pembelajaran = dt['capaian_pembelajaran'][i]
                    obj.manfaat_pembelajaran = dt['manfaat_pembelajaran'][i]
                    if type(matkul) is not str: 
                        obj.matkul = matkul
                    obj.save()

    def pengampu_csv(nama_data):
        if not nama_data :
            pass
        else :
            dt = pd.read_csv(nama_data)
            dt = dt.where((pd.notnull(dt)), None)
            for i in range(len(dt.index)):
                try :
                    dosens = Dosen.objects.filter(email=dt['username'][i])
                    matkuls = Matkul.objects.filter(kd_matkul=dt['matkul'][i])
                    if len(dosens)<1 or len(matkuls):
                        dosen = "0"
                        matkul = "0"
                        continue
                    dosen = dosens[0]
                    matkul = matkuls[0]     
                except :
                    print("Masukkan dosen/matkul salah, cek kembali data Pengampu! Pengampu",dt['matkul'][i],"yang diampu oleh",dt['dosen'][i],"tidak dibuat!")
                    dosen = "0"
                    matkul = "0"
                    continue
                exist = Pengampu.objects.filter(dosen_pengampu__email=dt['dosen'][i], mata_kuliah__kd_matkul=dt['matkul'][i]).count()
                if exist < 1:
                    pn = Pengampu.objects.create()
                    if type(dosen) is not str: 
                        pn.dosen = dosen  
                    if type(matkul) is not str: 
                        pn.matkul = matkul
                    pn.save()
                elif exist == 1:
                    obj = Pengampu.objects.get(dosen_pengampu__email=dt['dosen'][i], mata_kuliah__kd_matkul=dt['matkul'][i])
                    if type(dosen) is not str: 
                        obj.dosen = dosen  
                    if type(matkul) is not str: 
                        obj.matkul = matkul
                    obj.save()