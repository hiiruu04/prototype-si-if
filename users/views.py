from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from urllib import request
from django.contrib.auth.views import LoginView, PasswordChangeView, PasswordChangeDoneView, PasswordResetView, PasswordResetDoneView
from django.contrib.auth.forms import PasswordChangeForm
from django.contrib.auth.tokens import default_token_generator
from .forms import LoginForm, ResetPasswordForm


class LoginView(LoginView):
    success_url = reverse_lazy('authenticate')
    redirect_authenticated_user = True

class ChangePasswordView(PasswordChangeView):
    form_class = PasswordChangeForm
    success_url = reverse_lazy('password_change_done')
    template_name = 'registration/password_change_form.html'

    # def dispatch(self, request, *args, **kwargs):
    #     user = request.user
    #     group = user.groups.filter(name="Dosen") | user.groups.filter(name="Operator")
    #     if len(group) == 0:
    #         return redirect('authenticate')
    #     return super().dispatch(request, *args, **kwargs)
    
class ChangePasswordDoneView(PasswordChangeDoneView):
    template_name = 'registration/password_change_done.html'

class ResetPasswordView(PasswordResetView):
    email_template_name = 'registration/password_reset_email.html'
    extra_email_context = None
    form_class = ResetPasswordForm
    from_email = None
    html_email_template_name = None
    subject_template_name = 'registration/password_reset_subject.txt'
    success_url = reverse_lazy('password_reset_done')
    template_name = 'registration/password_reset_form.html'
    token_generator = default_token_generator

    def form_valid(self, form):
        opts = {
            'use_https': self.request.is_secure(),
            'token_generator': self.token_generator,
            'from_email': self.from_email,
            'email_template_name': self.email_template_name,
            'subject_template_name': self.subject_template_name,
            'request': self.request,
            'html_email_template_name': self.html_email_template_name,
            'extra_email_context': self.extra_email_context,
        }
        form.save(**opts)
        return super().form_valid(form)

# class ResetPasswordDoneView(PasswordResetDoneView):
#     template_name = 'registration/password_reset_complete.html'