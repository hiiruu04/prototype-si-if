from django.urls import path
from .views import LoginView
from django.contrib.auth import views as auth_views
from dosen import views as dosen_views

urlpatterns = [
    # path('login/', views.user_login, name='login')
    path('login/', LoginView.as_view(), name="login_admin"),
    path('dashboard/',dosen_views.dashboardAdmin, name="dashboard_admin"),
    path('create-op/', dosen_views.CreateOperator.as_view(), name="new_operator"),
    path('list-op/', dosen_views.ManageOperatorListView.as_view(), name='list_operator'),
    path('<pk>/delete/', dosen_views.DeleteOperator.as_view(), name='operator_delete'),
]
