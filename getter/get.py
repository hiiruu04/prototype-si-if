from dosen.models import Dosen, Operator, Ijazah, SKJabfung, Jabfung, Status, Strata, Golongan, LabKBK
from users.models import User
from matkul.models import Kurikulum, Matkul, CPMK, KontrakPembelajaran, Pengampu
from rps.models import ArsipRPS, RPS, MetodePembelajaran, PengalamanBelajar, BahanKajian, Tim, Anggota, CommentRPS
from bs4 import BeautifulSoup
from dosen.utils import xstr, xint
import re
# RPS

def get_data_comments(comment):
    data = {}
    id = comment.id
    rps = comment.rps
    date = comment.created_date
    body = ""
    if comment.body:
        body = comment.body
    log = ""
    if comment.log_status:
        log = comment.log_status
    check = []
    check_now = comment.checked.all()
    if len(check_now) > 0:
        for cek in check_now:
            # dosen = get_data_dosen(cek)
            check.append(cek)
    data['id'] = id
    data['rps'] = rps
    data['tanggal'] = date
    data['body'] = body
    data['log'] = log
    data['check'] = check
    return data

def get_comments(rps):
    data = []
    comments = CommentRPS.objects.filter(rps=rps).order_by("-created_date")
    if len(comments) > 0:
        for comment in comments:
            cmt = get_data_comments(comment)
            data.append(cmt)
    return data

def get_data_metode(mp):
    data = {}
    data['id'] = mp.id
    metode = "-"
    if mp.metode :
        metode = mp.metode
    kg_dosen = "-"
    if mp.kegiatan_dosen :
        kg_dosen = mp.kegiatan_dosen
    kg_mhs = "-"
    if mp.kegiatan_mahasiswa :
        kg_mhs = mp.kegiatan_mahasiswa
    ciri = "-"
    if mp.ciri_umum :
        ciri = mp.ciri_umum
    data['metode'] = metode
    data['kg_dosen'] = kg_dosen
    data['kg_mhs'] = kg_mhs
    data['ciri'] = ciri
    return data

def get_metode_pembelajaran(bk):
    data = []
    metodes = bk.metode_pembelajaran.all()
    if len(metodes) > 0:
        for metode in metodes:
            mt = get_data_metode(metode)
            data.append(mt) 
    return data

def get_data_pengalaman(pb):
    data = {}
    data['id'] = pb.id
    pengalaman = "-"
    if pb.pengalaman_belajar :
        pengalaman = pb.pengalaman_belajar
    penjelasan = "-"
    if pb.penjelasan :
        penjelasan = pb.penjelasan
    data['pengalaman'] = pengalaman
    data['penjelasan'] = penjelasan
    return data

def get_pengalaman(bk):
    data = []
    pengalamans = bk.pengalaman_belajar.all()
    if len(pengalamans) > 0:
        for pengalaman in pengalamans:
            pb = get_data_pengalaman(pengalaman)
            data.append(pb)
    return data

def get_referensi_parse(ref):
    data = []
    if ref:
        tags = BeautifulSoup(ref, "html.parser")
        ref_ke = 0
        for tag in str(tags).split('\n'):
            if re.compile(r'<li>').search(tag):
                tag = re.sub(r"</?li>", "", tag)
                tag = re.sub(r"[\t\r\n\f\xa0]", "", tag)
                tag = re.sub(r"\s\s+", "", tag)
                text = re.sub(r"</?em>", "", tag) 
                normal1 = re.sub(r"<em>([A-z0-9]*[.,\"\'\(\)]*\s*)*</em>([A-z0-9]*[.,\"\'\(\)]*\s*)*", "", tag)
                normal2 = None
                em = None
                if re.compile(r'<em>').search(tag):
                    normal2 = re.sub(r"^([A-z0-9]*[.,\"\'\(\)]*\s*)*<em>([A-z0-9]*[.,\"\'\(\)]*\s*)*</em>", "", tag)
                    em = re.sub(r"^([A-z0-9]*[.,\"\'\(\)]*\s*)*<em>", "", tag)
                    em = re.sub(r"</em>([A-z0-9]*[.,\"\'\(\)]*\s*)*", "", em)
                ref = {
                    'text': text,
                    'normal1': normal1,
                    'em': em,
                    'normal2': normal2,
                }
                data.append(ref)
                ref_ke += 1
    return data

def get_kajian_parse(kajian):
    data = []
    if kajian:
        parse = BeautifulSoup(kajian, "html.parser")
        level = 0
        topik_ke = 0
        for tag in str(parse).split('\n'):
            if re.compile(r'<ul>').search(tag):
                level += 1
            if re.compile(r'</ul>').search(tag):
                level -= 1
            if level is 1:
                if re.compile(r'<li>').search(tag):
                    tag = re.sub(r"</?li>", "", tag)
                    tag = re.sub(r"[\t\r\n\f\xa0]", "", tag)
                    tag = re.sub(r"\s\s+", "", tag)
                    sub = []
                    topik = {}
                    topik['bahan'] = tag
                    topik['sub-bahan'] = sub
                    data.append(topik)
                    topik_ke += 1
            if level is 2:
                if re.compile(r'<li>').search(tag):
                    tag = re.sub(r"</?li>", "", tag)
                    tag = re.sub(r"[\t\r\n\f\xa0]", "", tag)
                    tag = re.sub(r"\s\s+", "", tag)
                    data[topik_ke-1]['sub-bahan'].append(tag)
    return data

def get_kriteria_parse(kajian):
        data = []
        if kajian.kriteria_indikator:
            tags = BeautifulSoup(kajian.kriteria_indikator, "html.parser")
            data = [p.get_text() for p in tags.find_all("p")]
        return data

def get_data_bahan_kajian(bk):
    data = {}
    data['id'] = bk.id
    data['minggu'] = bk.minggu_ke
    data['kemampuan'] ="-"
    if bk.kemampuan_akhir :
        data['kemampuan'] = bk.kemampuan_akhir
    data['referensi'] = None
    if bk.referensi :
        data['referensi'] = bk.referensi
    kajian ="-" # Rich text field
    if bk.bahan_kajian :
        kajian = bk.bahan_kajian
    data['kajian'] = kajian
    data['kajian_parse'] = get_kajian_parse(kajian)
    data['metode'] = get_metode_pembelajaran(bk)
    tm ="-"
    if bk.waktu_tm :
        tm = bk.waktu_tm
    kt ="-"
    if bk.waktu_kt :
        kt = bk.waktu_kt
    km ="-"
    if bk.waktu_km :
        km = bk.waktu_km
    data['waktu_tm'] = tm
    data['waktu_kt'] = kt
    data['waktu_km'] = km
    waktu = "TM: {}\nKT: {}\nKM: {}".format(xstr(tm), xstr(kt), xstr(km))
    data['waktu'] = waktu
    data['pengalaman'] = get_pengalaman(bk)
    data['kriteria'] ="-" # Rich text field
    if bk.kriteria_indikator :
        data['kriteria'] = bk.kriteria_indikator
    data['kriteria_parse'] = get_kriteria_parse(bk)
    data['bobot'] = 0
    if bk.bobot :
        data['bobot'] = bk.bobot
    data['is_uts'] = bk.is_uts
    data['is_uas'] = bk.is_uas
    return data

def get_bahan_kajian(rps):
    data = []
    bks = BahanKajian.objects.filter(rps=rps).order_by("minggu_ke")
    if len(bks) > 0:
        for bk in bks :
            bahan = get_data_bahan_kajian(bk)
            data.append(bahan)
    return data

def get_jadwal_pembelajaran(rps):
    """
        bahan_kajian
        referensi
        minggu_ke
    """
    data = []
    bks = get_bahan_kajian(rps)
    if len(bks) > 0:
        for bk in bks:
            datum = {}
            kajian = bk['kajian_parse']
            referensi = bk['referensi']
            minggu = bk['minggu']
            datum['kajian'] = kajian
            datum['referensi'] = referensi
            datum['minggu'] = minggu
            data.append(datum)
    return data

def get_data_anggota(anggota):
    data = {}
    data['id'] = anggota.id
    dosen = get_data_dosen(anggota.dosen)
    data['dosen'] = dosen
    ketua = anggota.is_ketua
    data['is_ketua'] = ketua
    return data

def get_anggota(tim):
    data = []
    qs_anggota = Anggota.objects.filter(tim=tim).exclude(is_ketua=True)
    if len(qs_anggota) > 0:
        for anggota in qs_anggota:
            agt = get_data_anggota(anggota)
            data.append(agt)
    return data

def get_data_rps(rps):
    data={}
    max_mg = 16
    id = rps.id
    matkul = get_data_matkul(rps.matkul)
    deskripsi = "-"
    if rps.deskripsi_matkul:
        deskripsi = rps.deskripsi_matkul
    referensi = "-"
    if rps.referensi:
        referensi = rps.referensi
    status = rps.status
    kajian = get_bahan_kajian(rps)
    sisa_bahan = max_mg - len(kajian)
    bobot = sum(xint(b['bobot']) for b in kajian)
    data['id'] = id
    data['matkul'] = matkul
    data['deskripsi'] = deskripsi
    data['referensi'] = referensi
    data['status'] = status
    data['kajian'] = kajian
    data['bobot'] = bobot
    data['sisa'] = sisa_bahan
    return data

def get_data_tim(tim):
    data = {}
    data['id'] = tim.id
    data['nama'] = tim.namaTim
    _rps = tim.rps.all()
    data_rps = []
    if len(_rps)>0:
        for rps in _rps:
            rp = get_data_rps(rps)
            data_rps.append(rp)
    data['rps'] = data_rps
    qs_ketua = Anggota.objects.filter(tim=tim, is_ketua=True)
    ketua = "-"
    exist = False
    if len(qs_ketua) > 0 :
        ketua = get_data_anggota(qs_ketua[0])
        exist = True
    data['ketua'] = ketua
    data['is_exist_ketua'] = exist
    anggota = get_anggota(tim)
    data['anggota'] = anggota
    return data

def get_tim():
    data = []
    tim = Tim.objects.all()
    if len(tim) > 0:
        for tm in tim:
            date = get_data_tim(tm)
            data.append(date)
    return data

def get_tahun_arsip():
    data = []
    arsips = ArsipRPS.objects.all().order_by("-tahun")
    if len(arsips) > 0 :
        for arsip in arsips:
            tahun = arsip.tahun
            if tahun not in data:
                data.append(tahun)
    return data

def get_data_arsip(arsip):
    data = {}
    id = arsip.id
    matkul = None
    if arsip.matkul :
        matkul = get_data_matkul(arsip.matkul)
    tahun = arsip.tahun
    path = arsip.fileArsip.url
    data['id'] = id
    data['matkul'] = matkul
    data['tahun'] = tahun
    data['path'] = path
    return data

def get_arsip_per_tahun(tahun):
    data = []
    arsips = ArsipRPS.objects.filter(tahun=tahun)
    if len(arsips) > 0 :
        for arsip in arsips :
            ars = get_data_arsip(arsip)
            data.append(ars)
    return data

def get_arsip():
    data = []
    tahuns = get_tahun_arsip()
    if len(tahuns) > 0 :
        for tahun in tahuns :
            datum = {}
            th = get_arsip_per_tahun(tahun)
            datum['tahun'] = tahun
            datum['arsip'] = th
            data.append(datum)
    return data

# Matkul

def get_pengampu(matkul):
    data = []
    data_id = []
    a = []
    pengampus = Pengampu.objects.filter(mata_kuliah=matkul)
    if len(pengampus) > 0:
        for pengampu in pengampus:
            if pengampu.dosen_pengampu not in a:
                dosen = Dosen.objects.get(id=pengampu.dosen_pengampu_id)
                u_id = dosen.user.id
                u = dosen
                data.append(u)
                data_id.append(u_id)
                a.append(pengampu.dosen_pengampu)
    return data, data_id

def get_data_cpmk(cpmk):
    data = {}
    data['id'] = cpmk.id
    data['kurikulum'] = cpmk.id
    data['cpmk'] = "-"
    if cpmk.cpmk :
        data['cpmk'] = cpmk.cpmk
    return data
        
def get_cpmk(matkul):
    data = []
    cpmks = matkul.cpmk.all()
    if len(cpmks) > 0:
        for cpmk in cpmks:
            cp =  get_data_cpmk(cpmk)
            data.append(cp)
    return data

def get_data_prasyarat(prasyarat):
    data = {}
    data['id'] = prasyarat.id
    data['kd'] = "-"
    if prasyarat.kd_matkul:
        data['kd'] = prasyarat.kd_matkul
    data['nama'] = "-"
    if prasyarat.nama_matkul :
        data['nama'] = prasyarat.nama_matkul
    return data

def get_prasyarat(matkul):
    data = []
    prasyarats = matkul.prasyarat.all()
    if len(prasyarats) > 0:
        for prasyarat in prasyarats :
            pra = get_data_prasyarat(prasyarat)
            data.append(pra)
    return data

def get_data_kurikulum(kurikulum):
    data = {}
    data['id'] = kurikulum.id
    data['tahun'] = kurikulum.tahun
    data['prodi'] = kurikulum.prodi
    data['dekan'] = "-"
    if kurikulum.dekan :
        data['dekan'] = kurikulum.dekan
    data['rektor'] = "-"
    if kurikulum.dekan :
        data['rektor'] = kurikulum.rektor
    data['departemen'] = "-"
    if kurikulum.departemen :
        data['departemen'] = kurikulum.departemen
    data['fakultas'] = "-"
    if kurikulum.fakultas :
        data['fakultas'] = kurikulum.fakultas
    data['universitas'] = "-"
    if kurikulum.universitas :
        data['universitas'] = kurikulum.universitas
    data['deskripsi'] = "-"
    if kurikulum.deskripsi :
        data['deskripsi'] = kurikulum.deskripsi
    return data

def get_kurikulum():
    data = []
    kurikulums = Kurikulum.objects.all()
    if len(kurikulums) > 0:
        kur = get_data_kurikulum(kurikulum)
        data.append(data)
    return data

def get_matkul(kur, pilihan=False):
    """
        Fungsi yang digunakan untuk mendapatkan semua matakuliah
        pada kurikulum tertentu
    """
    matkul = Matkul.objects.filter(kurikulum=kur).order_by('semester')
    if pilihan:
        matkul = Matkul.objects.filter(kurikulum=kur, kompetensi="Pilihan").order_by('semester')
    return matkul

def get_data_matkul(matkul):
    """
        Fungsi yang digunakan untuk mendapatkan semua data pada suatu matkul
    """
    makul={}
    makul['id'] = matkul.id
    makul['nama_matkul'] = matkul.nama_matkul
    makul['kd_matkul'] = "-"
    if matkul.kd_matkul:
        makul['kd_matkul'] = matkul.kd_matkul
    makul['kurikulum'] = matkul.kurikulum
    makul['sks'] = "-"
    if matkul.sks:
        makul['sks'] = matkul.sks
    makul['smt'] = "-"
    if matkul.semester:
        makul['smt'] = matkul.semester
    makul['kompetensi'] = "-"
    if matkul.kompetensi:
        makul['kompetensi'] = matkul.kompetensi
    makul['prasyarat'] = get_prasyarat(matkul)
    makul['cpmk'] = get_cpmk(matkul)
    return makul

def matkul_per_semester(smt,kur,pilihan=False):
    mk=[]
    if not pilihan :
        matkuls = Matkul.objects.filter(semester=smt, kurikulum=kur)
        matkuls = matkuls.exclude(kompetensi='Pilihan')
    else :
        matkuls = Matkul.objects.filter(semester=smt, kurikulum=kur, kompetensi='Pilihan')
    if len(matkuls) > 0:
        for matkul in matkuls :
            matkul=get_data_matkul(matkul)
            mk.append(matkul)
    return mk

# Dosen
def get_user_dosen(user):
    id = user.id
    dosen_id = Dosen.objects.filter(user_id=id)
    if len(dosen_id)>0:
        dosen = dosen_id[0]
        return dosen.id

def get_data_dosen(dosen):
    """
        Fungsi yang digunakan untuk mengambil data dosen dari database
        Parameter :
            dosen
                    Merupakan data dengan model dosen, sehingga mempunyai semua atribut dosen
        Output :
            dsn
                    Dict
                    Merupakan dictionary dari data dosen yang didapat
    """
    dsn={}
    fullname = dosen.namaDepan
    if dosen.namaTengah :
        fullname = fullname +" "+ dosen.namaTengah
    if dosen.namaBelakang :
        fullname = fullname +" "+ dosen.namaBelakang
    if dosen.gelarDepan :
        fullname = dosen.gelarDepan +". "+ fullname
    if dosen.gelarBelakang :
        fullname = fullname +", "+ dosen.gelarBelakang
    dsn['name'] = fullname
    dsn['id'] = dosen.id
    dsn['nip'] = "-"
    if dosen.nip :
        dsn['nip'] = dosen.nip
    dsn['nidn'] = "-"
    if dosen.nidn :
        dsn['nidn'] = dosen.nidn
    dsn['golongan'] = "-"
    if dosen.golongan:
        dsn['golongan'] = dosen.golongan
    dsn['status'] = "-"
    if dosen.status :
        dsn['status'] = dosen.status
    dsn['alamat_rumah'] ="-"
    if dosen.alamat_rumah :
        dsn['alamat_rumah'] = dosen.alamat_rumah
    dsn['no_telp'] = "-"
    if dosen.no_telp :
        dsn['no_telp'] = dosen.no_telp
    dsn['kd_doswal'] = "-"
    if dosen.kd_doswal :
        dsn['kd_doswal'] = dosen.kd_doswal
    dsn['keahlian'] = "-"
    if dosen.keahlian :
        dsn['keahlian'] = dosen.keahlian
    dsn['email'] = "-"
    if dosen.email :
        email = dosen.email
        depan, belakang = email.split('@')
        full = depan + ' [et] ' + belakang
        dsn['email'] = full
    dsn['website'] = "-"
    if dosen.website :
        dsn['website'] = dosen.website
    dsn['prodi'] = "-"
    if dosen.prodi :
        dsn['prodi'] = dosen.prodi
    dsn['foto'] = "-"
    if dosen.foto :
        dsn['foto'] = dosen.foto
    dsn['user_id'] = "-"
    if dosen.user_id :
        dsn['user_id'] = dosen.user_id
    # LAB KBK
    dsn['lab'] = "-"
    if dosen.lab_kbk :
        dsn['lab'] = dosen.lab_kbk

    # Alamat kantor
    dsn['alamat_kantor'] = '-'
    if dosen.alamat_kantor :
        alamat = dosen.alamat_kantor
        ruang = ""
        if dosen.ruangan :
            ruang = " Ruang " + dosen.ruangan
        dsn['alamat_kantor'] = alamat + ruang

    # Jabatan
    skjabfung = SKJabfung.objects.filter(pemilik_id=dosen.id).order_by('-tanggalSK')
    dsn['jabatan'] = "-"
    if len(skjabfung) > 0 :
        sk = skjabfung[0]
        jabatans = Jabfung.objects.filter(id=sk.jabatan_id)
        if len(jabatans)>0:
            jabatan = jabatans[0]
            dsn['jabatan'] = jabatan
    
    # Ijazah
    ijazahs = Ijazah.objects.filter(dosen_id=dosen.id).order_by('tahunLulus')
    dsn['ijazah'] = []
    if len(ijazahs) > 0 :
        data = []
        for ijazah in ijazahs:
            ijz = get_data_ijazah(ijazah)
            data.append(ijz)
        dsn['ijazah'] = data

    # GENDER CONSTRUCT
    dsn['jenkel'] = "-"
    if dosen.jenkel :
        if dosen.jenkel.lower() == 'l':
            dsn['jenkel'] = "Laki-laki"
        elif dosen.jenkel.lower() == 'p':
            dsn['jenkel'] = "Perempuan"

    # TTL CONSTRUCT
    tempat_lahir = "-"
    if dosen.tempat_lahir :
        tempat_lahir = dosen.tempat_lahir

    tanggal_lahir = "-"
    if dosen.tanggal_lahir :
        tgl = dosen.tanggal_lahir
        tanggal_lahir = tgl.strftime('%d-%m-%Y')

    dsn['ttl'] = tempat_lahir + ", " + tanggal_lahir
    
    return dsn

def get_data_operator(operator):
    data= {}
    data['id'] = operator.id
    data['nama'] = operator.nama
    data['email'] = operator.user.email
    return data

def get_operator():
    operators = Operator.objects.all()
    data = []
    if len(operators)>0:
        for op in operators:
            operator = get_data_operator(op)
            data.append(op)
    return data

def get_dosen(lab=None):
    dosens = Dosen.objects.all().order_by('-nip')
    error = False
    if lab:
        lab_ = LabKBK.objects.get(namaLab=lab)
        lab_id = lab_.id
        dosens = Dosen.objects.filter(lab_kbk_id = lab_id)
    data = []
    data2 = []
    if len(dosens) > 0:
        for dosen in dosens:
            if dosen.nip is not None:
                data_dsn = get_data_dosen(dosen)
                dsn = data_dsn
                data.append(dsn)
            else:
                data_dsn = get_data_dosen(dosen)
                dsn = data_dsn
                data2.append(dsn)
    return data + data2

def get_data_ijazah(ijazah):
    ijz = {}
    id_ijazah = ijazah.id
    id_dosen = ijazah.dosen_id
    strata = "-"
    if ijazah.strata :
        strata = ijazah.strata.strata
    bidang = "-"
    if ijazah.bidang:
        bidang = ijazah.bidang
    sekolah="-"
    if ijazah.asalSekolah:
        sekolah = ijazah.asalSekolah
    negara="-"
    if ijazah.asalNegara:
        negara = ijazah.asalNegara
    gambar="-"
    if ijazah.fileIjazah:
        gambar = ijazah.fileIjazah
    tahun ="-"
    if ijazah.tahunLulus:
        tahun = ijazah.tahunLulus
    ket = "-"
    if ijazah.keterangan:
        ket = ijazah.keterangan
    # DOSEN

    ijz['id'] = id_ijazah
    ijz['dosen'] = id_dosen
    ijz['nama'] = strata + " " + bidang
    ijz['sekolah'] = sekolah
    ijz['negara'] = negara
    ijz['gambar'] = gambar
    ijz['tahun'] = tahun
    ijz['keterangan'] = ket

    return ijz

def get_data_sk(sk):
    sks={}
    id_sk = sk.id
    id_dosen = sk.pemilik_id
    id_jabatan = sk.jabatan_id
    gambar = "-"
    if sk.fileSK:
        gambar = sk.fileSK
    tanggal = "-"
    if sk.tanggalSK :
        tanggal = sk.tanggalSK
    jabatan = Jabfung.objects.get(id=id_jabatan)
    sks['id'] = id_sk
    sks['dosen'] = id_dosen
    sks['tanggal'] = tanggal
    sks['jabatan'] = jabatan
    sks['gambar'] = gambar
    return sks

def get_data_jabatan(jabfung):
    jab = {}
    id = jabfung.id
    jabatan = "-"
    if jabfung.jabatan:
        jabatan = jabfung.jabatan
    jab['id'] = id
    jab['jabatan'] = jabatan
    return jab

def get_jabatan():
    jabatans = Jabfung.objects.all()
    jabs = []
    if len(jabatans) > 0:
        for jabfung in jabatans:
            jab = get_data_jabatan(jabfung)
            jabs.append(jab)
    return jabs

def get_data_golongan(golongan):
    gol = {}
    id = golongan.id
    nama = "-"
    if golongan.golongan:
        nama = golongan.golongan
    pangkat = "-"
    if golongan.pangkat:
        pangkat = golongan.pangkat
    gol['id'] = id
    gol['golongan'] = nama
    gol['pangkat'] = pangkat
    return gol

def get_golongan():
    golongans = Golongan.objects.all()
    gols = []
    if len(golongans) > 0:
        for golongan in golongans:
            gol = get_data_golongan(golongan)
            gols.append(gol)
    return gols

def get_data_status(status):
    stat = {}
    id = status.id
    sts = "-"
    if status.status:
        sts = status.status
    stat['id'] = id
    stat['status'] = sts
    return stat

def get_status():
    statuses = Status.objects.all()
    data = []
    if len(statuses) > 0:
        for status in statuses:
            stat = get_data_status(status)
            data.append(stat)
    return data

def get_data_lab(lab):
    data = {}
    id = lab.id
    nama = "-"
    if lab.namaLab :
        nama = lab.namaLab
    ket = "-"
    if lab.keterangan :
        ket = lab.keterangan
    desk = "-"
    if lab.deskripsi :
        desk = lab.deskripsi
    
    data['id'] = id
    data['nama'] = nama
    data['ket'] = ket
    data['desk'] = desk
    return data

def get_lab():
    labs = LabKBK.objects.all()
    data = []
    if len(labs) > 0:
        for lab in labs:
            kbk = get_data_lab(lab)
            data.append(kbk)
    return data

def get_data_strata(strata):
    data = {}
    id = strata.id
    strat = "-"
    if strata.strata :
        strat = strata.strata
    ket = "-"
    if strata.keterangan :
        ket = strata.keterangan
    
    data['id'] = id
    data['strata'] = strat
    data['ket'] = ket

    return data

def get_strata():
    stratas = Strata.objects.all()
    data = []
    if len(stratas) > 0:
        for strata in stratas:
            strat = get_data_strata(strata)
            data.append(strat)
    return data
