from django import template

from jadwalkuliah.models import Jam, Ruangan
import datetime


register = template.Library()


@register.simple_tag
# Menentukan gasal atau genap
def gas_or_gen():
    month = datetime.date.today().month
    if month < 7:
        return "GENAP"
    else:
        return "GASAL"


@register.simple_tag
# Mendapatkan tahun pelajaran dari sistem
def thn_pel():
    year = datetime.date.today().year
    if gas_or_gen() == "GASAL":
        return f"{year}/{year+1}"
    else:
        return f"{year-1}/{year}"


@register.simple_tag
# Mendapatkan jam
def jm():
    msg = "var jamauto = ["
    jams = Jam.objects
    for jam in jams.all():
        msg = msg + '"' + jam.jam_mulai.strftime("%H:%M") + ' - ' + jam.jam_selesai.strftime("%H:%M") + '",\n\t\t\t\t\t'

    msg = msg + "]; jamauto.sort();"
    return msg


@register.simple_tag
# Mendapatkan ruang
def rng():
    msg = "var ruangauto = ["
    ruangans = Ruangan.objects
    for ruangan in ruangans.all():
        msg = msg + '"' + ruangan.ruang + '",\n\t\t\t\t\t'

    msg = msg + "]; ruangauto.sort();"
    return msg
