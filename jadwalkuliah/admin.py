from django.contrib import admin
from .models import Jam, Ruangan


class JamAdmin(admin.ModelAdmin):
    ordering = ("jam_mulai", "jam_selesai",)


class RuanganAdmin(admin.ModelAdmin):
    ordering = ("ruang",)


admin.site.register(Jam, JamAdmin)
admin.site.register(Ruangan, RuanganAdmin)
