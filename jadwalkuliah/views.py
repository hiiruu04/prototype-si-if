from django.shortcuts import render

from .forms import FormAddSlot, FormUpdateSlot


def index(request):
    # get form slot add dan for slot update
    formaddslot = FormAddSlot(auto_id=True)
    formupdateslot = FormUpdateSlot(auto_id=True)
    context = {
        'formaddslot': formaddslot,
        'formupdateslot': formupdateslot
    }
    return render(request, 'jadwalkuliah/main.html', context)
