from django import forms


class FormAddSlot(forms.Form):
    hari = forms.CharField(label='Hari', max_length=7)
    jam = forms.CharField(label='Jam', max_length=13)
    ruang = forms.CharField(label='Ruang', max_length=10)
    matkul = forms.CharField(label='Mata Kuliah', max_length=60)
    kelas = forms.CharField(label='Kelas', max_length=1)


class FormUpdateSlot(forms.Form):
    hariUp = forms.CharField(label='Hari', max_length=7)
    jamUp = forms.CharField(label='Jam', max_length=13)
    ruangUp = forms.CharField(label='Ruang', max_length=10)
    matkulUp = forms.CharField(label='Mata Kuliah', max_length=60)
    kelasUp = forms.CharField(label='Kelas', max_length=1)
