from django.db import models


class Jam(models.Model):
    jam_mulai = models.TimeField(
        db_index=True,
        verbose_name="Jam mulai",
        help_text="<em>JJ:MM</em>."
    )
    jam_selesai = models.TimeField(
        verbose_name="Jam selesai",
        help_text="<em>JJ:MM</em>.",
    )

    def __str__(self):
        jam = "%s - %s" % (self.jam_mulai.strftime("%H:%M"), self.jam_selesai.strftime("%H:%M"))
        return jam

    class Meta:
        unique_together = ('jam_mulai', 'jam_selesai')


class Ruangan(models.Model):
    ruang = models.CharField(
        db_index=True,
        help_text="Nama ruang",
        max_length=15,
        unique=True
    )

    def __str__(self):
        return self.ruang
