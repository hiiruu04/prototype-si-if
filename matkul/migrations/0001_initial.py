# Generated by Django 2.1.5 on 2019-07-29 01:20

import ckeditor.fields
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('dosen', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='CPMK',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('cpmk', models.CharField(max_length=255)),
            ],
        ),
        migrations.CreateModel(
            name='KontrakPembelajaran',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('manfaat_pembelajaran', models.TextField(blank=True, null=True)),
                ('deskripsi_pembelajaran', models.TextField(blank=True, null=True)),
                ('tugas', models.TextField(blank=True, null=True)),
                ('strategi_pembelajaran', ckeditor.fields.RichTextField(blank=True, null=True)),
                ('kriteria_penilaian', ckeditor.fields.RichTextField(blank=True, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Kurikulum',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('tahun', models.PositiveIntegerField()),
                ('kaprodi', models.CharField(blank=True, max_length=50, null=True)),
                ('dekan', models.CharField(blank=True, max_length=50, null=True)),
                ('rektor', models.CharField(blank=True, max_length=50, null=True)),
                ('prodi', models.CharField(max_length=50)),
                ('departemen', models.CharField(blank=True, max_length=50, null=True)),
                ('fakultas', models.CharField(blank=True, max_length=50, null=True)),
                ('universitas', models.CharField(blank=True, max_length=50, null=True)),
                ('deskripsi', models.TextField(blank=True, null=True)),
            ],
            options={
                'ordering': ('-tahun',),
            },
        ),
        migrations.CreateModel(
            name='Matkul',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('kd_matkul', models.CharField(blank=True, max_length=10, null=True)),
                ('nama_matkul', models.CharField(max_length=70)),
                ('sks', models.PositiveIntegerField(blank=True, null=True)),
                ('semester', models.PositiveIntegerField(blank=True, null=True)),
                ('kompetensi', models.CharField(blank=True, choices=[('Lainnya', 'Lainnya'), ('Pilihan', 'Pilihan'), ('Utama', 'Utama'), ('Pendukung', 'Pendukung')], max_length=10, null=True)),
                ('cpmk', models.ManyToManyField(blank=True, null=True, to='matkul.CPMK')),
                ('kurikulum', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='matkul.Kurikulum')),
                ('prasyarat', models.ManyToManyField(blank=True, null=True, related_name='prasyarat_set', to='matkul.Matkul')),
            ],
        ),
        migrations.CreateModel(
            name='Pengampu',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('kelas', models.CharField(max_length=2)),
                ('dosen_pengampu', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='dosen_pengampu_1', to='dosen.Dosen')),
                ('dosen_pengampu_2', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='dosen_pengampu_2', to='dosen.Dosen')),
                ('mata_kuliah', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='matkul.Matkul')),
            ],
        ),
        migrations.AlterUniqueTogether(
            name='kurikulum',
            unique_together={('tahun',)},
        ),
        migrations.AddField(
            model_name='kontrakpembelajaran',
            name='matkul',
            field=models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='matkul.Matkul'),
        ),
        migrations.AddField(
            model_name='cpmk',
            name='kurikulum',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='matkul.Kurikulum'),
        ),
        migrations.AlterUniqueTogether(
            name='pengampu',
            unique_together={('dosen_pengampu', 'dosen_pengampu_2', 'mata_kuliah', 'kelas')},
        ),
        migrations.AlterUniqueTogether(
            name='matkul',
            unique_together={('kd_matkul',)},
        ),
    ]
