from django import forms
from django.forms import DateInput, DateField
from users.models import User
from .models import Kurikulum, Matkul, KontrakPembelajaran, Pengampu, CPMK
from django.contrib.auth.forms import UserCreationForm
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Fieldset, ButtonHolder, Submit, Row, Column, Button, Field, Fieldset, Div, HTML
from crispy_forms.bootstrap import FormActions, TabHolder, Tab
from betterforms.multiform import MultiModelForm
from django.urls import reverse

class KurikulumForm (forms.ModelForm):
    class Meta():
        model = Kurikulum
        exclude = ['universitas']
    
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.attrs = {'enctype':'multipart/form-data'}
        self.helper.layout = Layout(
            Row(
                Column('tahun', css_class='form-group col-6 col-md-6 mb-0'),
                Column('prodi', css_class='form-group col-6 col-md-6 mb-0'),
                css_class='form-row'
            ),
            Row(
                Column('departemen', css_class='form-group col-6 col-md-6 mb-0'),
                Column('kaprodi', css_class='form-group col-6 col-md-6 mb-0'),
                css_class='form-row'
            ),
            Row(
                Column('fakultas', css_class='form-group col-6 col-md-6 mb-0'),
                Column('dekan', css_class='form-group col-6 col-md-6 mb-0'),
                css_class='form-row'
            ),
            Row(
                Column('deskripsi', css_class='form-group col-12 col-md-12 mb-0'),
                css_class='form-row'
            ),
            FormActions(
                Submit('submit', 'Simpan'),
                Button('cancel', 'Batal'),
                css_class = 'form-row'
            ),
        )


class CPMKForm (forms.ModelForm):
    class Meta():
        model = CPMK
        exclude = ()

class MatkulForm (forms.ModelForm):
    class Meta():
        model = Matkul
        exclude = ['kurikulum']
        labels = {
            'cpmk': 'Capaian Pembelajaran Mata Kuliah',
        }

    def __init__(self, kur, pk=None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['cpmk'] = forms.ModelMultipleChoiceField(
            widget = forms.CheckboxSelectMultiple(),
            queryset=CPMK.objects.filter(kurikulum__tahun=kur),
            required=False
        )
        if pk is not None:
            self.fields['prasyarat'] = forms.ModelMultipleChoiceField(
                widget = forms.CheckboxSelectMultiple(),
                queryset=Matkul.objects.filter(kurikulum__tahun=kur).exclude(id=pk),
                required=False
            )
        else:
            self.fields['prasyarat'] = forms.ModelMultipleChoiceField(
                widget = forms.CheckboxSelectMultiple(),
                queryset=Matkul.objects.filter(kurikulum__tahun=kur),
                required=False
            )
        self.helper = FormHelper()
        self.helper.attrs = {'enctype':"multipart/form-data",}
        self.helper.layout = Layout(
            Row(
                Div('kd_matkul',
                    'nama_matkul',
                    Row(
                        Column('sks', css_class='form-group col-md-3 mb-0'),
                        Column('semester', css_class='form-group col-md-3 mb-0'),
                        Column('kompetensi', css_class='form-group col-md-6 mb-0'),
                        css_class='form-row'
                    ),
                    css_class="form-group col-md-6 mb-0"),
                Div(
                    Div(
                        Div(
                            'prasyarat',
                            css_class='card-body'
                        ),
                        css_class="card scrollbar scrollbar-primary",
                    ),
                    css_class="form-group col-md-6 mb-0"),
            ),
            Row(
                Div(
                    Div(
                        Div(
                            'cpmk',
                            css_class='card-body'
                        ),
                        css_class="card scrollbar scrollbar-primary mb-2 mt-2",
                    ),
                    css_class="form-group col-md-12"),
            ),
            FormActions(
                Submit('submit', 'Save'),
                Button('cancel', 'Cancel'),
                css_class='px-1'
            ),
            
        )

    def clean(self):
        cd = super().clean()
        curr_name = cd.get('nama_matkul')
        curr_matkul = self.instance.pk
        qs = Matkul.objects.filter(nama_matkul=curr_name)
        qs = qs.exclude(pk=curr_matkul)
        if len(qs)>0:
            raise forms.ValidationError('Nama Mata kuliah sudah ada')        
        


#class UpdateSilabusForm(forms.ModelForm):
    #

class UpdateKontrakPembelajaranForm(forms.ModelForm):
    # tugas_kecil = forms.CharField(max_length=200, widget=forms.Textarea, required=False)
    # tugas_besar = forms.CharField(max_length=200, widget=forms.Textarea, required=False)
    # jenis_penilaian = forms.CharField(max_length=100, required=False)
    # bobot_penilaian = forms.IntegerField(min_value=0, max_value=100, required=False)
    # rentang_nilai = forms.BooleanField(required=False)

    class Meta():
        model = KontrakPembelajaran
        exclude = ['matkul',]
        widgets = {
            # 'tugas': forms.HiddenInput(),
            # 'kriteria_penilaian': forms.HiddenInput(),
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)   
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.disable_csrf = True
        self.helper.layout = Layout(
            Tab('Manfaat Pembelajaran',
                'manfaat_pembelajaran',
                css_class='active show'
            ),
            Tab('Deskripsi Pembelajaran',
                'deskripsi_pembelajaran',
            ),
            Tab('Strategi Pembelajaran',
                'strategi_pembelajaran',
            ),
            Tab('Tugas',
                Field('tugas', css_class="noresize", rows="10"),
            ),
            Tab('Kriteria Penilaian',
                'kriteria_penilaian',
                Div(
                    Field(
                        HTML("""<p>*) Jika text editor kosong atau belum ada format yang semestinya, Salin format HTML pada link 
                        <a data-toggle="modal" data-target="#MyModal" href="#MyModal"> link/kriteria_penilaian.txt </a> untuk mendapatkan format yang semestinya, 
                        kemudian gunakan menu "Source" pada text editor diatas untuk menempelkan format HTML yang disalin. </p>""")
                    ),
                ),
            ),
            Tab('Data dari RPS',
                Row(
                    Column(
                        Field(
                            HTML("""{% load custom_tags %} {% get_data_rps_konpem id_matkul=id %}""")
                    )),
                ),
            ),
            FormActions(
                    Submit('submit', 'Save'),
                    Button('cancel', 'Cancel'),
                    css_class = 'form-row',
            ),
        )
        


# class MatkulSilabusForm(forms.ModelForm):
#     class Meta():
#         model = Silabus
#         exclude = ()

# MatkulSilabusFormSet = forms.inlineformset_factory(Matkul, Silabus, form=MatkulSilabusForm, extra=1) 

