from django.contrib import admin
from .models import Matkul, Kurikulum, KontrakPembelajaran, Pengampu, CPMK


# Register your models here.

admin.site.register(Kurikulum)

admin.site.register(CPMK)

admin.site.register(KontrakPembelajaran)


class PengampuInline(admin.StackedInline):
    model = Pengampu

class KontrakPembelajaranInline(admin.StackedInline):
    model = KontrakPembelajaran

@admin.register(Matkul)
class MatkulAdmin(admin.ModelAdmin):
    inlines = [KontrakPembelajaranInline, PengampuInline]

@admin.register(Pengampu)
class PengampuAdmin(admin.ModelAdmin):
    list_display = ("mata_kuliah", "kelas", "dosen_pengampu", "dosen_pengampu_2")
