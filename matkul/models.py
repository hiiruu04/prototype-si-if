from django.db import models
from dosen.models import Dosen
from ckeditor.fields import RichTextField
from bs4 import BeautifulSoup


# Create your models here.

class Kurikulum (models.Model):
    tahun = models.PositiveIntegerField()
    kaprodi = models.CharField(max_length=50, null=True, blank=True)
    dekan = models.CharField(max_length=50, null=True, blank=True)
    rektor = models.CharField(max_length=50, null=True, blank=True)
    prodi = models.CharField(max_length=50)
    departemen = models.CharField(max_length=50, null=True, blank=True)
    fakultas = models.CharField(max_length=50, null=True, blank=True)
    universitas = models.CharField(max_length=50, null=True, blank=True)
    deskripsi = models.TextField(null=True, blank=True)

    def __str__(self):
        kurikulum = "Kurikulum "+str(self.tahun)+" - "+self.prodi
        return kurikulum

    class Meta:
        unique_together = ('tahun',)
        ordering = ('-tahun',)

class CPMK (models.Model):
    kurikulum = models.ForeignKey(Kurikulum, on_delete=models.CASCADE, default=Kurikulum.objects.all().order_by('-tahun')[0].id)
    cpmk = models.CharField(max_length=255)

    def __str__(self):
        return self.cpmk

class Matkul (models.Model):
    KOMPETENSI_CHOICES = {
        ('Utama','Utama'),
        ('Pendukung','Pendukung'),
        ('Pilihan','Pilihan'),
        ('Lainnya','Lainnya'),
    }
    kd_matkul = models.CharField(max_length=10, null=True, blank=True)
    nama_matkul = models.CharField(max_length=70)
    kurikulum = models.ForeignKey(Kurikulum, on_delete=models.CASCADE)
    sks = models.PositiveIntegerField(blank=True, null=True)
    semester = models.PositiveIntegerField(blank=True, null=True)
    cpmk = models.ManyToManyField(CPMK, blank=True, null=True)
    kompetensi = models.CharField(choices=KOMPETENSI_CHOICES, max_length=10, null=True, blank=True)
    prasyarat = models.ManyToManyField("self", symmetrical=False, related_name="prasyarat_set", null=True, blank=True)

    def __str__(self):
        return self.nama_matkul

    class Meta:
        unique_together = ('kd_matkul',)

class KontrakPembelajaran (models.Model):
    matkul = models.OneToOneField(Matkul, on_delete=models.CASCADE)
    manfaat_pembelajaran = models.TextField(null=True, blank=True)
    deskripsi_pembelajaran = models.TextField(null=True, blank=True)
    # jadwal_pembelajaran = RichTextField(config_name='tabeling_ckeditor', null=True, blank=True) # ngko takonke , nak gak (format, tabel, ul ol, BIU, undoredo, link, A color)
    # organisasi_materi = RichTextField(null=True, blank=True)
    tugas = models.TextField(null=True, blank=True)
    strategi_pembelajaran = RichTextField(config_name='numbering_ckeditor', null=True, blank=True)
    kriteria_penilaian = RichTextField(config_name='tabeling_ckeditor', null=True, blank=True)
    # deskripsi_matkul = models.TextField(null=True, blank=True)
    # referensi = RichTextField(null=True, blank=True) # (format, ul, BIU, undoredo, link)
    # capaian_pembelajaran = RichTextField(null=True, blank=True)
    # ditampilkan pengampu

    def __str__(self):
        return self.matkul.nama_matkul

class Pengampu (models.Model):
    dosen_pengampu = models.ForeignKey(Dosen, related_name="dosen_pengampu_1", on_delete=models.CASCADE, null=True, blank=True)
    dosen_pengampu_2 = models.ForeignKey(Dosen, related_name="dosen_pengampu_2", on_delete=models.CASCADE, null=True, blank=True)
    mata_kuliah = models.ForeignKey(Matkul, on_delete=models.CASCADE)
    kelas = models.CharField(max_length=2)

    def __str__(self):
        nama_dosen = self.dosen_pengampu.fullnamagelar()
        return nama_dosen

    class Meta:
        unique_together = ('dosen_pengampu', 'dosen_pengampu_2', 'mata_kuliah', 'kelas')
