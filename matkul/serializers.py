from rest_framework import serializers
from .models import Matkul, Pengampu


class MatkulsSerializer(serializers.ModelSerializer):
    kurikulum = serializers.StringRelatedField()
    #cpmk = serializers.StringRelatedField()
    #prasyarat = serializers.StringRelatedField()

    class Meta:
        model = Matkul
        fields = ("kd_matkul", "nama_matkul", "kurikulum", "sks", "semester", "cpmk", "kompetensi", "prasyarat")

class PengampusSerializer(serializers.ModelSerializer):
    dosen_pengampu = serializers.StringRelatedField()
    dosen_pengampu_2 = serializers.StringRelatedField()
    mata_kuliah = serializers.StringRelatedField()

    class Meta:
        model = Pengampu
        fields = ("dosen_pengampu", "dosen_pengampu_2", "mata_kuliah", "kelas")
