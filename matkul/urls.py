from django.urls import path
from . import views as matkul_views

urlpatterns = [
    path('list-matkul/', matkul_views.MatkulListView.as_view(), name='matkul_list'),
    path('<kur>/list-matkul/', matkul_views.MatkulListView.as_view(), name='matkul_list_lama'),
    path('<kur>/create-matkul/', matkul_views.MatkulCreateView.as_view(), name='matkul_create'),
    # path('create-matkul- silabus/', matkul_views.MatkulSilabusCreateView.as_view(), name='matkul_silabus_create'),    
    path('<kur>/<pk>/delete-matkul/', matkul_views.MatkulDeleteView.as_view(), name='matkul_delete'),
    path('<kur>/<pk>/update-matkul/', matkul_views.MatkulUpdateView.as_view(), name='matkul_update'),
    # path('<pk>/update-matkul-silabus/', matkul_views.MatkulSilabusUpdateView.as_view(), name='matkul_silabus_update'),
    # path('<kur>/<pk>/detail-matkul/', matkul_views.MatkulDetailView.as_view(), name='matkul_lama_detail'),
    path('<kur>/<pk>/detail-matkul/', matkul_views.MatkulDetailView.as_view(), name='matkul_detail'),

    path('list-kurikulum/', matkul_views.KurikulumListView.as_view(), name='kurikulum_list'),
    path('create-kurikulum/', matkul_views.KurikulumCreateView.as_view(), name='kurikulum_create'),
    path('<pk>/delete-kurikulum/', matkul_views.KurikulumDeleteView.as_view(), name='kurikulum_delete'),
    path('<pk>/update-kurikulum/', matkul_views.KurikulumUpdateView.as_view(), name='kurikulum_update'),
    # path('<pk>/detail-kurikulum/', matkul_views.KurikulumDetailView.as_view(), name='kurikulum_detail'),

    path('<pk>/list-cpmk/', matkul_views.CPMKListView.as_view(), name='cpmk_list'),
    path('create-cpmk/', matkul_views.CPMKCreateView.as_view(), name='cpmk_create'),
    path('<pk>/delete-cpmk/', matkul_views.CPMKDeleteView.as_view(), name='cpmk_delete'),
    path('<pk>/update-cpmk/', matkul_views.CPMKUpdateView.as_view(), name='cpmk_update'),
    
    # path('<thn>/list-matkul-lama', matkul_views.MatkulLamaListView.as_view(), name='matkul_lama_detail'),
    # path('<thn>/create-matkul-lama/', matkul_views.MatkulLamaCreateView.as_view(), name='matkul_lama_create'),
    # # path('<thn>/create-matkul-silabus-lama/', matkul_views.MatkulSilabusLamaCreateView.as_view(), name='matkul_silabus_lama_create'),
    # path('<thn>/<pk>/delete-matkul-lama/', matkul_views.MatkulLamaDeleteView.as_view(), name='matkul_lama_delete'),
    # path('<thn>/<pk>/update-matkul-lama/', matkul_views.MatkulLamaUpdateView.as_view(), name='matkul_lama_update'),
    # # path('<thn>/<pk>/update-matkul-silabus-lama/', matkul_views.MatkulSilabusLamaUpdateView.as_view(), name='matkul_silabus_lama_update'),
    # path('<thn>/<pk>/detail-matkul-lama/', matkul_views.MatkulLamaDetailView.as_view(), name='matkul_lama_detail'),

    path('<pk>/edit-kontrak/', matkul_views.KontrakPembelajaranEditView.as_view(), name='kontrak_pembelajaran_edit'),


    ### DOSEN ###
    # path('create-silabus/', matkul_views.SilabusCreateView.as_view(), name='silabus_create'),    
    # path('<pk>/delete-silabus/', matkul_views.SilabusDeleteView.as_view(), name='silabus_delete'),
    # path('<pk>/update-silabus/', matkul_views.SilabusUpdateView.as_view(), name='silabus_update'),
    # path('<pk>/detail-silabus/', matkul_views.SilabusDetailView.as_view(), name='silabus_detail'),

]