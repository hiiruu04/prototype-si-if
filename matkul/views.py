from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import Group
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.shortcuts import render, redirect, get_object_or_404
from django.views.generic import View, TemplateView, DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic.list import ListView
from django.urls import reverse, reverse_lazy
from django.conf import settings
from users.models import User
from .models import Kurikulum, Matkul, KontrakPembelajaran, Pengampu, CPMK
from dosen.models import Dosen
from rps.models import RPS, Anggota
from rps.views import get_data_rps
from getter.get import get_matkul, get_data_matkul, matkul_per_semester, get_data_kurikulum, get_pengampu
from .forms import KurikulumForm, MatkulForm, UpdateKontrakPembelajaranForm, CPMKForm #, MatkulSilabusFormSet
from django.views.generic.list import ListView
from dosen.views import OwnerMixin, OwnerEditMixin, PerfGroup
from rest_framework import generics
from .serializers import MatkulsSerializer, PengampusSerializer

# Create your views here.
def curr_curriculum():
    """
        Fungsi yang digunakan untuk mendapatkan kurikulum terbaru (dengan nilai tahun terbesar)
    """
    kurikulum = Kurikulum.objects.all().order_by("-tahun")
    curr = kurikulum[0]
    return curr

class OwnerPengampuMixin(object):
    """
        Class yang digunakan untuk mendapatkan pengampu untuk matkul yang dipilih
    """
    def getPengampu(self):
        my_dosen = Dosen.objects.filter(user=self.request.user)
        my_pengampu = Pengampu.objects.filter(dosen_pengampu=my_dosen[0])
        my_matkuls = list()
        for i in range(0,len(my_pengampu)):
            my_matkul = Matkul.objects.filter(id=my_pengampu[i].mata_kuliah.id)
            my_matkuls.append(my_matkul[0])
        return my_matkuls
    def get_queryset(self):
        qs = super(OwnerPengampuMixin, self).get_queryset()
        try:
            group = self.request.user.groups.all()[0].name
            if 'Operator' in group :
                return qs.filter(matkul=self.get_myobj().matkul)
            my_matkuls = self.getPengampu()
            if self.get_myobj().matkul in my_matkuls:
                my_matkul = self.get_myobj().matkul
                return qs.filter(matkul=my_matkul)
            else :
                raise Http404("Anda tidak dapat mengubah kontrak pembelajaran ini")
        except IndexError:
            raise Http404("Anda belum mengampu matakuliah apapuns")

class OwnerPengampuEditMixin(object):
    """
        Class yang digunakan untuk mengatur hanya pengampu saja yang dapat
        mengedit kontrak pembelajaran untuk tiap matkul
    """
    def getPengampu(self):
        my_dosen = Dosen.objects.filter(user=self.request.user)
        my_pengampu = Pengampu.objects.filter(dosen_pengampu=my_dosen[0])
        my_matkul = Matkul.objects.filter(id=my_pengampu[0].mata_kuliah.id)
        return my_matkul[0]
    def form_valid(self, form):
        try:
            group = self.request.user.groups.all()[0].name
            if 'Operator' in group :
                form.instance.matkul=self.get_myobj().matkul
            else :
                my_matkuls = self.getPengampu()
                if self.get_myobj().matkul in my_matkuls:
                    form.instance.matkul=self.get_myobj().matkul
                else :
                    raise Http404("Anda tidak dapat mengubah kontrak pembelajaran ini")
            return super(OwnerPengampuEditMixin, self).form_valid(form)
        except IndexError:
            raise Http404("Anda belum mengampu matakuliah apapuns")

class KurikulumListView(ListView):
    """
        Class yang digunakan untuk menampilkan daftar kurikulum
    """
    model = Kurikulum
    template_name = "manage/list-kurikulum.html"

class KurikulumCreateView(PerfGroup, CreateView):
    """
        Class yang digunakan untuk membuat kurikulum baru
    """
    form_class = KurikulumForm
    template_name = "manage/kurikulum/form-create-kurikulum.html"
    success_url = reverse_lazy('kurikulum_list')
    permission_required = "matkul.add_kurikulum"

    def get_tahun(request):
        curr = curr_curriculum()
        data = {
            'kur': curr
        }
        return JsonResponse(data)

    def form_valid(self, form):
        c = {'form': form, }
        kurikulum = form.save(commit=False)
        kurikulum.universitas = 'Diponegoro'
        kurikulum.save()
        return super(KurikulumCreateView, self).form_valid(form)


class KurikulumUpdateView(PerfGroup, UpdateView):
    """
        Class yang digunakan untuk mengedit kurikulum yang sudah ada
    """
    model = Kurikulum
    form_class = KurikulumForm
    template_name = "manage/kurikulum/form-edit-kurikulum.html"
    success_url = reverse_lazy('kurikulum_list')
    permission_required = "matkul.change_kurikulum"

class KurikulumDeleteView(PerfGroup, DeleteView):
    """
        Class yang digunakan untuk menghapus kurikulum yang ada
    """
    model = Kurikulum
    template_name = "manage/kurikulum/delete-kurikulum.html"
    success_url = reverse_lazy('kurikulum_list')
    permission_required = "matkul.delete_kurikulum"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        kurikulum = self.get_object()
        context['kurikulum'] = kurikulum.__str__()
        return context

class CPMKListView(ListView):
    """
        Class yang digunakan untuk menampilkan daftar CPMK
    """
    model = CPMK
    template_name = "manage/list-cpmk.html"

    def get_queryset(self):
        qs = super(CPMKListView, self).get_queryset()
        return qs.filter(kurikulum__tahun=self.kwargs['pk'])

class CPMKCreateView(PerfGroup, CreateView):
    """
        Class yang digunakan untuk membuat cpmk baru
    """
    form_class = CPMKForm
    template_name = "manage/cpmk/form-create-cpmk.html"
    success_url = reverse_lazy('cpmk_list')
    permission_required = "matkul.add_cpmk"

    def form_valid(self, form):
        c = {'form': form, }
        cpmk = form.save()
        kurikulum_selected = cpmk.kurikulum.tahun
        self.success_url = reverse_lazy('cpmk_list', kwargs={'pk': kurikulum_selected})
        cpmk.save()
        return super(CPMKCreateView, self).form_valid(form)

class CPMKUpdateView(PerfGroup, UpdateView):
    """
        Class yang digunakan untuk mengedit CPMK yang sudah ada
    """
    model = CPMK
    form_class = CPMKForm
    template_name = "manage/cpmk/form-edit-cpmk.html"
    success_url = reverse_lazy('cpmk_list')
    permission_required = "matkul.change_cpmk"

    def get_success_url(self, **kwargs):
        return reverse_lazy('cpmk_list', kwargs={'pk': self.get_object().kurikulum.tahun})

class CPMKDeleteView(PerfGroup, DeleteView):
    """
        Class yang digunakan untuk menghapus cpmk yang ada
    """
    model = CPMK
    template_name = "manage/cpmk/delete-cpmk.html"
    success_url = reverse_lazy('cpmk_list')
    permission_required = "matkul.delete_cpmk"

    def get_success_url(self, **kwargs):
        return reverse_lazy('cpmk_list', kwargs={'pk': self.get_object().kurikulum.tahun})

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        cpmk = self.get_object()
        context['cpmk'] = cpmk.__str__()
        return context

class MatkulListView(ListView):
    """
        Class yang digunakan untuk menampilkan daftar matakuliah yanng ada (Detail Kurikulum) + operasi CPMK dan Matkul
    """
    model = Matkul
    template_name = "manage/list-matkul.html"

    def dispatch(self, request, *args, **kwargs):
        self.user = None
        if request.user.id:
            id_user = request.user.id
            self.user = User.objects.get(id=id_user)
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        kurikulum = curr_curriculum()
        kuri = None
        smt = []
        pilihan = []
        if 'kur' in self.kwargs:
            kur = self.kwargs['kur']
            kurikulum = Kurikulum.objects.get(tahun=kur)
            if (kurikulum == curr_curriculum()):
                context['is_latest'] = True
            else:
                context['is_latest'] = False
        else:
            kurikulum = curr_curriculum()
            context['is_latest'] = True

        matkuls = get_matkul(kurikulum)
        data = []
        if len(matkuls) > 0:
            for matkul in matkuls:
                makul = get_data_matkul(matkul)
                data.append(makul)
        context["kuri"] = kuri

        if self.user and self.user.groups.filter(name="Operator").exists():
            context["group_op"] = True

        kuri = get_data_kurikulum(kurikulum)

        for i in range(1,9):
            matkul = matkul_per_semester(smt=i, kur=kurikulum, pilihan=False)
            smt.append(matkul)
        matkul_pilihan = get_matkul(kurikulum, pilihan=True)

        if len(matkul_pilihan) > 0:
            for curr_matkul in matkul_pilihan:
                mk = get_data_matkul(curr_matkul)
                pilihan.append(mk)

        context["semesters"] = smt
        context["kuri"] = kuri
        context["pilihans"] = pilihan
        context["range"] = range(3)
        return context

class MatkulCreateView(PerfGroup, CreateView):
    """
        Class yang akan digunakan untuk membuat matakuliah baru
    """
    form_class = MatkulForm
    template_name = "manage/matkul/form-create-matkul.html"
    permission_required = "matkul.add_matkul"

    def get_form_kwargs(self):
        """This method is what injects forms with their keyword
            arguments."""
        # grab the current set of form #kwargs
        kwargs = super(MatkulCreateView, self).get_form_kwargs()
        # Update the kwargs with the user_id
        kwargs['kur'] = self.kwargs['kur']
        return kwargs

    def form_valid(self, form):
        c = {'form': form, }
        matkul = form.save(commit=False)
        matkul.kurikulum = Kurikulum.objects.get(tahun=self.kwargs['kur'])
        kurikulum_selected = matkul.kurikulum.tahun
        matkul.save()
        # form.save()
        self.success_url = reverse_lazy('matkul_list_lama', kwargs={'kur': kurikulum_selected})
        # Create Kontrak Pembelajaran
        KontrakPembelajaran.objects.create(
            matkul=matkul,
        )
        RPS.objects.create(
            matkul=matkul,
        )
        return super(MatkulCreateView, self).form_valid(form)

class MatkulUpdateView(PerfGroup, UpdateView):
    """
        Class yang akan digunakan untuk mengedit matakuliah yang sudah ada
    """
    model = Matkul
    form_class = MatkulForm
    template_name = "manage/matkul/form-edit-matkul.html"
    success_url = reverse_lazy('matkul_list')
    permission_required = "matkul.change_matkul"

    def get_form_kwargs(self):
        """This method is what injects forms with their keyword
            arguments."""
        # grab the current set of form #kwargs
        kwargs = super(MatkulUpdateView, self).get_form_kwargs()
        # Update the kwargs with the user_id
        kwargs['kur'] = self.kwargs['kur']
        kwargs['pk'] = self.kwargs['pk']
        return kwargs

class MatkulDeleteView(PerfGroup, DeleteView):
    """
        Class yang digunakan untuk menghapus matakuliah
    """
    model = Matkul
    template_name = "manage/matkul/delete-matkul.html"
    success_url = reverse_lazy('matkul_list')
    permission_required = "matkul.delete_matkul"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        matkul = self.get_object()
        context['matkul'] = matkul.nama_matkul
        return context

class MatkulDetailView(DetailView):
    """
        Class yang digunakan untuk melihat detail matakuliah
    """
    model = Matkul
    template_name = "manage/detail-matkul.html"

    def get_form_kwargs(self):
        """This method is what injects forms with their keyword
            arguments."""
        # grab the current set of form #kwargs
        kwargs = super(MatkulDetailView, self).get_form_kwargs()
        # Update the kwargs with the user_id
        kwargs['kur'] = self.kwargs['kur']
        return kwargs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        matkul = self.get_object()
        makul = get_data_matkul(matkul)
        context["matkul"] = makul
        context['pengampu']  = get_pengampu(matkul)[0]
        context['pengampu_id'] = get_pengampu(matkul)[1]
        context["rps"] = None
        kur = self.kwargs['kur']
        kurikulum = Kurikulum.objects.get(tahun=kur)
        kuri = get_data_kurikulum(kurikulum)
        context['kuri'] = kuri
        anggota = None
        # if RPS.objects.filter(matkul=matkul.id).exists():
        #     rps = RPS.objects.get(matkul=matkul.id)
        #     context["rps"] = get_data_rps(rps)
        #     if self.request.user.is_authenticated and Anggota.objects.filter(tim__rps=rps, dosen__user=self.request.user).exists():
        #         anggota = Anggota.objects.get(tim__rps=rps, dosen__user=self.request.user)
        context['anggota'] = anggota
        return context

class KontrakPembelajaranEditView(PerfGroup, OwnerPengampuMixin, OwnerPengampuEditMixin, UpdateView):
    """
        Class yang akan digunakan untuk mengedit kontrak pembelajaran
    """
    model = KontrakPembelajaran
    form_class = UpdateKontrakPembelajaranForm
    template_name = "manage/matkul/kontrak-pembelajaran/form-edit-kontrak-pembelajaran.html"
    # success_url = reverse_lazy('matkul_detail')
    permission_required = "matkul.change_kontrakpembelajaran"

    def get_myobj(self):
        kontrak_pembelajaran = get_object_or_404(KontrakPembelajaran, matkul_id=self.kwargs['pk'])
        return kontrak_pembelajaran

    def get_object(self, queryset=None):
        queryset = self.get_queryset()
        kontrak_pembelajaran = queryset.get()
        return kontrak_pembelajaran

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        pk = self.kwargs['pk']
        context["id"] = pk
        return context

    def get_success_url(self):
        return reverse_lazy('matkul_detail', kwargs={ 'kur': Matkul.objects.get(pk=self.kwargs['pk']).kurikulum.tahun , 'pk': self.kwargs['pk']})

# API Classes
class ListMatkulsView(generics.ListAPIView):
    """
        Menyediakan penanganan metode get untuk matkul
    """
    queryset = Matkul.objects.all()
    serializer_class = MatkulsSerializer

class ListPengampuView(generics.ListAPIView):
    """
        Menyediakan penanganan metode get untuk pengampu
    """
    queryset = Pengampu.objects.all()
    serializer_class = PengampusSerializer

# class MatkulSilabusCreateView(CreateView):
#     model = Matkul
#     # fields = ['kd_matkul','nama_matkul','kurikulum','sks','semester','kompetensi','prasyarat']
#     # template_name = "manage/matkul/form-create-matkul-silabus.html"
#     success_url = reverse_lazy('matkul_detail')

#     def get_context_data(self, **kwargs):
#         data = super(OperatorCreateMatkulView, self).get_context_data(**kwargs)
#         if self.request.POST:
#             data['silabus'] = MatkulSilabusFormSet(self.request.POST)
#         else:
#             data['silabus'] = MatkulSilabusFormSet()
#         return data

#     def form_valid(self, form):
#         context = self.get_context_data()
#         matkulsilabus = context['silabus']
#         with transaction.atomic():
#             self.object = form.save()

#             if matkulsilabus.is_valid():
#                 matkulsilabus.instance = self.object
#                 matkulsilabus.save()
#         return super(OperatorCreateMatkulView, self).form_valid(form)
