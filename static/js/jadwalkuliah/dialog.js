/****************************************************************************/
/* Settingan untuk semua dialog pada halaman jadwal kuliah                  */
/****************************************************************************/
var dialogAddSlot, dialogUpdateSlot, dialogHapusSlot, dialogReset, dialogFileMismatch;

/****************************************************************************/
/* Settingan dialog untuk form tambah slot                                  */
/****************************************************************************/
dialogAddSlot = $("#form-slot-add").dialog({
  autoOpen:false,
  classes: {
    "ui-dialog-titlebar-close":"no-close"
  },
  show:{effect:"blind", duration:200},
  resizable:false,
  draggable:true,
  height:"auto",
  width:400,
  modal:true,
  buttons:{
    "Buat slot baru":addSlot,
    Cancel:function() {
      dialogAddSlot.dialog("close");
    }
  },
  close:function() {
    formAddSubmit[0].reset();
    allFieldsAdd.removeClass("ui-state-error");
    tipsAdd.text("Semua field wajib diisi.");
  }
});

/****************************************************************************/
/* Settingan dialog untuk form update slot                                  */
/****************************************************************************/
dialogUpdateSlot = $("#form-slot-update").dialog({
  autoOpen:false,
  classes: {
    "ui-dialog-titlebar-close":"no-close"
  },
  show:{effect:"blind", duration:200},
  resizable:false,
  draggable:true,
  height:"auto",
  width:400,
  modal:true,
  buttons:{
    "Update slot":updateSlot,
    Cancel:function() {
      dialogUpdateSlot.dialog("close");
    }
  },
  open:function() {
    data = dialogUpdateSlot.data("rowData");

    $("#hariUp").easyAutocomplete({
      data: hariauto,
      list: {
        maxNumberOfElements: 6,
        match: {
          enabled: true
        }
      }
    }).val(data["hari"]);
    $("#jamUp").easyAutocomplete({
      data: jamauto,
      list: {
        maxNumberOfElements: 6,
        match: {
          enabled: true
        }
      }
    }).val(data["jam"]);
    $("#ruangUp").easyAutocomplete({
      data: ruangauto,
      list: {
        maxNumberOfElements: 6,
        match: {
          enabled: true
        }
      }
    }).val(data["ruang"]);
    $("#matkulUp").easyAutocomplete({
      data: matakuliah,
      getValue: "nama_matkul",
      list: {
        maxNumberOfElements: 6,
        match: {
          enabled: true
        }
      }
    }).val(data["matkul"]);
    $("#kelasUp").easyAutocomplete({
      data: kelasauto,
      list: {
        maxNumberOfElements: 6,
        match: {
          enabled: true
        }
      }
    }).val(data["kls"]);
  },
  close:function() {
    formUpdateSubmit[0].reset();
    allFieldsUpdate.removeClass("ui-state-error");
    tipsUpdate.text("Semua field wajib diisi.");
  }
});

/****************************************************************************/
/* Settingan dialog untuk konfirmasi hapus slot                             */
/****************************************************************************/
dialogHapusSlot = $("#confirm-delete-slot").dialog({
  autoOpen:false,
  classes: {
    "ui-dialog-titlebar-close":"no-close"
  },
  show:{effect:"blind", duration:200},
  resizable:false,
  draggable:false,
  height:"auto",
  width:400,
  modal:true,
  buttons:{
    "Hapus":function() {
      jadwal.deleteRow(dialogHapusSlot.data("rowIdx"));
      dialogHapusSlot.dialog("close");
    },
    Cancel:function() {
      dialogHapusSlot.dialog("close");
    }
  }
});

/****************************************************************************/
/* Settingan dialog untuk konfirmasi reset tabel                            */
/****************************************************************************/
dialogReset = $("#confirm-reset").dialog({
  autoOpen:false,
  classes: {
    "ui-dialog-titlebar-close":"no-close"
  },
  show:{effect:"blind", duration:200},
  resizable:false,
  draggable:false,
  height:"auto",
  width:400,
  modal:true,
  buttons:{
    "Reset":function() {
      jadwal.clearData();
      dialogReset.dialog("close");
    },
    Cancel:function() {
      dialogReset.dialog("close");
    }
  }
});

/****************************************************************************/
/* Settingan dialog untuk file tidak sesuai                                 */
/****************************************************************************/
dialogFileMismatch = $("#file-mismatch").dialog({
  autoOpen:false,
  classes: {
    "ui-dialog-titlebar-close":"no-close"
  },
  show:{effect:"blind", duration:200},
  resizable:false,
  draggable:false,
  height:"auto",
  width:600,
  modal:true,
  buttons:{
    Ok:function() {
      dialogFileMismatch.dialog("close");
    }
  }
});
