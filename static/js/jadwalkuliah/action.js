/****************************************************************************/
/* Settingan untuk aksi yang dilakukan di halaman utama jadwal kuliah       */
/* seperti klik pada tombol dan submit form                                 */
/****************************************************************************/
var formAddSubmit, formUpdateSubmit;

/****************************************************************************/
/* Dijalankan setelah menekan tombol buat slot baru pada form tambah slot   */
/****************************************************************************/
formAddSubmit = dialogAddSlot.find("#form-add").on("submit", function(event) {
  event.preventDefault();
  addSlot();
});

/****************************************************************************/
/* Dijalankan setelah menekan tombol update slot pada form update slot      */
/****************************************************************************/
formUpdateSubmit = dialogUpdateSlot.find("#form-update").on("submit", function(event) {
  event.preventDefault();
  updateSlot();
});

/****************************************************************************/
/* Tampilkan form untuk menambah slot baru pada jadwal jika tombol          */
/* "Tambah Slot" diklik                                                     */
/****************************************************************************/
$("#tambah-slot").click(function() {
  dialogAddSlot.dialog("open").keypress(function(e) {
    if(e.keyCode == $.ui.keyCode.ENTER) {
      addSlot();
    }
  });
  $("#hari").easyAutocomplete({
    data: hariauto,
    list: {
      maxNumberOfElements: 6,
      match: {
        enabled: true
      }
    }
  });
  $("#jam").easyAutocomplete({
    data: jamauto,
    list: {
      maxNumberOfElements: 6,
      match: {
        enabled: true
      }
    }
  });
  $("#ruang").easyAutocomplete({
    data: ruangauto,
    list: {
      maxNumberOfElements: 6,
      match: {
        enabled: true
      }
    }
  });
  $("#matkul").easyAutocomplete({
    data: matakuliah,
    getValue: "nama_matkul",
    list: {
      maxNumberOfElements: 6,
      match: {
        enabled: true
      }
    }
  });
  $("#kelas").easyAutocomplete({
    data: kelasauto,
    list: {
      maxNumberOfElements: 6,
      match: {
        enabled: true
      }
    }
  });
});

/****************************************************************************/
/* Tampilkan form konfirmasi reset tabel jika tombol "Reset" diklik         */
/****************************************************************************/
$("#reset").click(function() {
  dialogReset.dialog("open");
});

/****************************************************************************/
/* Settingan untuk save tabel jadwal kuliah                                 */
/****************************************************************************/
$("#export").click(function() {
  // Download sebuah file xlsx yang mempunyai nama sheet "Jadwal_Kuliah"
  if(jadwal["rowManager"]["activeRowsCount"] == 0) {
    null;
  }
  else {
    jadwal.download("xlsx", "Jadwal_Kuliah.xlsx", {sheetName:"Jadwal_Kuliah"});
  }
});data: matakuliah

/****************************************************************************/
/* Settingan untuk load tabel jadwal kuliah                                 */
/****************************************************************************/
$("#import").click(function() {
  // Import sebuah file xlsx untuk dimasukkan kedalam tabel

  // Fungsi untuk mengubah file xlsx menjadi Slot array
  function handleFile(e) {
    var files = e.target.files, f = files[0];
    if(f.type !== "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet") {
      // alert("File tidak sesuai, silahkan pilih file dengan ekstensi .xlsx");
      dialogFileMismatch.dialog("open");
    }
    else {
      var reader = new FileReader();
      reader.onload = function(e) {
        var data = new Uint8Array(e.target.result);
        var workbook = XLSX.read(data, {type: 'array'});

        var sheet_name = workbook.SheetNames[0];
        var worksheet = workbook.Sheets[sheet_name];

        var tempData = XLSX.utils.sheet_to_json(worksheet);
        var counter = 0;
        jadwal.clearData();

        for(var i = 0; i < tempData.length; i++) {
          if(tempData[i].KODE == undefined) {
            null;
          }
          else {
            var slotImport = new Slot();
            slotImport.setHari(tempData[i].HARI);
            slotImport.setJam(tempData[i].JAM);
            slotImport.setRuang(tempData[i].RUANG);
            slotImport.setKode(tempData[i].KODE);
            for(var x = 0; x < matakuliah.length; x++) {
              if(tempData[i].KODE == matakuliah[x].kd_matkul) {
                slotImport.setMatkul(matakuliah[x].nama_matkul);
                slotImport.setSks(matakuliah[x].sks);
                slotImport.setSmt(matakuliah[x].semester);
                slotImport.setKls(tempData[i].KLS);
                for(var y = 0; y < pengampu.length; y++) {
                  if((slotImport.getMatkul() == pengampu[y].mata_kuliah) && (slotImport.getKls() == pengampu[y].kelas)) {
                    slotImport.setDosen1(pengampu[y].dosen_pengampu);
                    slotImport.setDosen2(pengampu[y].dosen_pengampu_2);
                  }
                }
              }
            }
            jadwal.addRow({id:counter, hari:slotImport.getHari(), jam:slotImport.getJam(), ruang:slotImport.getRuang(), kode:slotImport.getKode(), matkul:slotImport.getMatkul(), sks:slotImport.getSks(), smt:slotImport.getSmt(), kls:slotImport.getKls(), dosen:slotImport.getDosenPengampu()}, true);
            counter = counter + 1;
            // Sort jadwal setelah update slot
            jadwal.setSort("jam", "asc");
            jadwal.setSort("hari", "dsc");
          }
        }
      };
      reader.readAsArrayBuffer(f);
    }
  }
  var konfirmasi = confirm("Tabel akan dihapus terlebih dahulu, Lanjut?");
  if(konfirmasi == true) {
    $("#inputfile").trigger("click");
    inputfile.addEventListener('change', handleFile, false);
  }
});
