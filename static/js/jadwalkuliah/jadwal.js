//  Tabulator option example
//	data:tabledata,           //load row data from array
//	layout:"fitColumns",      //fit columns to width of table
//	responsiveLayout:"hide",  //hide columns that dont fit on the table
//	tooltips:true,            //show tool tips on cells
//	addRowPos:"top",          //when adding a new row, add it to the top of the table
//	history:true,             //allow undo and redo actions on the table
//	pagination:"local",       //paginate the datdraggable:truea
//	paginationSize:7,         //allow 7 rows per page of data
//	movableColumns:true,      //allow column order to be changed
//	resizableRows:true,       //allow row order to be changed

var count = 14;
var sampleData = [
  {id: 0, hari: "Senin", jam: "07:30 - 09:10", ruang: "A 303", kode: "AIK21321", matkul: "Algoritma dan Pemrograman", sks: 4, smt: 2, kls: "A", dosen: "1.Aris Puji Widodo\n2.Eko Adi Sarwoko" },
  { id: 1, hari: "Senin", jam: "10:10 - 12:40", ruang: "E 101", kode: "AIK21588", matkul: "Metodologi dan Penelitian Ilmiah", sks: 2, smt: 8, kls: "B", dosen: "1.Retno Kusumaningrum\n2.Sukmawati Nur Endah" },
  { id: 2, hari: "Senin", jam: "15:40 - 18:10", ruang: "E 101", kode: "AIK21342", matkul: "Jaringan Komputer", sks: 3, smt: 4, kls: "C", dosen: "1.Indra Waspada\n2.Edy Suharto" },
  { id: 3, hari: "Selasa", jam: "07:30 - 10:00", ruang: "A 303", kode: "AIK21324", matkul: "Aljabar Linier", sks: 3, smt: 2, kls: "A", dosen: "1.Suhartono\n2.Edy Suharto" },
  { id: 4, hari: "Selasa", jam: "10:10 - 11:50", ruang: "A 103", kode: "AIK21322", matkul: "Organisasi dan Arsitektur Komputer", sks: 4, smt: 2, kls: "B", dosen: "1.Ragil Saputra\n2.Adi Wibowo" },
  { id: 5, hari: "Selasa", jam: "13:00 - 15:30", ruang: "B 101", kode: "AIK21344", matkul: "Grafika dan Komputasi Visual", sks: 3, smt: 4, kls: "A", dosen: "1.Priyo Sidik Sasongko\n2.Helmie Arif Wibawa" },
  { id: 6, hari: "Rabu", jam: "10:10 - 11:50", ruang: "A 303", kode: "AIK21321", matkul: "Algoritma dan Pemrograman", sks: 4, smt: 2, kls: "B", dosen: "1.Aris Puji Widodo\n2.Eko Adi Sarwoko" },
  { id: 7, hari: "Rabu", jam: "13:00 - 15:30", ruang: "E 101", kode: "AIK21343", matkul: "Manajemen Basis Data", sks: 3, smt: 4, kls: "A", dosen: "1.Beta Noranita\n2.Djalal Er Riyanto" },
  { id: 8, hari: "Rabu", jam: "15:40 - 18:10", ruang: "E 102", kode: "AIK21345", matkul: "Rekayasa Perangkat Lunak", sks: 3, smt: 4, kls: "C", dosen: "1.Panji Wisnu Wirawan\n2.Dinar Mutiara Kusumo Nugraheni" },
  { id: 9, hari: "Kamis", jam: "07:30 - 09:10", ruang: "A 303", kode: "AIK21322", matkul: "Organisasi dan Arsitektur Komputer", sks: 4, smt: 2, kls: "A", dosen: "1.Ragil Saputra\n2.Adi Wibowo" },
  { id: 10, hari: "Kamis", jam: "13:00 - 15:30", ruang: "A 303", kode: "AIK21346", matkul: "Sistem Cerdas", sks: 3, smt: 4, kls: "A", dosen: "1.Sukmawati Nur Endah\n2.Khadijah" },
  { id: 11, hari: "Jum'at", jam: "06:30 - 08:10", ruang: "Stadion", kode: "UNW00005", matkul: "Olah Raga", sks: 1, smt: 2, kls: "A", dosen: "1.Endang Kumaidah\n " },
  { id: 12, hari: "Jum'at", jam: "10:10 - 11:50", ruang: "A 303", kode: "AIK21320", matkul: "Matematika II", sks: 2, smt: 2, kls: "A", dosen: "1.Farikhin\n2.Solikhin" },
  { id: 13, hari: "Jum'at", jam: "15:40 - 18:10", ruang: "E 102", kode: "AIK21423", matkul: "Pengenalan Pola", sks: 3, smt: 8, kls: "A", dosen: "1.Rismiyati\n2.Eko Adi Sarwoko" }
];

// buat Tabulator tabel pada container div id "jadwal"
var jadwal = new Tabulator("#jadwal", {
  autoResize:true,          // opsi untuk mengubah ukuran tabel secara otomatis
  resizableColumns:false,   // opsi untuk kolom yang dapat diubah ukurannnya (default = true)
  layout:"fitData",         // tabel secara otomatis mengubah ukuran agar sesuai dengan data
  movableRows: false,       // opsi untuk row agar dapat dipindah-pindah dalam tabel dengan drag-and-drop
  data:sampleData,
  groupBy:"hari",           // grup baris sesuai dengan hari
  columns:[                 // definisi kolom pada tabel
    {title:"id", visible:false},
    {title:"HARI", field:"hari", headerSort:false, visible:true, align:"center", minWidth:70},
    {title:"JAM", field:"jam", sorter: "string", headerSort:false, align:"center", minWidth:100, sorter:"string"},
    {title:"RUANG", field:"ruang", headerSort:false, align:"center", minWidth:60},
    {title:"KODE", field:"kode", headerSort:false, align:"center", minWidth:70},
    {title:"MATA KULIAH", field:"matkul", headerSort:false, minWidth:240},
    {title:"SKS", field:"sks", headerSort:false, align:"center", minWidth:40},
    {title:"SMT", field:"smt", headerSort:false, align:"center", minWidth:40},
    {title:"KLS", field:"kls", headerSort:false, align:"center", minWidth:40},
    {title:"DOSEN PENGAMPU", field:"dosen", headerSort:false, minWidth:300, formatter:"textarea"},
  ],
  // fungsi untuk row apabila row diklik
  rowClick:function(e, row) {
    rowIdx = row.getIndex();
    rowData = row.getData();
    $.contextMenu({
      selector:".tabulator-row",
      trigger:"left",
      autoHide:true,
      items:{
        "edit":{
          name:"Edit",
          icon:"edit",
          callback:function() {
            dialogUpdateSlot.data("rowData", rowData).dialog("open").keypress(function(e) {
              if(e.keyCode == $.ui.keyCode.ENTER) {
                updateSlot();
              }
            });
          }
        },
        "delete":{
          name:"Delete",
          icon:"delete",
          callback:function() {
            dialogHapusSlot.data("rowIdx", rowIdx).dialog("open");
          }
        }
      }
    });
  }
});

/*******************************************************/
/* Variabel untuk label dan tips pada form tambah slot */
/*******************************************************/
var hari = $("#hari"),
  jam = $("#jam"),
  ruang = $("#ruang"),
  matkul = $("#matkul"),
  kls = $("#kelas"),
  allFieldsAdd = $([]).add(hari.labels()).add(jam.labels()).add(ruang.labels()).add(matkul.labels()).add(kls.labels()),
  tipsAdd = $(".validasiAdd");

/*******************************************************/
/* Variabel untuk label dan tips pada form update slot */
/*******************************************************/
var hariUp = $("#hariUp"),
  jamUp = $("#jamUp"),
  ruangUp = $("#ruangUp"),
  matkulUp = $("#matkulUp"),
  klsUp = $("#kelasUp"),
  allFieldsUpdate = $([]).add(hariUp.labels()).add(jamUp.labels()).add(ruangUp.labels()).add(matkulUp.labels()).add(klsUp.labels()),
  tipsUpdate = $(".validasiUpdate");

/*******************************************************/
/* Get data matakuliah dan pengampu dari API           */
/*******************************************************/
var matakuliah = "";
var pengampu = "";
$.getJSON("../api/v1/matkul/?format=json", function(data) {
   matakuliah = data;
});
$.getJSON("../api/v1/pengampu/?format=json", function(data) {
  pengampu = data;
});
