/****************************************************************************/
/* Fungsi untuk memberitahu kesalahan data yang dimasukkan di dalam form    */
/* update slot                                                              */
/****************************************************************************/
function updateTipsUpdate(t) {
  tipsUpdate.text(t).addClass("ui-state-highlight");
  setTimeout(function() {
    tipsUpdate.removeClass("ui-state-highlight", 1500);
  }, 500 );
}

/****************************************************************************/
/* Fungsi untuk mengupdate slot kedalam tabel menggunakan data yang         */
/* disubmit melalui form update slot                                        */
/****************************************************************************/
function updateSlot() {
  var valid = true;
  var id = dialogUpdateSlot.data("rowData")["id"];
  var slot = new Slot();
  allFieldsUpdate.removeClass("ui-state-error");

  valid = valid && validasiHari(hariUp, "update");
  valid = valid && validasiJam(jamUp, "update");
  valid = valid && validasiUpdate(ruangUp, "Ruang");
  valid = valid && validasiUpdate(matkulUp, "Mata Kuliah");
  valid = valid && validasiUpdate(klsUp, "Kelas");

  for(var i = 0; i < matakuliah.length; i++) {
    if(matkulUp.val() == matakuliah[i].nama_matkul) {
      slot.setId(id);
      slot.setHari(hariUp.val());
      slot.setJam(jamUp.val());
      slot.setRuang(ruangUp.val());
      slot.setKode(matakuliah[i].kd_matkul);
      slot.setMatkul(matakuliah[i].nama_matkul);
      slot.setSks(matakuliah[i].sks);
      slot.setSmt(matakuliah[i].semester);
      for(var x = 0; x < pengampu.length; x++) {
        if((matkulUp.val() == pengampu[x].mata_kuliah) && (klsUp.val().toUpperCase() == pengampu[x].kelas)) {
          slot.setKls(pengampu[x].kelas);
          slot.setDosen1(pengampu[x].dosen_pengampu);
          slot.setDosen2(pengampu[x].dosen_pengampu_2);
        }
      }
    }
  };

  valid = valid && validasiSlot(slot, "update");

  if(valid){
    jadwal.updateData([{id:id, hari:slot.getHari(), jam:slot.getJam(), ruang:slot.getRuang(), kode:slot.getKode(), matkul:slot.getMatkul(), sks:slot.getSks(), smt:slot.getSmt(), kls:slot.getKls(), dosen:slot.getDosenPengampu()}]);

    dialogUpdateSlot.dialog("close");
    tipsUpdate.text("Semua field wajib diisi.");
  }
  // Sort jadwal setelah update slot
  jadwal.setSort("jam", "asc");
  jadwal.setSort("hari", "dsc");
  return valid;
}
