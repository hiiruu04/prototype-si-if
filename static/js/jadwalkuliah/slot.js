/****************************************************************************/
/* Slot object                                                              */
/****************************************************************************/
class Slot {
  // Constructor untuk slot jadwal
  constructor(id, hari, jamMulai, jamSelesai, ruang, kode, matkul, sks, smt, kls, dosen1, dosen2) {
    this.id = id;
    this.hari = hari;
    this.jamMulai = jamMulai;
    this.jamSelesai = jamSelesai;
    this.ruang = ruang;
    this.kode = kode;
    this.matkul = matkul;
    this.sks = sks;
    this.smt = smt;
    this.kls = kls;
    this.dosen1 = dosen1;
    this.dosen2 = dosen2;
  }

  // Getter dan Setter
  getId() {
    return this.id;
  }

  getHari() {
    return this.hari;
  }

  getJamMulai() {
    return this.jamMulai;
  }

  getJamSelesai() {
    return this.jamSelesai
  }

  getRuang() {
    return this.ruang;
  }

  getKode() {
    return this.kode;
  }

  getMatkul() {
    return this.matkul;
  }

  getSks() {
    return this.sks;
  }

  getSmt() {
    return this.smt;
  }

  getKls() {
    return this.kls;
  }

  getDosen1() {
    return this.dosen1;
  }

  getDosen2() {
    return this.dosen2;
  }

  setId(id) {
    this.id = id;
  }

  setHari(hari) {
    this.hari = hari;
  }

  setJamMulai(jamMulai) {
    this.jamMulai = jamMulai;
  }

  setJamSelesai(jamSelesai) {
    this.jamSelesai = jamSelesai;
  }

  setRuang(ruang) {
    this.ruang = ruang;
  }

  setKode(kode) {
    this.kode = kode;
  }

  setMatkul(matkul) {
    this.matkul = matkul;
  }

  setSks(sks) {
    this.sks = sks;
  }

  setSmt(smt) {
    this.smt = smt;
  }

  setKls(kls) {
    this.kls = kls;
  }

  setDosen1(dosen1) {
    this.dosen1 = dosen1;
  }

  setDosen2(dosen2) {
    this.dosen2 = dosen2;
  }

  // Custom Getter and Setter
  getJam() {
    return this.getJamMulai() + ' - ' + this.getJamSelesai();
  }

  setJam(jam) {
    var patternJam = /^([0-1][0-9]:[0-5][0-9]|[2][0-3]:[0-5][0-9])\s-\s([0-1][0-9]:[0-5][0-9]|[2][0-3]:[0-5][0-9])$/;
    if(patternJam.test(jam) == true) {
      this.setJamMulai(jam.split("-")[0].replace(/\s/g, ''));
      this.setJamSelesai(jam.split("-")[1].replace(/\s/g, ''));
    }
    else {
      this.setJamMulai("jam not valid");
      this.setJamSelesai("jam not valid");
    }
  }

  getDosenPengampu() {
    if(this.getDosen2() !== null) {
      return '1.' + this.getDosen1() + '\n2.' + this.getDosen2();
    }
    else {
      return '1.' + this.getDosen1() + '\n ';
    }
  }

  setDosenPengampu(dosenPengampu) {
    this.setDosen1(dosenPengampu.split("\n")[0].replace(/1./g, ''));
    this.setDosen2(dosenPengampu.split("\n")[1].replace(/2./g, ''));
  }

  // toString method
  toString() {
    return 'id: ' + this.getId() + ',hari: ' + this.getHari() + ', jam: ' + this.getJam() + ', ruang: ' + this.getRuang() + ', kode: ' + this.getKode() + ', matkul: ' + this.getMatkul() + ', sks: ' + this.getSks() + ', smt: ' + this.getSmt() + ', kls: ' + this.getKls() + ', dosen1: ' + this.getDosen1() + ', dosen2: ' + this.getDosen2();
  }
}
