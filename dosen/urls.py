from django.urls import path
from django.contrib.auth.views import LogoutView
from django.contrib.auth import views as auth_views
from users.views import LoginView, ChangePasswordView, ChangePasswordDoneView, ResetPasswordView
from . import views as dosen_views

from dosen import utils

urlpatterns = [
    # otentikasi
    path('login/', LoginView.as_view(), name="login"),
    path('logout/', LogoutView.as_view(), name="logout"), 
    path('authenticate/',dosen_views.authenticateUser, name="authenticate"),
    # staf umum
    path('dashboard/',dosen_views.dashboardStaff, name="dashboard"),
    path('list/', dosen_views.ManageDosenListView.as_view(), name='dosen_list'),
    path('list/<lab>', dosen_views.ManageDosenListView.as_view(), name='dosen_list_lab'),
    path('password_change/',ChangePasswordView.as_view(),name='password_change'),
    path('password_change/done/',ChangePasswordDoneView.as_view(),name='password_change_done'),
    path('password_reset/',ResetPasswordView.as_view(),name='password_reset'),
    path('password_reset_done',auth_views.PasswordResetDoneView.as_view(),name='password_reset_done'),
    path('reset/<uidb64>/<token>/',auth_views.PasswordResetConfirmView.as_view(),name='password_reset_confirm'),
    path('reset/done/',auth_views.PasswordResetCompleteView.as_view(),name='password_reset_complete'),
    # operator
    path('create/', dosen_views.OperatorCreateDosenView.as_view(), name='dosen_create'),
    path('view/data-dosen', dosen_views.OperatorDataDosen, name="view_data_dosen"),
    path('<pk>/delete/', dosen_views.OperatorDeleteDosenView.as_view(), name='dosen_delete'),
    path('<pk>/update/', dosen_views.OperatorUpdateDosenView.as_view(), name='dosen_update'),
    path('print/data-dosen', dosen_views.printDataDosenCSV, name="print_data_dosen"),
    # dosen
    path('print/data-dosen/<int:dosen_id>',dosen_views.printDosenCSV, name="dosen_print"),
    path('<pk>/update-dosen/', dosen_views.DosenUpdateDataView.as_view(), name='dosen_update_dosen'),
    path('<pk>/detail/', dosen_views.DosenDetailView.as_view(), name='dosen_detail'),
    path('<pk>/to_pdf/', dosen_views.DosenToPDF.as_view(), name='dosen_to_pdf'),
    # ijazah
    path('<pk>/create/ijazah', dosen_views.CreateIjazahView.as_view(), name='dosen_ijazah'),
    path('<dosen>/update/ijazah/<pk>', dosen_views.UpdateIjazahView.as_view(), name='dosen_ijazah_update'),
    path('<dosen>/delete/ijazah/<pk>', dosen_views.DeleteIjazahView.as_view(), name='dosen_ijazah_delete'),
    # sk
    path("<pk>/create/sk", dosen_views.CreateSKJabatanView.as_view(), name="dosen_sk"),
    path("<dosen>/update/sk/<pk>", dosen_views.UpdateSKJabatanView.as_view(), name="dosen_sk_update"),
    path("<dosen>/delete/sk/<pk>", dosen_views.DeleteSKJabatanView.as_view(), name="dosen_sk_delete"),
    # Jabatan
    path("jabatan/list", dosen_views.ListJabatanView.as_view(), name="jabatan_list"),
    path("jabatan/create", dosen_views.CreateJabatanView.as_view(), name="jabatan_create"),
    path("jabatan/<pk>/update", dosen_views.UpdateJabatanView.as_view(), name="jabatan_update"),
    path("jabatan/<pk>/delete", dosen_views.DeleteJabatanView.as_view(), name="jabatan_delete"),
    # Golongan
    path("golongan/list", dosen_views.ListGolonganView.as_view(), name="golongan_list"),
    # path("golongan/create", dosen_views.CreateGolonganView.as_view(), name="golongan_create"),
    path("golongan/<pk>/update", dosen_views.UpdateGolonganView.as_view(), name="golongan_update"),
    path("golongan/<pk>/delete", dosen_views.DeleteGolonganView.as_view(), name="golongan_delete"),
    # Status
    path("status/list", dosen_views.ListStatusView.as_view(), name="status_list"),
    path("status/create", dosen_views.CreateStatusView.as_view(), name="status_create"),
    path("status/<pk>/update", dosen_views.UpdateStatusView.as_view(), name="status_update"),
    path("status/<pk>/delete", dosen_views.DeleteStatusView.as_view(), name="status_delete"),
    # Lab
    path("lab/list", dosen_views.ListLabView.as_view(), name="lab_list"),
    path("lab/create", dosen_views.CreateLabView.as_view(), name="lab_create"),
    path("lab/<pk>/detail", dosen_views.LabDetailView.as_view(), name="lab_detail"),
    path("lab/<pk>/update", dosen_views.UpdateLabView.as_view(), name="lab_update"),
    path("lab/<pk>/delete", dosen_views.DeleteLabView.as_view(), name="lab_delete"),
    # Strata
    path("strata/list", dosen_views.ListStrataView.as_view(), name="strata_list"),
    path("strata/create", dosen_views.CreateStrataView.as_view(), name="strata_create"),
    path("strata/<pk>/update", dosen_views.UpdateStrataView.as_view(), name="strata_update"),
    path("strata/<pk>/delete", dosen_views.DeleteStrataView.as_view(), name="strata_delete"),
]
