from django.http import HttpResponse, FileResponse
from django.template.loader import get_template
from django.utils.deconstruct import deconstructible
from django.conf import settings
from django.http import HttpResponse
from django.template import Context
from django.template.loader import get_template
from xhtml2pdf import pisa
from io import StringIO, BytesIO
from docx import Document
from docx.shared import Inches
import os
import re
import datetime
import time
from bs4 import BeautifulSoup


BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

def current_year():
    return datetime.date.today().year

def link_callback(uri, rel):
    """
        Convert HTML URIs to absolute system paths so xhtml2pdf can access those
        resources
    """
    # use short variable names
    sUrl = settings.STATIC_URL # Typically /static/
    sRoot = settings.STATIC_ROOT # Typically /home/userX/project_static/
    mUrl = settings.MEDIA_URL # Typically /static/media/
    mRoot = settings.MEDIA_ROOT # Typically /home/userX/project_static/media/
    # convert URIs to absolute system paths
    if uri.startswith(mUrl):
        path = os.path.join(mRoot, uri.replace(mUrl, ""))
    elif uri.startswith(sUrl):
        path = os.path.join(sRoot, uri.replace(sUrl, ""))
    else:
        return uri # handle absolute uri (ie: http://some.tld/foo.png)
    # make sure that file exists
    if not os.path.isfile(path):
        raise Exception(
            'media URI must start with %s or %s' % (sUrl, mUrl)
        )
    return path

# def render_to_pdfs(request):
#     template_path = 'to_pdf.html'
#     context = {'myvar': 'this is your template context'}
#     # Create a Django response object, and specify content_type as pdf
#     response = HttpResponse(content_type='application/pdf')
#     response['Content-Disposition'] = 'attachment; filename="report.pdf"'
#     # find the template and render it.
#     template = get_template(template_path)
#     html = template.render(Context(context))
#     # create a pdf
#     pisaStatus = pisa.CreatePDF(html, dest=response, link_callback=link_callback)
#     # if error then show some funy view
#     if pisaStatus.err:
#         return HttpResponse('We had some errors <pre>' + html + '</pre>')
#     return response

def render_to_pdf(template_src, context_dict={}):
    template = get_template(template_src)
    html  = template.render(context_dict)
    # result = BytesIO()
    # pdf = pisa.pisaDocument(BytesIO(html.encode("ISO-8859-1")), result,)
    response = HttpResponse(content_type='application/pdf')
    response['Content-Disposition'] = 'attachment; filename="Data.pdf"'
    pdf = pisa.CreatePDF(html, dest=response, link_callback=link_callback)
    if not pdf.err:
        # return HttpResponse(result.getvalue(), content_type='application/pdf')
        return response
    return HttpResponse("Error Rendering PDF", status=400)

def ref_replace_regex(doc_obj, rep):
    start = time.time()
    for cell in doc_obj.tables[0].rows[26].cells:
        length = 15
        k = 0
        for i in range(len(rep), length):
            regex_i = re.compile(r'Referensi_{}_'.format(i+1))
            if len(cell.paragraphs) is length-k:
                if regex_i.search(cell.paragraphs[len(rep)].text):
                    docx_delete_p(cell.paragraphs[len(rep)])
                    k += 1   
                    if length is k:
                        return time.time() - start
        for i in range(0,len(rep)):
            regex_i = re.compile(r'Referensi_{}_'.format(i+1))
            for p in cell.paragraphs:        
                replaced = 0
                if regex_i.search(p.text):
                    inline = p.runs
                    text_concated = ""
                    for j in range(len(inline)):
                        text_concated += inline[j].text
                        if regex_i.pattern in text_concated and replaced is 0:
                            text = inline[j].text.replace(inline[j].text, rep[i]['normal1'])
                            inline[j].text = text
                            replaced = 1
                        elif rep[i]['em'] and "i" in inline[j].text and replaced is 1:
                            text = inline[j].text.replace(inline[j].text, " "+rep[i]['em'])
                            inline[j].text = text
                            replaced = 2
                        elif rep[i]['normal2'] and "n" in inline[j].text and replaced is 2:
                            text = inline[j].text.replace(inline[j].text, " "+rep[i]['normal2'])
                            inline[j].text = text
                            replaced = 3
                        else:
                            text = inline[j].text.replace(inline[j].text, "")
                            inline[j].text = text
            if replaced is not 0:
                return time.time() - start
    return time.time() - start


def docx_replace_regex(doc_obj, regex, replace):
    try:
        for p in doc_obj.paragraphs:        
            if regex.search(p.text):
                inline = p.runs
                text_concated = ""
                # Loop added to work with runs (strings with same style)
                for i in range(len(inline)):
                    text_concated += inline[i].text
                    # print("inline[%i].text"%i,inline[i].text)
                    if regex.pattern in text_concated:
                        text = inline[i].text.replace(inline[i].text, replace)
                        inline[i].text = text
                        raise StopIteration
                        # print("text",text)
                    else:
                        text = inline[i].text.replace(inline[i].text, "")
                        inline[i].text = text                    
        for table in doc_obj.tables:
            for row in table.rows:
                for cell in row.cells:
                    is_break = docx_replace_regex(cell, regex , replace)
                    if is_break:
                        return True
        return False
    except StopIteration :
        return True

def docx_replace_regex_list(doc_obj, regex, replace_list, length):
    for table in doc_obj.tables:
        for row in table.rows:
            for cell in row.cells:
                is_break = False
                k=0
                for i in range(len(replace_list), length):
                    regex_i = re.compile(r'{}_{}_'.format(regex.pattern,i+1))
                    if len(cell.paragraphs) is length-k:
                        if regex_i.search(cell.paragraphs[len(replace_list)].text):
                            docx_delete_p(cell.paragraphs[len(replace_list)])
                            k+=1   
                            if length is k:
                                return True
                for i in range(1,len(replace_list)+1):
                    regex_i = re.compile(r'{}_{}_'.format(regex.pattern,i))
                    if replace_list[i-1] in "_NOTHING_":
                        if len(cell.paragraphs) is length-k:
                            for p in cell.paragraphs:
                                if regex_i.search(p.text):
                                    docx_delete_p(p)
                                    k+=1  
                        continue
                    is_break = docx_replace_regex(cell, regex_i, replace_list[i-1],)  
                if is_break:
                    return True  
    return False            

def docx_delete_p(paragraph):
    p = paragraph._element
    p.getparent().remove(p)
    p._p = p._element = None

def regex(r):
    return re.compile(r"{}".format(r))

def doc_row(list_bahan,s):
    cut_row1 = 0
    cut_row2 = 0
    cut_row3 = 0
    cut_row4 = 0
    list_true = [bahan.is_uts for bahan in list_bahan]
    if True in list_true:
        cut_row1 = minggu_pra_bahan(list_bahan, "uts")
        cut_row2 = minggu_pra_bahan(list_bahan, "uas")
    else:
        cut_row3 = minggu_pra_bahan(list_bahan, "uas_only")
        cut_row4 = 3
    row_list = {
        'matkul': 2, 
        'dosen': 3, 
        'capaian': 4, 
        'deskripsi': 5, 
        'bahan1-1': 9, 
        'bahan1-2': 10, 
        'bahan1-3': 20-cut_row1-cut_row3+cut_row4, 
        'ujian1': 21-cut_row1-cut_row3+cut_row4, 
        'bahan2-1': 22-cut_row1-cut_row3+cut_row4,
        'bahan2-2': 23-cut_row1-cut_row3+cut_row4, 
        'bahan2-3': 33-cut_row1-cut_row2-cut_row3+cut_row4, 
        'ujian2': 34-cut_row1-cut_row2-cut_row3+cut_row4, 
        'jumlah': 35-cut_row1-cut_row2-cut_row3+cut_row4, 
        'referensi': 36-cut_row1-cut_row2-cut_row3+cut_row4,
    }
    return row_list[s]

def minggu_pra_bahan(list_bahan, uts_or_uas):
    uts,uas=0,0
    list_minggu = []
    for bahan in list_bahan:
        list_minggu.append(int(bahan.minggu_ke))
        if bahan.is_uts is True:
            uts = int(bahan.minggu_ke)
        if bahan.is_uas is True:
            uas = int(bahan.minggu_ke)
    list_minggu.sort()
    pra_uts = []
    pra_uas = []
    for mg in list_minggu:
        if mg < uts:
            pra_uts.append(mg)
        if mg > uts and mg < uas:
            pra_uas.append(mg)
    if uts_or_uas is "pra_uts":
        return pra_uts
    if uts_or_uas is "pra_uas":
        return pra_uas
    if uts_or_uas is "uts":
        hapus_pra_uts = 10 - (len(pra_uts)-2)
        return hapus_pra_uts
    if uts_or_uas is "uas":
        hapus_pra_uas = 10 - (len(pra_uas)-2)
        return hapus_pra_uas
    if uts_or_uas is "uas_only":
        hapus_pra_ujian = (10 * 2) - (len(pra_uas)-2)
        return hapus_pra_ujian

def loadDocx(list_bahan):
    BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    document = Document(os.path.join(BASE_DIR, 'doc/tem/template-RPS.docx'))
    hapus_uts = True
    for bahan in list_bahan:
        if bahan.is_uts is True:
            hapus_uts = False
    tbl = document.tables[0]
    rows = tbl.rows
    if hapus_uts is False:
        for i in range(doc_row(list_bahan,'bahan1-2'), doc_row(list_bahan,'bahan1-2') + minggu_pra_bahan(list_bahan, "uts")):
            tbl._tbl.remove(rows[doc_row(list_bahan,'bahan1-2')]._tr)
        n = 0
        if minggu_pra_bahan(list_bahan, "uas") > 8:
            n = 1
        for i in range(doc_row(list_bahan,'bahan2-2')-n, doc_row(list_bahan,'bahan2-2') + minggu_pra_bahan(list_bahan, "uas")-n):
            tbl._tbl.remove(rows[doc_row(list_bahan,'bahan2-2')-n]._tr)
        
        ok = docx_replace_regex(document, regex('Ujian1_'), "Ujian Tengah Semester")
        ok = docx_replace_regex(document, regex('Ujian2_'), "Ujian Akhir Semester")

    else:
        # hapus row 1-3, uts, 2-1
        tbl._tbl.remove(rows[doc_row(list_bahan,'bahan1-3')]._tr)
        tbl._tbl.remove(rows[doc_row(list_bahan,'ujian1')]._tr)
        tbl._tbl.remove(rows[doc_row(list_bahan,'bahan2-1')]._tr)

        for i in range(doc_row(list_bahan,'bahan1-2'), doc_row(list_bahan,'bahan1-2') + minggu_pra_bahan(list_bahan, "uas_only")):
            tbl._tbl.remove(rows[doc_row(list_bahan,'bahan1-2')]._tr)
        
        ok = docx_replace_regex(document, regex('Ujian2_'), "Ujian Akhir Semester")

    return document

def downloadDocx(tahun, file_name):
    print(file_name)
    file_name = file_name.replace(" ", "_")
    document = Document('{}/media/arsip/{}/{}.docx'.format(str(BASE_DIR), str(tahun), file_name))
    doc_name = 'RPS - {} {}.doc'.format(file_name, str(tahun))
    f = BytesIO()
    document.save(f)
    length = f.tell()
    f.seek(0)
    response = HttpResponse(
        f.getvalue(),
        content_type='application/vnd.openxmlformats-officedocument.wordprocessingml.document'
    )
    response['Content-Disposition'] = 'attachment; filename=' + doc_name
    response['Content-Length'] = length
    return response

def createDocx(document, tahun, rps):
    docx_name = rps.matkul.nama_matkul.replace(" ", "_")
    # Prepare document for arsipping        
    # -----------------------------
    # f = BytesIO()
    # filepath = settings.MEDIA_ROOT + '/arsip/{}/{}.doc'.format(str(tahun), docx_name)
    filepath_docx = settings.MEDIA_ROOT + '/arsip/{}/{}.docx'.format(str(tahun), docx_name)
    document.save(filepath_docx)
    # document.save(f)
    # length = f.tell()
    # f.seek(0)
    # response = HttpResponse(
    #     f.getvalue(),
    #     content_type='application/vnd.openxmlformats-officedocument.wordprocessingml.document'
    # )
    # response['Content-Disposition'] = 'attachment; filename=' + docx_name
    # response['Content-Length'] = length
    # return response

# @deconstructible
# def rename_and_path(path,jenis):
#     def wrapper(instance,filename):
#         ext = filename.split('.')[-1]
#         nama = '{}/{}'.format(jenis,instance.pk)
#         filename = '{}.{}'.format(nama,ext)
#         return os.path.join(path,filename)

#     return wrapper
@deconstructible
class PathAndRename(object):

    def __init__(self, sub_path):
        self.path = sub_path

    def __call__(self, instance, filename):
        ext = filename.split('.')[-1]
        # nama = '{}/{}'.format(jenis,instance.pk)
        name = '{}.{}'.format(instance.pk,ext)
        return os.path.join(self.path, name)

def rename_and_path(jenis):
    return PathAndRename(jenis)


def xstr(s):
    if s is 0:
        return str(s)
    if s:
        return str(s)
    return ""

def xint(n):
    if n:
        return int(n)
    return 0



###### NEW RPS TO DOC
def loadDocx16():
    BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    document = Document(os.path.join(BASE_DIR, 'doc/tem/template-RPS-16fix.docx'))
    return document

def printall(doc_obj):
    dict_list = {'kd':[],'d':[]}
    for row in range(len(doc_obj.tables[0].rows)):
        for cell in range(len(doc_obj.tables[0].rows[row].cells)):
            for p in range(len(doc_obj.tables[0].rows[row].cells[cell].paragraphs)):
                # print("R{}-C{}-P{}".format(row,cell,p),":",doc_obj.tables[0].rows[row].cells[cell].paragraphs[p].text)
                dict_list['kd'].append("R{}-C{}-P{}".format(row,cell,p))
                dict_list['d'].append(doc_obj.tables[0].rows[row].cells[cell].paragraphs[p].text)
    return dict_list

def docx_replace_16_list(i, doc_obj, regex, replace_list, length):
    start = time.time()
    for cell in doc_obj.tables[0].rows[i].cells:
        is_break = False
        k=0
        for i in range(len(replace_list), length):
            regex_i = re.compile(r'{}_{}_'.format(regex.pattern,i+1))
            if len(cell.paragraphs) is length-k:
                if regex_i.search(cell.paragraphs[len(replace_list)].text):
                    docx_delete_p(cell.paragraphs[len(replace_list)])
                    k+=1   
                    if length is k:
                        return time.time() - start
        for i in range(1,len(replace_list)+1):
            regex_i = re.compile(r'{}_{}_'.format(regex.pattern,i))
            if replace_list[i-1] in "_NOTHING_":
                if len(cell.paragraphs) is length-k:
                    for p in cell.paragraphs:
                        if regex_i.search(p.text):
                            docx_delete_p(p)
                            k+=1  
                continue
            is_break = docx_replace_regex(cell, regex_i, replace_list[i-1],)  
        if is_break:
            return time.time() - start

def docx_replace_16(doc_obj, regex, replace):
    start = time.time()
    try:
        if type(doc_obj) is type(loadDocx16().tables[0].rows[0].cells[0]):
            for p in doc_obj.paragraphs:        
                if regex.search(p.text):
                    inline = p.runs
                    text_concated = ""
                    # Loop added to work with runs (strings with same style)
                    for i in range(len(inline)):
                        text_concated += inline[i].text
                        # print("inline[%i].text"%i,inline[i].text)
                        if regex.pattern in text_concated:
                            text = inline[i].text.replace(inline[i].text, replace)
                            inline[i].text = text
                            raise StopIteration
                            # print("text",text)
                        else:
                            text = inline[i].text.replace(inline[i].text, "")
                            inline[i].text = text 
        else :
            for cell in doc_obj.cells:
                ok = docx_replace_16(cell, regex, replace)
    except StopIteration :
        return time.time() - start
    return time.time() - start