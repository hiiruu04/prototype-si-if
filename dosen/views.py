from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import Group
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, redirect, get_object_or_404
from django.views.generic import View, TemplateView, DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic.list import ListView
from django.urls import reverse, reverse_lazy
from django.conf import settings
from users.models import User
from matkul.models import Kurikulum
from .models import (Dosen, 
                    Operator, 
                    SKJabfung, 
                    Jabfung, 
                    LabKBK, 
                    Ijazah,
                    Golongan, 
                    Status,
                    Strata)
from .forms import (CreateDosenForm, 
                    UpdateDosenForm, 
                    CreateOperatorForm, 
                    CreateIjazahForm, 
                    CreateSKJabatanForm,
                    UpdateIjazahForm,
                    UpdateSKJabatanForm,
                    CreateJabatanForm,
                    UpdateJabatanForm,
                    CreateGolonganForm,
                    UpdateGolonganForm,
                    CreateStatusForm,
                    UpdateStatusForm,
                    CreateLabKBKForm,
                    UpdateLabKBKForm,
                    CreateStrataForm,
                    UpdateStrataForm)
from django.views.generic.list import ListView
from django.template.loader import get_template
from .utils import render_to_pdf, current_year
import csv
from getter.get import get_data_dosen, get_data_golongan, get_data_ijazah, get_data_jabatan, get_data_lab, get_data_operator, get_data_sk, get_data_status, get_data_strata, get_dosen, get_golongan, get_jabatan, get_lab, get_operator, get_status, get_strata, get_user_dosen


def homeView(request):
    """
        Fungsi untuk merender halaman awal
    """
    return render(request, 'home.html')

def authenticateUser(request):
    """
        Fungsi yang digunakan untuk mengarahkan user ke dashboard masing-masing
        Digunakan sebagai port redirect utama
    """
    try:
        user = request.user
        if user.groups.filter(name='Dosen').exists() :
            return redirect('dashboard')
        elif user.groups.filter(name='Operator').exists() :
            return redirect('dashboard')
        elif user.groups.filter(name='Pengurus').exists():
            return redirect('dashboard_admin')
        else:
            return HttpResponse('su')
    except:
        return HttpResponse('error')

def curr_curriculum():
    """
        Fungsi yang digunakan untuk mendapatkan kurikulum terbaru (dengan nilai tahun terbesar)
    """
    kurikulum = Kurikulum.objects.all().order_by("-tahun")
    curr = kurikulum[0]
    return curr

@login_required
def dashboardStaff(request):
    """
        Fungsi yang digunakan untuk menyaring user yang masuk
        User yang diperbolehkan hanya Dosen dan Operator
        di redirect ke dashboard.html
    """
    user = request.user
    kur = curr_curriculum().tahun
    group = {g.name for g in request.user.groups.all()}
    dosen_id = get_user_dosen(user)
    if 'Dosen' in group or 'Operator' in group :
        return render(request, 'dashboard.html', {'user':request.user,'group':group, 'dosen_id':dosen_id,'kur':kur})
    else:
        return redirect('authenticate')
    

@login_required
def dashboardAdmin(request):
    """
        Fungsi yang digunakan untuk menyaring user yang masuk
        User yang diperbolehkan hanya Pengurus
        di redirect ke dashboard-admin.html
    """
    user = request.user
    group = {g.name for g in request.user.groups.filter(name="Pengurus")}
    if 'Pengurus' in group :
        return render(request, 'dashboard-admin.html', {'user':request.user})
    else:
        return redirect('authenticate')

class DashboardStaff(TemplateView):
    """
        Class Based View yang digunakan untuk merender dashboard staff
    """
    model = User
    template_name = 'dashboard.html'


class PerfGroup(LoginRequiredMixin, PermissionRequiredMixin):
    login_url = "login"
    
### STAFF ###
class OwnerMixin(object):
    """
        Class untuk menggabungkan loginrequired dan permission yang dibutuhkan untuk mengakses suatu halaman
        jika gagal melewati kedua parameter tersebut akan dikembalikan ke login page
    """
    login_url = "login" 

### STAFF ###
class OwnerMixin(object):
    """
        Class yang digunakan untuk mendapatkan data pemilik dan user yang login sekarang
    """
    def get_queryset(self):
        qs = super(OwnerMixin, self).get_queryset()
        return qs.filter(user=self.request.user)

class OwnerEditMixin(object):
    """
        Class yang digunakan untuk membatasi siapa yang dapat mengedit suatu data
        Cara kerja dengan mencocokkan data pemiliki dan data user
        Bertujuan agar hanya pemiliki data yang dapat mengedit datanya sendiri (Dengan level yang sama)
    """
    def form_valid(self, form):
        form.instance.user=self.request.user
        return super(OwnerEditMixin, self).form_valid(form)

class ManageDosenListView(ListView):
    """
        Class yang akan digunakan untuk menampilkan list dosen
    """
    model = Dosen
    template_name = "manage/list-dosen.html"
    # queryset = Dosen.objects.filter(lab_kbk=8)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        data = get_dosen()
        lab_kbk = get_lab()
        if 'lab' in self.kwargs:
            lab = self.kwargs['lab']
            context['curr_lab'] = "Lab " + lab
            data = get_dosen(lab)
        context['dsn'] = data
        context['lab'] = lab_kbk
        return context
        
 
class OperatorCreateDosenView(PerfGroup, CreateView):
    """
        Class based view yang bertujuan untuk membuat dosen
    """
    form_class = CreateDosenForm
    template_name = 'manage/operator/form-create.html'
    success_url = reverse_lazy('dosen_list')
    permission_required = "dosen.add_dosen"

    def form_valid(self, form):
        c = {'form': form, }
        user = form.save()
        group = Group.objects.get(name='Dosen')
        user.groups.add(group)
        user.save()
        # user = form.save(commit=False)
        firstname = form.cleaned_data['Nama_Depan']
        midname = form.cleaned_data['Nama_Tengah']
        lastname = form.cleaned_data['Nama_Belakang']
        email = form.cleaned_data['email']
        # Create UserProfile model
        Dosen.objects.create(
            user=user, 
            namaDepan=firstname, 
            namaTengah=midname, 
            namaBelakang=lastname,
            email=email
        )
        return super(OperatorCreateDosenView, self).form_valid(form)

class OperatorDeleteDosenView(PerfGroup, DeleteView):
    """
        Class based view yang digunakan untuk menghapus dosen
    """
    model = User
    template_name = 'manage/operator/delete-dosen.html'
    success_url = reverse_lazy('dosen_list')
    permission_required = 'dosen.delete_dosen'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        id_user = self.kwargs['pk']
        dosen = get_object_or_404(Dosen, user_id=id_user)
        context["id"] = dosen.id
        context["dosen"] = get_data_dosen(dosen)
        return context
    

class OperatorUpdateDosenView(PerfGroup, UpdateView):
    """
        Class based view yang digunakan untuk mengedit dosen
        OPERATOR
    """
    model = Dosen
    form_class = UpdateDosenForm
    template_name = 'manage/operator/form-edit.html'
    success_url = reverse_lazy('dosen_list')
    permission_required = 'dosen.change_dosen'

    def dispatch(self, request, *args, **kwargs):
        id_dosen = self.kwargs['pk']
        dosen = Dosen.objects.get(id=id_dosen)
        self.user_id = dosen.user_id
        user = request.user
        group = user.groups.filter(name='Dosen')
        if len(group) > 0 :
            return redirect('authenticate')
        return super().dispatch(request, *args, **kwargs)
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        dosen = self.get_object()
        dsn = get_data_dosen(dosen)
        context['id'] = dsn['id']
        context['jabatan'] = dsn['jabatan']
        return context

    def form_valid(self, form):
        c = {'form': form, }
        id_user = self.user_id
        user = User.objects.get(id=id_user)
        email = form.cleaned_data['email']
        user.email = email
        user.save()
        formDosen = form.save()
        formDosen.save()
        return super(OperatorUpdateDosenView, self).form_valid(form)

    def get_success_url(self, **kwargs):
        if 'pk' in self.kwargs:
            id_dosen = self.kwargs['pk']
            return reverse_lazy('dosen_detail',kwargs={'pk':id_dosen})
        else:
            return HttpResponse('GAGAL')

class DosenUpdateDataView(PerfGroup, OwnerMixin, OwnerEditMixin, UpdateView):
    """
        Class based view yang digunakan untuk mengedit dosen
        DOSEN
    """
    model = Dosen
    form_class = UpdateDosenForm
    template_name = 'manage/dosen/form-dosen.html'
    success_url = reverse_lazy('dosen_list')
    permission_required = 'dosen.change_dosen'

    def dispatch(self, request, *args, **kwargs):
        id_user = request.user.id
        user = User.objects.get(id=id_user)
        self.user= user
        group = user.groups.filter(name='Operator')
        if len(group) > 0 :
            return redirect('authenticate')
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        dosen = self.get_object()
        dsn = get_data_dosen(dosen)
        context['id'] = dsn['id']
        context['jabatan'] = dsn['jabatan']
        return context
    
    def form_valid(self, form):
        c = {'form': form, }
        email = form.cleaned_data['email']
        formDosen = form.save()
        formDosen.save()
        self.user.email = email
        self.user.save()
        return super(DosenUpdateDataView, self).form_valid(form)

class DosenDetailView(DetailView):
    """
        Class based view yang digunakan untuk menampilkan detail dosen
    """
    model = Dosen
    template_name = 'manage/detail-dosen.html'
    context_object_name = 'dosen'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        dosen = self.get_object()
        # FULLNAME CONSTRUCT
        dsn = get_data_dosen(dosen)
        context['id'] = dsn['id']
        context['fullname'] = dsn['name']
        context['jenkel'] = dsn['jenkel']
        context['nip'] = dsn['nip']
        context['nidn'] = dsn['nidn']
        context['prodi'] = dsn['prodi']
        context['golongan']=dsn['golongan']
        context['status']=dsn['status']
        context['alamat_kantor']=dsn['alamat_kantor']
        context['keahlian']=dsn['keahlian']
        context['lab'] = dsn['lab']
        context['email']=dsn['email']
        context['website']=dsn['website']
        context['ttl'] = dsn['ttl']
        context['jabatan'] = dsn['jabatan']
        context['foto'] = dsn['foto']
        context['no_telp'] = dsn['no_telp']
        return context

def OperatorDataDosen(request):
    """
        Digunakan untuk mendapatkan dan menampilkan semua data dosen yang ada
        kedalam bentuk tabel
    """
    user = request.user
    group = user.groups.all()[0].name
    if 'Operator' in group :
        data_dosen = Dosen.objects.all()
        dosens = []
        for dosen in data_dosen:
            dsn = get_data_dosen(dosen)
            dosens.append(dsn)
        return render(request, 'manage/operator/view-data-dosen.html', {'dosens':dosens})
    else :
        return redirect('authenticate')

@login_required
def printDataDosenCSV(request):
    """
        Fungsi yang akan digunakan untuk menyimpan semua data dosen yang ada pada database
        ke dalam format csv
    """
    user = request.user
    group = user.groups.all()[0].name
    if 'Operator' in group :
        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename="data_dosen_if.csv"'

        data_dosen = Dosen.objects.all()
        dosens = []
        writer = csv.writer(response, delimiter=';')
        writer.writerow(['nama','nip','nidn','jk','prodi','jabatan','golongan','ttl','notelp','kd_doswal','alamat_kantor','keahlian','lab','email','web','status'])

        for dosen in data_dosen:
            
            dsn = get_data_dosen(dosen)
            fullname = dsn['name']
            nip = dsn['nip']
            jafung = dsn['jabatan']
            golongan = dsn['golongan']
            status = dsn['status']
            jenkel= dsn['jenkel']
            nidn = dsn['nidn']
            prodi = dsn['prodi']
            alamat_kantor = dsn['alamat_kantor']
            keahlian = dsn['keahlian']
            lab = dsn['lab']
            email = dsn['email']
            website = dsn['website']
            ttl = dsn['ttl']
            kode = dsn['kd_doswal']
            lab = dsn['lab']
            telp = dsn['no_telp']

            writer.writerow([
                fullname,
                nip,
                nidn,
                jenkel,
                prodi,
                jafung,
                golongan,
                ttl,
                telp,
                kode,
                alamat_kantor,
                keahlian,
                lab,
                email,
                website,
                status,
                ])
        
        return response

    else :
        return redirect('authenticate')

@login_required
def printDosenCSV(request, dosen_id):
    """
        Fungsi yang akan digunakan untuk menyimpan suatu data dosen yang ada pada database
        ke dalam format csv
    """
    dosen = Dosen.objects.get(id=dosen_id)

    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="data_dosen_if_{}.csv"'.format(dosen.namaDepan)

    writer = csv.writer(response, delimiter=';')
    writer.writerow(['nama','nip','nidn','jk','prodi','jabatan','golongan','ttl','notelp','kd_doswal','alamat_kantor','keahlian','lab','email','web','status'])

    dsn = get_data_dosen(dosen)
    fullname = dsn['name']
    nip = dsn['nip']
    jafung = dsn['jabatan']
    golongan = dsn['golongan']
    status = dsn['status']
    jenkel= dsn['jenkel']
    nidn = dsn['nidn']
    prodi = dsn['prodi']
    alamat_kantor = dsn['alamat_kantor']
    keahlian = dsn['keahlian']
    lab = dsn['lab']
    email = dsn['email']
    website = dsn['website']
    ttl = dsn['ttl']
    kode = dsn['kd_doswal']
    lab = dsn['lab']
    telp = dsn['no_telp']

    writer.writerow([
        fullname,
        nip,
        nidn,
        jenkel,
        prodi,
        jafung,
        golongan,
        ttl,
        telp,
        kode,
        alamat_kantor,
        keahlian,
        lab,
        email,
        website,
        status,
        ])    
    return response
    

#### ADMIN ####
class CreateOperator(PerfGroup, CreateView):
    """
        Class based view yang digunakan untuk membuat operator
    """
    form_class = CreateOperatorForm
    success_url = reverse_lazy('authenticate')
    template_name = 'manage/admin/create_operator.html'
    permission_required = "dosen.add_operator"

    def form_valid(self, form):
        c = {'form': form, }
        user = form.save()
        group = Group.objects.get(name='Operator')
        group.user_set.add(user)
        # user = form.save(commit=False)
        namaOp = form.cleaned_data['namaOp']
        # Create UserProfile model
        Operator.objects.create(user=user, nama=namaOp)
 
        return super(CreateOperator, self).form_valid(form)

class ManageOperatorListView(PerfGroup, ListView):
    """
        Class based view yang digunakan untuk menampilkan daftar operator
    """
    model = Operator
    template_name = "manage/admin/list-operator.html"
    permission_required = "dosen.view_operator"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["operator"] = get_operator()
        return context

class DeleteOperator(PerfGroup, DeleteView):
    """
        Class based view yang digunakan untuk menghapus operator
    """
    model = User
    template_name = 'manage/admin/delete-operator.html'
    success_url = reverse_lazy('authenticate')
    permission_required = "dosen.delete_operator"

class DosenToPDF(DetailView):
    """
        Class yang akan digunakan untuk menyimpan data dosen yang ada pada database
        ke dalam format pdf
    """
    model = Dosen
    context_object_name = 'dosen'

    def get(self, request, *args, **kwargs):
        template = get_template('files/to_pdf.html')
        dosen = self.get_object()
        dsn = get_data_dosen(dosen)
        # jabatan = SKJabfung.objects.filter(pemilik_id=dosen.id).order_by('-tanggalSK')[0].jabatan
        # list_ijazah = Ijazah.objects.filter(dosen=dosen)
        tahun_ini = current_year()
        context = {}
        context['fullname'] = dsn['name']
        context['nip'] = dsn['nip']
        context['nidn'] = dsn['nidn']
        context['prodi'] = dsn['prodi']
        context['golongan']=dsn['golongan']
        context['status']=dsn['status']
        context['alamat_kantor']=dsn['alamat_kantor']
        context['keahlian']=dsn['keahlian']
        context['lab'] = dsn['lab']
        context['email']=dsn['email']
        context['website']=dsn['website']
        context['foto'] = dsn['foto']
        # context['jabatan'] = jabatan
        context['jabatan'] = dsn['jabatan']
        # context['list_ijazah'] = list_ijazah
        context['list_ijazah'] = dsn['ijazah']
        context['tahun_ini'] = tahun_ini
        html = template.render(context)
        pdf = render_to_pdf('files/to_pdf.html', context)
        if pdf:
            # response = HttpResponse(pdf, content_type='application/pdf')
            filename = 'Profil '+dosen.namaDepan
            content = 'attachment; filename=%s.pdf' %(filename)
            # download = request.GET.get("download")
            pdf['Content-Disposition'] = content
            return pdf
        return HttpResponse("Not found")

class CreateIjazahView(CreateView):
    """
        Class based view yang digunakan untuk menambahkan ijazah baru pada suatu dosen
    """
    model = Ijazah
    form_class = CreateIjazahForm
    template_name = 'manage/dosen/create-ijazah.html'
    success_url = reverse_lazy('dosen_list')

    def dispatch(self,request, *args, **kwargs):
        self.dosen = get_object_or_404(Dosen, id=kwargs['pk'])
        self.user = request.user
        return super().dispatch(request,*args,**kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        curr_user = self.user
        groups = curr_user.groups.all()
        if len(groups) > 0:
            group=groups[0].name
            if 'Dosen' in group :
                context['red'] = 'dosen_update_dosen'
            elif 'Operator' in group :
                context['red'] = 'dosen_update'

        if 'pk' in self.kwargs:
            id_dosen = self.kwargs['pk']
            dosen = Dosen.objects.get(id=id_dosen)
            dsn = get_data_dosen(dosen)
            context['id'] = dsn['id']
            context['jabatan'] = dsn['jabatan']
        return context

    def form_valid(self, form):
        form.instance.dosen = self.dosen
        return super().form_valid(form)

    def get_success_url(self, **kwargs):         
        if 'pk' in self.kwargs:
            pk = self.kwargs['pk']
            id_ = self.request.user.id
            user_ = User.objects.get(id=id_)
            groups = user_.groups.all()
            if len(groups) > 0:
                if 'Dosen' in groups:
                    return reverse_lazy('dosen_ijazah', kwargs = {'pk': pk})
                else:
                    return reverse_lazy('dosen_update', kwargs = {'pk': pk})
        else :
            return HttpResponse('GAGAL')

class UpdateIjazahView(UpdateView):
    model = Ijazah
    form_class = UpdateIjazahForm
    template_name = 'manage/dosen/update-ijazah.html'
    success_url = reverse_lazy('dosen_list')
    # permission_required = 'dosen.change_dosen'
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if 'dosen' in self.kwargs:
            id_dosen = self.kwargs['dosen']
            context['dosen'] = id_dosen
        return context

    def get_success_url(self, **kwargs):
        if 'dosen' in self.kwargs:
            id_dosen = self.kwargs['dosen']
            return reverse_lazy('dosen_ijazah',kwargs={'pk':id_dosen})
        else:
            return HttpResponse('GAGAL')

class DeleteIjazahView(DeleteView):
    model = Ijazah
    template_name = 'manage/dosen/delete-ijazah.html'
    success_url = reverse_lazy('dosen_list')
    # permission_required = 'dosen.delete_dosen'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if 'pk' in self.kwargs:
            id_ijazah = self.kwargs['pk']
            ijazah = Ijazah.objects.get(id=id_ijazah)
            ijz = get_data_ijazah(ijazah)
            context['ijazah']=ijz
        return context

    def get_success_url(self, **kwargs):
        if 'dosen' in self.kwargs:
            id_dosen = self.kwargs['dosen']
            return reverse_lazy('dosen_ijazah',kwargs={'pk':id_dosen})
        else:
            return HttpResponse('GAGAL')

class CreateSKJabatanView(CreateView):
    """
        Class based view yang digunakan untuk menambahkan sk jabatan baru pada suatu dosen
    """
    model = SKJabfung
    form_class = CreateSKJabatanForm
    template_name = 'manage/dosen/create-sk.html'
    success_url = reverse_lazy('dosen_list')
    dosen_id = ""

    def get_form_kwargs(self):
        kwargs = super(CreateSKJabatanView, self).get_form_kwargs()
        kwargs['id_dosen'] = self.kwargs['pk'] # memasukkan id dosen ke kwargs
        return kwargs

    def dispatch(self,request, *args, **kwargs):
        self.dosen = get_object_or_404(Dosen, id=kwargs['pk'])
        self.user = request.user
        return super().dispatch(request,*args,**kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        curr_user = self.user
        groups = curr_user.groups.all()
        if len(groups) > 0:
            group=groups[0].name
            if 'Dosen' in group :
                context['dir'] = 'dosen_update_dosen'
            elif 'Operator' in group :
                context['dir'] = 'dosen_update'
            else:
                context['dir'] = 'home'

        if 'pk' in self.kwargs:
            id_dosen = self.kwargs['pk']
            dosen = Dosen.objects.get(id=id_dosen)
            dsn = get_data_dosen(dosen)
            context['id'] = dsn['id']
        return context

    def form_valid(self, form):
        form.instance.pemilik = self.dosen
        return super().form_valid(form)

    def get_success_url(self, **kwargs):         
        if 'pk' in self.kwargs:
            pk = self.kwargs['pk']
            id_ = self.request.user.id
            user_ = User.objects.get(id=id_)
            groups = user_.groups.all()
            group = Group.objects.get(name='Dosen')
            if len(groups) > 0:
                if group in groups:
                    return reverse_lazy('dosen_update_dosen', kwargs = {'pk': pk})
                else:
                    return reverse_lazy('dosen_update', kwargs = {'pk': pk})
        else :
            return HttpResponse('GAGAL')

class UpdateSKJabatanView(UpdateView):
    model = SKJabfung
    form_class = UpdateSKJabatanForm
    template_name = 'manage/dosen/update-sk.html'
    success_url = reverse_lazy('dosen_list')
    # permission_required = 'dosen.change_dosen'
    
    def get_form_kwargs(self):
        kwargs = super(UpdateSKJabatanView, self).get_form_kwargs()
        kwargs['id_dosen'] = self.kwargs['dosen'] # memasukkan id dosen ke kwargs
        return kwargs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if 'dosen' in self.kwargs:
            id_dosen = self.kwargs['dosen']
            context['dosen'] = id_dosen
        return context

    def get_success_url(self, **kwargs):
        if 'dosen' in self.kwargs:
            id_dosen = self.kwargs['dosen']
            return reverse_lazy('dosen_sk',kwargs={'pk':id_dosen})
        else:
            return HttpResponse('GAGAL')

class DeleteSKJabatanView(DeleteView):
    model = SKJabfung
    template_name = 'manage/dosen/delete-sk.html'
    success_url = reverse_lazy('dosen_list')
    # permission_required = 'dosen.delete_dosen'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if 'pk' in self.kwargs:
            id_sk = self.kwargs['pk']
            sk = SKJabfung.objects.get(id=id_sk)
            sks = get_data_sk(sk)
            context['sk']=sks
        return context

    def get_success_url(self, **kwargs):
        if 'dosen' in self.kwargs:
            id_dosen = self.kwargs['dosen']
            return reverse_lazy('dosen_sk',kwargs={'pk':id_dosen})
        else:
            return HttpResponse('GAGAL')

class ListJabatanView(ListView):
    model = Jabfung
    template_name = "manage/list-jabatan.html"

    def dispatch(self, request, *args, **kwargs):
        id_user = request.user.id
        user = User.objects.get(id=id_user)
        group = user.groups.filter(name='Operator')
        if len(group) == 0 :
            return redirect('authenticate')
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["jabatan"] = get_jabatan()
        return context
    
class CreateJabatanView(CreateView):
    model = Jabfung
    form_class = CreateJabatanForm
    template_name = 'manage/jabatan/create-jabatan.html'
    success_url = reverse_lazy('jabatan_list')

    def dispatch(self, request, *args, **kwargs):
        id_user = request.user.id
        user = User.objects.get(id=id_user)
        group = user.groups.filter(name='Operator')
        if len(group) == 0 :
            return redirect('authenticate')
        return super().dispatch(request, *args, **kwargs)

class UpdateJabatanView(UpdateView):
    model = Jabfung
    form_class = UpdateJabatanForm
    template_name = 'manage/jabatan/update-jabatan.html'
    success_url = reverse_lazy('jabatan_list')

    def dispatch(self, request, *args, **kwargs):
        id_user = request.user.id
        user = User.objects.get(id=id_user)
        group = user.groups.filter(name='Operator')
        if len(group) == 0 :
            return redirect('authenticate')
        return super().dispatch(request, *args, **kwargs)

class DeleteJabatanView(DeleteView):
    model = Jabfung
    template_name = 'manage/jabatan/delete-jabatan.html'
    success_url = reverse_lazy('jabatan_list')
    # permission_required = 'dosen.delete_dosen'

    def dispatch(self, request, *args, **kwargs):
        id_user = request.user.id
        user = User.objects.get(id=id_user)
        group = user.groups.filter(name='Operator')
        if len(group) == 0 :
            return redirect('authenticate')
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if 'pk' in self.kwargs:
            id_jab = self.kwargs['pk']
            jabatan = Jabfung.objects.get(id=id_jab)
            jabs = get_data_jabatan(jabatan)
            context['jabatan']= jabs
            context['jabatan_obj'] = jabatan
        return context

class ListGolonganView(ListView):
    model = Golongan
    template_name = "manage/list-golongan.html"

    def dispatch(self, request, *args, **kwargs):
        id_user = request.user.id
        user = User.objects.get(id=id_user)
        group = user.groups.filter(name='Operator')
        if len(group) == 0 :
            return redirect('authenticate')
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["golongan"] = get_golongan()
        return context
    
class CreateGolonganView(CreateView):
    model = Golongan
    form_class = CreateGolonganForm
    template_name = 'manage/golongan/create-golongan.html'
    success_url = reverse_lazy('golongan_list')

    def dispatch(self, request, *args, **kwargs):
        id_user = request.user.id
        user = User.objects.get(id=id_user)
        group = user.groups.filter(name='Operator')
        if len(group) == 0 :
            return redirect('authenticate')
        return super().dispatch(request, *args, **kwargs)

class UpdateGolonganView(UpdateView):
    model = Golongan
    form_class = UpdateGolonganForm
    template_name = 'manage/golongan/update-golongan.html'
    success_url = reverse_lazy('golongan_list')

    def dispatch(self, request, *args, **kwargs):
        id_user = request.user.id
        user = User.objects.get(id=id_user)
        group = user.groups.filter(name='Operator')
        if len(group) == 0 :
            return redirect('authenticate')
        return super().dispatch(request, *args, **kwargs)

class DeleteGolonganView(DeleteView):
    model = Golongan
    template_name = 'manage/golongan/delete-golongan.html'
    success_url = reverse_lazy('golongan_list')
    # permission_required = 'dosen.delete_dosen'

    def dispatch(self, request, *args, **kwargs):
        id_user = request.user.id
        user = User.objects.get(id=id_user)
        group = user.groups.filter(name='Operator')
        if len(group) == 0 :
            return redirect('authenticate')
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if 'pk' in self.kwargs:
            id_gol = self.kwargs['pk']
            golongan = Golongan.objects.get(id=id_gol)
            gol = get_data_golongan(golongan)
            context['golongan']= gol
        return context

class ListStatusView(ListView):
    model = Status
    template_name = "manage/list-status.html"

    def dispatch(self, request, *args, **kwargs):
        id_user = request.user.id
        user = User.objects.get(id=id_user)
        group = user.groups.filter(name='Operator')
        if len(group) == 0 :
            return redirect('authenticate')
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["status"] = get_status()
        return context
    
class CreateStatusView(CreateView):
    model = Status
    form_class = CreateStatusForm
    template_name = 'manage/status/create-status.html'
    success_url = reverse_lazy('status_list')

    def dispatch(self, request, *args, **kwargs):
        id_user = request.user.id
        user = User.objects.get(id=id_user)
        group = user.groups.filter(name='Operator')
        if len(group) == 0 :
            return redirect('authenticate')
        return super().dispatch(request, *args, **kwargs)

class UpdateStatusView(UpdateView):
    model = Status
    form_class = UpdateStatusForm
    template_name = 'manage/status/update-status.html'
    success_url = reverse_lazy('status_list')

    def dispatch(self, request, *args, **kwargs):
        id_user = request.user.id
        user = User.objects.get(id=id_user)
        group = user.groups.filter(name='Operator')
        if len(group) == 0 :
            return redirect('authenticate')
        return super().dispatch(request, *args, **kwargs)

class DeleteStatusView(DeleteView):
    model = Status
    template_name = 'manage/status/delete-status.html'
    success_url = reverse_lazy('status_list')
    # permission_required = 'dosen.delete_dosen'

    def dispatch(self, request, *args, **kwargs):
        id_user = request.user.id
        user = User.objects.get(id=id_user)
        group = user.groups.filter(name='Operator')
        if len(group) == 0 :
            return redirect('authenticate')
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if 'pk' in self.kwargs:
            id_stat = self.kwargs['pk']
            status = Status.objects.get(id=id_stat)
            stat = get_data_status(status)
            context['status']= stat
        return context

class ListLabView(ListView):
    model = LabKBK
    template_name = "manage/list-lab.html"

    def dispatch(self, request, *args, **kwargs):
        id_user = request.user.id
        user = User.objects.get(id=id_user)
        self.user = user
        # group = user.groups.filter(name='Operator')
        # if len(group) == 0 :
        #     return redirect('authenticate')
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["labs"] = get_lab()
        if self.user.groups.filter(name="Operator").exists():
            context["group_op"] = self.user.groups.filter(name="Operator")
        return context

class LabDetailView(DetailView):
    model = LabKBK
    template_name = "manage/lab/detail-lab.html"

    def dispatch(self, request, *args, **kwargs):
        self.user = None
        if request.user.is_authenticated:
            id_user = request.user.id
            user = User.objects.get(id=id_user)
            self.user = user
        # group = user.groups.filter(name='Operator')
        # if len(group) == 0 :
        #     return redirect('authenticate')
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        labs = LabKBK.objects.all()
        lab = self.get_object()
        data = get_data_lab(lab)
        if self.user:
            if self.user.groups.filter(name="Operator").exists():
                context["group_op"] = self.user.groups.filter(name="Operator")
        context["lab"] = data
        context['labs'] = labs
        return context

class CreateLabView(CreateView):
    model = LabKBK
    form_class = CreateLabKBKForm
    template_name = 'manage/lab/create-lab.html'
    success_url = reverse_lazy('lab_list')

    def dispatch(self, request, *args, **kwargs):
        id_user = request.user.id
        user = User.objects.get(id=id_user)
        group = user.groups.filter(name='Operator')
        if len(group) == 0 :
            return redirect('authenticate')
        return super().dispatch(request, *args, **kwargs)

class UpdateLabView(UpdateView):
    model = LabKBK
    form_class = UpdateLabKBKForm
    template_name = 'manage/lab/update-lab.html'
    success_url = reverse_lazy('lab_list')

    def dispatch(self, request, *args, **kwargs):
        id_user = request.user.id
        user = User.objects.get(id=id_user)
        group = user.groups.filter(name='Operator')
        if len(group) == 0 :
            return redirect('authenticate')
        return super().dispatch(request, *args, **kwargs)

class DeleteLabView(DeleteView):
    model = LabKBK
    template_name = 'manage/lab/delete-lab.html'
    success_url = reverse_lazy('lab_list')
    # permission_required = 'dosen.delete_dosen'

    def dispatch(self, request, *args, **kwargs):
        id_user = request.user.id
        user = User.objects.get(id=id_user)
        group = user.groups.filter(name='Operator')
        if len(group) == 0 :
            return redirect('authenticate')
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if 'pk' in self.kwargs:
            id_lab = self.kwargs['pk']
            labs = LabKBK.objects.get(id=id_lab)
            kbk = get_data_lab(labs)
            context['lab']= kbk
        return context

class ListStrataView(ListView):
    model = Strata
    template_name = "manage/list-strata.html"    
    
    def dispatch(self, request, *args, **kwargs):
        id_user = request.user.id
        user = User.objects.get(id=id_user)
        group = user.groups.filter(name='Operator')
        if len(group) == 0 :
            return redirect('authenticate')
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["stratas"] = get_strata()
        # return redirect('home')
        return context
    
class CreateStrataView(CreateView):
    model = Strata
    form_class = CreateStrataForm
    template_name = 'manage/strata/create-strata.html'
    success_url = reverse_lazy('strata_list')

    def dispatch(self, request, *args, **kwargs):
        id_user = request.user.id
        user = User.objects.get(id=id_user)
        group = user.groups.filter(name='Operator')
        if len(group) == 0 :
            return redirect('authenticate')
        return super().dispatch(request, *args, **kwargs)

class UpdateStrataView(UpdateView):
    model = Strata
    form_class = UpdateStrataForm
    template_name = 'manage/strata/update-strata.html'
    success_url = reverse_lazy('strata_list')

    def dispatch(self, request, *args, **kwargs):
        id_user = request.user.id
        user = User.objects.get(id=id_user)
        group = user.groups.filter(name='Operator')
        if len(group) == 0 :
            return redirect('authenticate')
        return super().dispatch(request, *args, **kwargs)

class DeleteStrataView(DeleteView):
    model = Strata
    template_name = 'manage/strata/delete-strata.html'
    success_url = reverse_lazy('strata_list')
    # permission_required = 'dosen.delete_dosen'

    def dispatch(self, request, *args, **kwargs):
        id_user = request.user.id
        user = User.objects.get(id=id_user)
        group = user.groups.filter(name='Operator')
        if len(group) == 0 :
            return redirect('authenticate')
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if 'pk' in self.kwargs:
            id_str = self.kwargs['pk']
            strata = Strata.objects.get(id=id_str)
            data = get_data_strata(strata)
            context['strata']= data
            context['strata_obj']=strata
        return context