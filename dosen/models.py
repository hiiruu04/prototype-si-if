from django.db import models
from django.conf import settings
# from django.contrib.auth.models import User
# from ..users.models import User 
from users.models import User
from .utils import rename_and_path
from .storage import OverwriteStorage

class Golongan (models.Model):
    golongan = models.CharField(max_length=5)
    pangkat = models.CharField(max_length=20)

    def __str__(self):
        golpang = self.golongan + " - " + self.pangkat
        return golpang

class Jabfung (models.Model):
    jabatan = models.CharField(max_length=20)
    owner = models.ManyToManyField(
        'Dosen',
        through='SKJabfung',
        through_fields=('jabatan','pemilik'),
        )

    def __str__(self):
        return self.jabatan

class Status (models.Model):
    status = models.CharField(max_length=20)

    def __str__(self):
        return self.status

class LabKBK (models.Model):
    namaLab = models.CharField(max_length=20)
    keterangan = models.CharField(max_length=100, null=True, blank=True)
    deskripsi = models.TextField(null=True, blank=True)

    def __str__(self):
        return self.namaLab

class Strata (models.Model):
    strata = models.CharField(max_length=10)
    keterangan = models.CharField(max_length=30, null=True, blank=True)

    def __str__(self):
        return self.strata

class Operator (models.Model):
    user = models.OneToOneField(User, related_name='user_operator', on_delete=models.CASCADE, default=None)
    # email = models.EmailField(max_length=254)
    nama = models.CharField(max_length=20)

    def __str__(self):
        return self.nama

class Admin (models.Model):
    user = models.OneToOneField(User, related_name='user_admin', on_delete=models.CASCADE, default=None)
    # email = models.EmailField(max_length=254)
    nama = models.CharField(max_length=20)

    def __str__(self):
        return self.nama

class Dosen (models.Model):
    JENIS_KEL = {
        ('L','Laki-laki'),
        ('P','Perempuan')
    }
    user = models.OneToOneField(User, related_name='user_dosen', on_delete=models.CASCADE, default=None)
    # user = models.OneToOneField(settings.AUTH_USER_MODEL, related_name='dosen_created', on_delete=models.CASCADE)
    nip = models.CharField(max_length=18, null=True, blank=True)
    nidn = models.CharField(max_length=10, null=True, blank=True)
    namaDepan = models.CharField(max_length=20)
    namaTengah = models.CharField(max_length=50, null=True, blank=True)
    namaBelakang = models.CharField(max_length=50, null=True, blank=True)
    gelarDepan = models.CharField(max_length=20, null=True, blank=True)
    gelarBelakang = models.CharField(max_length=50, null=True, blank=True)
    foto = models.ImageField(max_length=100, storage=OverwriteStorage(), upload_to=rename_and_path('img/dosen'), null=True, blank=True)
    golongan = models.ForeignKey(Golongan , on_delete=models.SET_NULL, null=True, blank=True)
    status = models.ForeignKey(Status, on_delete=models.SET_NULL, null=True, blank=True)
    jenkel = models.CharField(choices=JENIS_KEL, max_length=20, null=True, blank=True)
    tempat_lahir = models.CharField(max_length=20, null=True, blank=True)
    tanggal_lahir = models.DateField(auto_now_add=False, null=True, blank=True)
    alamat_rumah = models.TextField(null=True, blank=True)
    no_telp = models.CharField(max_length=16, null=True, blank=True)
    kd_doswal = models.CharField(max_length=4, null=True, blank=True)
    alamat_kantor = models.TextField(null=True, blank=True)
    ruangan = models.CharField(max_length=10, null=True, blank=True)
    keahlian = models.CharField(max_length=100, null=True, blank=True)
    lab_kbk = models.ForeignKey(LabKBK, on_delete=models.SET_NULL, null=True, blank=True)
    email = models.EmailField(max_length=254)
    website = models.CharField(max_length=50, null=True, blank=True)
    prodi = models.CharField(max_length=50, null=True, blank=True)

    # #Membuat aturan untuk ListView
    # class Meta:
        # ordering = ['-created']
    def fullname(self):
        fullname = self.namaDepan
        if self.namaTengah :
            fullname = fullname +" "+ self.namaTengah
        if self.namaBelakang :
            fullname = fullname +" "+ self.namaBelakang
        return fullname
    def fullnamagelar(self):
        fullnamagelar = ""
        if self.gelarDepan :
            fullnamagelar = self.gelarDepan +" "+self.fullname() 
        else:
            fullnamagelar = self.fullname()
        if self.gelarBelakang :
            fullnamagelar = fullnamagelar +" "+ self.gelarBelakang
        return fullnamagelar
    def __str__(self):
        fullname = self.namaDepan
        if self.namaTengah :
            fullname = fullname +" "+ self.namaTengah
        if self.namaBelakang :
            fullname = fullname +" "+ self.namaBelakang
        return fullname

class SKJabfung (models.Model):
    jabatan = models.ForeignKey(Jabfung, on_delete=models.CASCADE)
    pemilik = models.ForeignKey(Dosen, on_delete=models.CASCADE)
    fileSK = models.ImageField(max_length=100, storage=OverwriteStorage(), upload_to=rename_and_path('img/sk'))
    tanggalSK = models.DateField(auto_now_add=False, null=True, blank=True)

class Ijazah (models.Model):
    dosen = models.ForeignKey(Dosen, on_delete=models.CASCADE)
    strata = models.ForeignKey(Strata, on_delete=models.CASCADE) #ganti nama = jenjang?
    bidang = models.CharField(max_length=50, null=True, blank=True)
    asalSekolah = models.CharField(max_length=50, null=True, blank=True)
    asalNegara = models.CharField(max_length=50, null=True, blank=True)
    fileIjazah = models.ImageField(max_length=100, storage=OverwriteStorage(), upload_to=rename_and_path('img/ijazah'))
    tahunLulus = models.PositiveIntegerField(null=True, blank=True)
    keterangan = models.TextField(null=True, blank=True)
    
    def __str__(self):
        namaIjazah = "{} {} {}".format(self.strata, self.bidang, self.asalSekolah)
        return namaIjazah

