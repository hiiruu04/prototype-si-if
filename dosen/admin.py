from django.contrib import admin
from .models import Dosen, Admin, Operator, LabKBK, Status, Jabfung, Golongan, Ijazah, SKJabfung

# Register your models here.

admin.site.register(Admin)

admin.site.register(Operator)

admin.site.register(LabKBK)

admin.site.register(Status)

admin.site.register(Jabfung)

# admin.site.register(SKJabfung)

admin.site.register(Golongan)


class SKJabfungInline(admin.StackedInline):
    model = SKJabfung   

class IjazahInline(admin.StackedInline):
    model = Ijazah   
    
@admin.register(Dosen)
class DosenAdmin(admin.ModelAdmin):
    list_display=['namaDepan','foto','nip']
    inlines = [IjazahInline, SKJabfungInline]
