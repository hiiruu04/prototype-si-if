from django import template
from dosen.models import Ijazah, Dosen
from users.models import User
from django.contrib.auth.models import Group
from django.shortcuts import get_object_or_404
from dosen.models import Ijazah, SKJabfung, Jabfung, LabKBK, Strata
from matkul.models import KontrakPembelajaran, Matkul, Kurikulum, Pengampu
from getter.get import get_data_matkul, matkul_per_semester, get_data_dosen, get_data_lab, get_data_ijazah, get_operator, get_data_sk, get_bahan_kajian, get_lab, get_cpmk, get_data_rps, get_jadwal_pembelajaran, get_comments
from rps.models import RPS, MetodePembelajaran, PengalamanBelajar, BahanKajian, Tim, Anggota
import urllib, io, base64
from matkul.views import curr_curriculum

register = template.Library()

@register.filter()
def get64(url):
    """
        Method returning base64 image data instead of URL
    """
    if url.startswith("http"):
        image = io.StringIO(urllib.urlopen(url).read())
        return 'data:image/jpg;base64,' + base64.b64encode(image.read())

    return url

def xstr(s):
    if s:
        return s
    return ""

@register.filter()
def jarak(min=4):
    return range(1,min)

@register.inclusion_tag('manage/dosen/ijazah.html')
def get_ijazah(dsn_id, edit=False, edit_ijazah=False):
    """
        Tag untuk menampilkan 3 daftar pendidikan terakhir

        Parameter : 
            dsn_id 
                    Merupakan id dosen yang akan ditampilkan pendidikannya
            edit
                    Default False, Jika bernilai True maka akan muncul tombol tambah ijazah

        Output :
            context['daftar-ijazah','edit','id']
                daftar-ijazah 
                        List of Dict
                        Merupakan daftar ijazah yang ditampilkan
                edit 
                        Untuk menampilkan tombol tambah atau tidak
                id
                        Bernilai id dari dosen yang akan ditampilkan ijazahnya
    """
    ijazahs = Ijazah.objects.filter(dosen_id=dsn_id).order_by('-tahunLulus')
    edit_state = edit
    daftar=[]
    if len(ijazahs) > 0:
        count = 0
        for ijazah in ijazahs:
            ijaz = get_data_ijazah(ijazah)
            daftar.append(ijaz)
            count += 1
            # if count>3 :
            #     break
            

    return {'daftar_ijazah':daftar, 'edit':edit, 'id':dsn_id, 'edit_ijazah':edit_ijazah}

@register.inclusion_tag('manage/dosen/sk.html')
def get_sk(dsn_id, edit=False, edit_sk=False):
    skjabfung = SKJabfung.objects.filter(pemilik_id=dsn_id).order_by('-tanggalSK')
    daftar_sk=[]
    if len(skjabfung) > 0 :
        for skjab in skjabfung:
            sk={}
            id_sk = skjab.id
            pemilik = skjab.pemilik_id
            # id_jabatan = skjab.jabatan_id
            tanggal = skjab.tanggalSK
            gambar = skjab.fileSK
            # jabatan = Jabfung.objects.get(id=id_jabatan)
            jabatan = skjab.jabatan
            sk['id'] = id_sk
            sk['pemilik'] = pemilik
            sk['tanggal'] = tanggal
            sk['jabatan'] = jabatan
            sk['gambar'] = gambar
            daftar_sk.append(sk)
    return {'daftar_sk':daftar_sk, 'edit':edit, 'id':dsn_id, 'edit_sk':edit_sk}

@register.inclusion_tag('base/account.html')
def get_info(user_id):
    dosen = False
    user = User.objects.get(id=user_id)
    dosen_name = ""
    username = ""
    if user.groups.all() :
        dosen_group = Group.objects.get(name='Dosen')
        username = user.username
        if dosen_group in user.groups.all():
            dosen = True
            dosen = Dosen.objects.get(user_id=user_id)
            dosen_name = dosen.namaDepan
            username = ""
    return {'dosen_username':dosen_name, 'staff_username':username, 'dosen':dosen}

@register.inclusion_tag('manage/matkul/kontrak-pembelajaran/kontrak-pembelajaran.html')
def get_kontrak(mk_id):
    """
        Tag yang akan digunakan untuk menampilkan kontrak pembelajaran pada tiap matkul
        Parameter :
            mk_id
                    Merupakan id dari matakuliah yang akan ditampilkan kontrak pembelajarannya
        Output :
            context['kontrak-pembelajaran']
                kontrak-pembelajaran 
                        Dict
                        Merupakan data dari kontrak pembelajaran yang akan ditampilkan pada laman web
    """
    kontraks = KontrakPembelajaran.objects.filter(matkul_id=mk_id)
    data = {}
    if RPS.objects.filter(matkul=mk_id).exists():
        rps = RPS.objects.get(matkul=mk_id)
        data["rps"] = rps
        data["jadwals"] = get_jadwal_pembelajaran(rps)
    matkul = Matkul.objects.get(id=mk_id) 
    cpmks = matkul.cpmk.all()
    referensi = "-"
    data['cpmk'] = "-"
    if len(cpmks)>0:
        data['cpmk'] = cpmks
    if len(kontraks) > 0:
        kontrak = kontraks[0]
        data['manfaat'] = "-"
        if kontrak.manfaat_pembelajaran:
            data['manfaat'] = kontrak.manfaat_pembelajaran
        # data['jadwal'] = "-"
        # if kontrak.jadwal_pembelajaran :
        #     data['jadwal'] = kontrak.jadwal_pembelajaran
        # data['materi'] ="-"
        # if kontrak.organisasi_materi:
        #     data['materi'] = kontrak.organisasi_materi
        data['tugas'] = "-"
        if kontrak.tugas:
            data['tugas'] = kontrak.tugas
        data['strategi'] = "-"
        if kontrak.strategi_pembelajaran:
            data['strategi'] = kontrak.strategi_pembelajaran
        data['kriteria'] = "-"
        if kontrak.kriteria_penilaian:
            data['kriteria'] = kontrak.kriteria_penilaian
    
    return {'kontrak':data}

@register.inclusion_tag('manage/custom-tag/sidebar-smt.html')
def get_matkul_smt(kur):
    smt=[]
    pil=[]
    curr_kurikulum = kur
    for semester in range(1,9):
        matkul = matkul_per_semester(smt=semester, kur=curr_kurikulum)
        smt.append(matkul)
    for semester in range(7,9):
        item={}
        pilihan = matkul_per_semester(smt=semester, kur=curr_kurikulum, pilihan=True)
        item['smt'] = semester
        item['matkul'] = pilihan
        pil.append(item)

    return {'semester':smt, 'pilihans':pil, 'kuri':curr_kurikulum.tahun}

@register.inclusion_tag('manage/custom-tag/lab-dosen.html')
def get_lab_dosen(lab_id):
    lab = LabKBK.objects.get(id=lab_id)
    data_lab = get_data_lab(lab)
    nama_lab = data_lab['nama']
    dosens = Dosen.objects.filter(lab_kbk_id=lab_id)
    data = []
    if len(dosens) > 0:
        for dosen in dosens:
            dsn = get_data_dosen(dosen)
            data.append(dsn)
            
    return {'dosens':data, 'nama_lab':nama_lab}

@register.inclusion_tag('manage/custom-tag/rps.html')
def get_rps_tugas(id_dosen):
    data=[]
    curr_dosen = Dosen.objects.get(user_id=id_dosen)
    if curr_dosen.user.groups.filter(name='GPM').exists():
        Rps = RPS.objects.filter(status__in=[1,2])
        for rps in Rps:
            # datum = {}
            # tm = get_data_rps(rps)
            # cmt = get_comments(rps, curr_dosen)
            # datum['rps'] = tm
            # datum['comment'] = cmt['count']
            # datum['unread'] = cmt['notread']
            # data.append(datum)
            tm = {}
            id_rps = rps.id
            matkul = rps.matkul
            status = rps.status
            tm['id'] = id_rps
            tm['matkul'] = matkul
            tm['status'] = status
            data.append(tm)
    elif curr_dosen.user.groups.filter(name='Kaprodi').exists():
        Rps = RPS.objects.filter(status__in=[2,3])
        for rps in Rps:
            tm = {}
            id_rps = rps.id
            matkul = rps.matkul
            status = rps.status
            tm['id'] = id_rps
            tm['matkul'] = matkul
            tm['status'] = status
            data.append(tm)
    else:
        gg = Anggota.objects.filter(dosen=curr_dosen).exists()
        tim = None
        if gg:
            anggota = Anggota.objects.get(dosen=curr_dosen)
            tim = anggota.tim
            Rps = anggota.tim.rps.all()
        if tim is not None:
            for rps in Rps:
                tm = {}
                id_rps = rps.id
                matkul = rps.matkul
                status = rps.status
                tm['id'] = id_rps
                tm['matkul'] = matkul
                tm['status'] = status
                data.append(tm)
    return {'id_dosen':id_dosen, 'kmpl_rps':data }

@register.inclusion_tag('manage/custom-tag/operator.html')
def get_daftar_operator():
    data= get_operator()
    return {'operators':data}

@register.inclusion_tag('manage/custom-tag/delete-strata.html')
def get_delete_strata(curr_strata):
    data = []
    ijazahs = Ijazah.objects.filter(strata=curr_strata)
    if len(ijazahs) > 0:
        for ijazah in ijazahs:
            ijz = get_data_ijazah(ijazah)
            dt = {}
            id_dosen = ijz['dosen']
            curr_dosen = get_object_or_404(Dosen, id=id_dosen)
            dosen = get_data_dosen(curr_dosen)
            dt['dosen'] = dosen
            dt['data'] = ijz
            data.append(dt)
    return {'ijazahs':data}

@register.inclusion_tag('manage/custom-tag/delete-jabatan.html')
def get_delete_jabatan(curr_jabatan):
    data = []
    sks = SKJabfung.objects.filter(jabatan=curr_jabatan)
    if len(sks) > 0:
        for sk in sks:
            jab = get_data_sk(sk)
            dt = {}
            id_dosen = jab['dosen']
            curr_dosen = get_object_or_404(Dosen, id=id_dosen)
            dosen = get_data_dosen(curr_dosen)
            dt['dosen'] = dosen
            dt['data'] = jab
            data.append(dt)
    return {'sks':data}

@register.inclusion_tag('manage/custom-tag/kurikulum.html')
def get_kurikulum():
    kurs = Kurikulum.objects.all()
    return {'kurs':kurs}

@register.inclusion_tag('manage/custom-tag/lab_nav.html')
def lab_nav():
    data = get_lab()
    return {'labs':data}

@register.inclusion_tag('manage/custom-tag/data-rps-konpem.html')
def get_data_rps_konpem(id_matkul):
    matkul = Matkul.objects.get(id=id_matkul)
    rps = None
    referensi = "-"
    cpmk = "-"
    jadwal = "-"
    if len(RPS.objects.filter(matkul=matkul)) > 0 :
        rps = RPS.objects.get(matkul=matkul)
        data_rps = get_data_rps(rps)
        referensi = data_rps['referensi']
        cpmk = get_cpmk(matkul)
        jadwal = get_jadwal_pembelajaran(rps)
    return {'cpmks':cpmk, 'referensi':referensi, 'jadwals':jadwal}

@register.inclusion_tag('manage/custom-tag/kriteria_crud.html')
def get_data_kriteria_konpem(matkul_id):
    konpem = KontrakPembelajaran.objects.get(matkul_id=matkul_id)
    data = None
    if konpem :
        data = konpem
    return {'konpem':data}

@register.inclusion_tag('manage/custom-tag/daftar-comment.html')
def daftar_comment(id_rps):
    rps = RPS.objects.get(id=id_rps)
    data = get_comments(rps)
    return {'comments':data}

@register.inclusion_tag('manage/custom-tag/matkul-pengampu.html')
def get_matkul_pengampu(id_pengampu):
    dosen = Dosen.objects.get(user_id=id_pengampu)
    b = Pengampu.objects.filter(dosen_pengampu=dosen.id)
    data = []
    for pengampu in b:
        if pengampu.mata_kuliah.kurikulum == curr_curriculum() and pengampu.mata_kuliah not in data:
            data.append(pengampu.mata_kuliah)
    return {'matkul': data}
    
