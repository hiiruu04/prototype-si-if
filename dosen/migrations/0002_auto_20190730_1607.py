# Generated by Django 2.1.5 on 2019-07-30 16:07

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('dosen', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='dosen',
            name='jenkel',
            field=models.CharField(blank=True, choices=[('L', 'Laki-laki'), ('P', 'Perempuan')], max_length=20, null=True),
        ),
        migrations.AlterField(
            model_name='dosen',
            name='namaBelakang',
            field=models.CharField(blank=True, max_length=50, null=True),
        ),
    ]
