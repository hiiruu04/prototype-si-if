from django import forms
from django.forms import DateInput, DateField
from users.models import User
from .models import Dosen, SKJabfung, Ijazah, Jabfung, Golongan, Status, LabKBK, Strata, Operator
from django.contrib.auth.forms import UserCreationForm
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Fieldset, ButtonHolder, Submit, Row, Column, Button, Field, Fieldset, Div, HTML
from crispy_forms.bootstrap import FormActions, TabHolder, Tab
from betterforms.multiform import MultiModelForm
from django.forms.models import formset_factory
from django.core.validators import MaxValueValidator, MinValueValidator, RegexValidator
from string import Template
from django.utils.safestring import mark_safe
from bootstrap_datepicker_plus import DatePickerInput
from .utils import current_year


TAHUN = range(1900,current_year()+1,1)
BULAN = {
            1:'Januari', 2:'Februari', 3:'Maret', 4:'April',
            5:'Mei', 6:'Juni', 7:'Juli', 8:'Agustus',
            9:'September', 10:'Oktober', 11:'November', 12:'Desember'
        }

PESAN = {
    'numerik': 'Masukkan harus berupa numerik',
    'alfabet': 'Masukkan harus berupa alfabet dan beberapa karakter yang diperbolehkan saja',
    'mintahun': 'Masukkan tahun harus lebih dari atau sama dengan 1990',
    'maxtahun': 'Masukkan tahun harus kurang dari atau sama dengan {}'.format(current_year()),
    'link': 'Masukkan url harus dengan format yang benar! tanpa http:// ataupun https://',
}

REGEX_CODE = {
    'numerik': '^[0-9]+$',
    'alfabet': '^[A-z]+([/(),.\'@%\":;+-]*.[A-z]+[/(),.\'@%\":;+-]?)*$',
    'link': '^(www\.)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$', 
}

class LoginForm(forms.Form):
    """
        Form yang digunakan pada laman login
        UNUSED
    """
    username = forms.CharField(max_length=100, required=True)
    # email = forms.EmailField(required=True)
    password = forms.CharField(required=True, widget=forms.PasswordInput)
        
class CreateDosenForm(UserCreationForm):
    """
        Form yang digunakan untuk membuat dosen (C)
        View = OperatorCreateDosenView
    """
    Nama_Depan = forms.CharField(max_length=20)
    Nama_Tengah = forms.CharField(max_length=50, required=False)
    Nama_Belakang = forms.CharField(max_length=20, required=False)
    
    class Meta(UserCreationForm.Meta):
        model = User
        fields = ['username', 'email']
        widgets = {
            'username':forms.HiddenInput(),
        }
    
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for fieldname in ['username', 'password1', 'password2']:
            self.fields[fieldname].help_text = None
        self.helper = FormHelper()
        self.helper.layout = Layout(
            Row(
                Column('Nama_Depan', css_class='form-group col-4 col-md-4 mb-0'),
                Column('Nama_Tengah', css_class='form-group col-4 col-md-4 mb-0'),
                Column('Nama_Belakang', css_class='form-group col-4 col-md-4 mb-0'),
                css_class='form-row'
            ),
            Row(
                Column('email', css_class='form-group col-md-6 mb-0'),
                Column('username', css_class='form-group col-md-6 mb-0'),
                css_class='form-row'
            ),
            Row(
                Column('password1', css_class='form-group col-md-6 mb-0'),
                Column('password2', css_class='form-group col-md-6 mb-0'),
                css_class='form-row'
            ),
            FormActions(
                Submit('submit', 'Simpan'),
                Button('cancel', 'Batal'),
                css_class = 'form-row'
            ),
        )
        self.fields['Nama_Depan'] = forms.CharField(max_length=15, validators=[RegexValidator(REGEX_CODE['alfabet'], message=PESAN['alfabet'])])
        self.fields['Nama_Tengah'] = forms.CharField(max_length=15, validators=[RegexValidator(REGEX_CODE['alfabet'], message=PESAN['alfabet'])], required=False)
        self.fields['Nama_Belakang'] = forms.CharField(max_length=15, validators=[RegexValidator(REGEX_CODE['alfabet'], message=PESAN['alfabet'])], required=False)

    def clean(self):
        cd = super().clean()
        tengah = cd.get('Nama_Tengah')
        belakang = cd.get('Nama_Belakang')
        if tengah and not belakang:
            raise forms.ValidationError('Nama Belakang harus diisi jika ingin mengisi nama tengah')

# Dosen

class UpdateDosenForm(forms.ModelForm):
    """
        Form yang digunakan untuk mengubah dosen (U)
        View = DosenUpdateDataView, OperatorUpdateDosenView
    """
    class Meta():
        model = Dosen
        exclude = ['user']
        widgets = {
            'tanggal_lahir': forms.SelectDateWidget(years=TAHUN, months=BULAN,),
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)   
        self.helper = FormHelper()
        self.helper.layout = Layout(
            TabHolder(
                Tab('Data Diri',
                    Row(
                        Column('namaDepan', css_class='form-group col-md-4 mb-0'),
                        Column('namaTengah', css_class='form-group col-md-4 mb-0'),
                        Column('namaBelakang', css_class='form-group col-md-4 mb-0'),
                        css_class='form-row'
                    ),
                    Row(
                        Column('gelarDepan', css_class='form-group col-md-6 mb-0'),
                        Column('gelarBelakang', css_class='form-group col-md-6 mb-0'),
                        css_class='form-row'
                    ),
                    Row(
                        Column('email', css_class='form-group col-md-6 mb-0'),
                        Column('no_telp', css_class='form-group col-md-6 mb-0'),
                        css_class='form-row'
                    ),
                    Row(
                        Column('tempat_lahir', css_class='form-group col-md-6 mb-0'),
                        Column('tanggal_lahir', css_class='form-group col-md-6 mb-0'),
                        css_class='form-row'
                    ),
                    'foto',
                    Field('alamat_rumah', css_class='noresize', rows=3),
                    'keahlian',
                    Field('website',placeholder='http://'),
                ),
                Tab('Data Kepegawaian',
                    Row(
                        Column('nidn', css_class='form-group col-md-6 mb-0'),
                        Column('nip', css_class='form-group col-md-6 mb-0'),
                        css_class='form-row'
                    ),
                    Row(
                        Column('prodi', css_class='form-group col-md-4 mb-0'),
                        Column('lab_kbk', css_class='form-group col-md-2 mb-0'),
                        Column('golongan', css_class='form-group col-md-2 mb-0'),
                        Column('status', css_class='form-group col-md-2 mb-0'),
                        Column('kd_doswal', css_class='form-group col-md-2 mb-0'),
                        css_class='form-row'
                    ),
                    Row(
                        Div(Field('alamat_kantor', css_class='noresize', rows=3), css_class='form-group col-md-10'),
                        Column('ruangan', css_class='form-group col-md-2'),
                        css_class='form_row'
                    )
                ),
                Tab('Lainnya',
                    Row(
                        Column(
                            Field(
                                HTML("""{% load custom_tags %}
                                <p>Jabatan sekarang : {{ jabatan }} <a class="btn-sm btn-success" href="{% url 'dosen_sk' pk=id %}">+ Update Jabatan</a> </p>
                                {% get_ijazah dsn_id=id edit=True %}""")
                        )),
                    ),
                ),
            ),
            FormActions(
                Submit('submit', 'Simpan'),
                Button('cancel', 'Batal'),
                css_class = 'row form-row',
            ),
        )
        self.fields['no_telp'] = forms.CharField(max_length=15, validators=[RegexValidator(REGEX_CODE['numerik'], message=PESAN['numerik'])], required=False)
        self.fields['nidn'] = forms.CharField(max_length=15, validators=[RegexValidator(REGEX_CODE['numerik'], message=PESAN['numerik'])], required=False)
        self.fields['tempat_lahir'] = forms.CharField(max_length=50, validators=[RegexValidator(REGEX_CODE['alfabet'], message=PESAN['alfabet'])], required=False)
        self.fields['website'] = forms.CharField(max_length=50, validators=[RegexValidator(REGEX_CODE['link'], message=PESAN['link'])], required=False)
        self.fields['namaDepan'] = forms.CharField(max_length=15, validators=[RegexValidator(REGEX_CODE['alfabet'], message=PESAN['alfabet'])])
        self.fields['namaTengah'] = forms.CharField(max_length=50, validators=[RegexValidator(REGEX_CODE['alfabet'], message=PESAN['alfabet'])], required=False)
        self.fields['namaBelakang'] = forms.CharField(max_length=30, validators=[RegexValidator(REGEX_CODE['alfabet'], message=PESAN['alfabet'])], required=False)
        self.fields['gelarDepan'] = forms.CharField(max_length=15, validators=[RegexValidator(REGEX_CODE['alfabet'], message=PESAN['alfabet'])], required=False)
        # self.fields['gelarBelakang'] = forms.CharField(max_length=50, validators=[RegexValidator(REGEX_CODE['alfabet'], message=PESAN['alfabet'])], required=False)

    def clean(self):
        cd = super().clean()
        tengah = cd.get('namaTengah')
        belakang = cd.get('namaBelakang')
        if tengah and not belakang:
            raise forms.ValidationError('Nama Belakang harus diisi jika ingin mengisi nama tengah')

class UpdateDosenDataPribadi(forms.ModelForm):
    """
        UNUSED
    """
    class Meta():
        model = Dosen
        fields = ['foto', 'namaDepan', 'namaTengah', 'namaBelakang', 'gelarDepan', 'gelarBelakang', 'jenkel', 'tempat_lahir', 'tanggal_lahir', 'alamat_rumah', 'no_telp', 'email', 'website',]
        widgets = {
            'tanggal_lahir': forms.SelectDateWidget(years=TAHUN, months=BULAN),
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)   
        self.helper = FormHelper()
        self.helper.layout = Layout(
            Row(
                Column('namaDepan', css_class='form-group col-md-4 mb-0'),
                Column('namaTengah', css_class='form-group col-md-4 mb-0'),
                Column('namaBelakang', css_class='form-group col-md-4 mb-0'),
                css_class='form-row'
            ),
            FormActions(
                Submit('submit', 'Save'),
                Button('cancel', 'Cancel')
            ),
        )

### ADMIN ###
class CreateOperatorForm(UserCreationForm):
    """
        Form yang digunakan untuk membuat operator (C)
        View = CreateOperator
    """
    namaOp = forms.CharField(max_length=50)
    class Meta(UserCreationForm.Meta):
        model = User
        fields = ('email', 'username')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for fieldname in ['username', 'password1', 'password2']:
            self.fields[fieldname].help_text = None
        self.helper = FormHelper()
        self.helper.layout = Layout(
            Row(
                Column('namaOp', css_class='form-group col-md-4 mb-0'),
                Column('email', css_class='form-group col-md-4 mb-0'),
                Column('username', css_class='form-group col-md-4 mb-0'),
                css_class='form-row'
            ),
            Row(
                Column('password1', css_class='form-group col-md-6 mb-0'),
                Column('password2', css_class='form-group col-md-6 mb-0'),
                css_class='form-row'
            ),
            FormActions(
                Submit('submit', 'Simpan'),
                Button('cancel', 'Batal'),
                css_class = 'form-row'
            ),
        )
        self.fields['namaOp'] = forms.CharField(max_length=15, validators=[RegexValidator(REGEX_CODE['alfabet'], message=PESAN['alfabet'])])

    def clean(self):
        cd = super().clean()
        curr_name = cd.get('namaOp')
        qs = Operator.objects.filter(nama=curr_name)
        if len(qs)>0:
            raise forms.ValidationError('Nama Operator sudah ada')


# Multi model form
class CreateIjazahForm(forms.ModelForm):
    """
        Form yang digunakan untuk membuat ijazah untuk setiap dosen
        View = CreateIjazahView
    """
    
    # asalNegara = forms.CharField(max_length=100, validators=[RegexValidator(REGEX_CODE['alfabet'], message=PESAN['alfabet'])], required=False)    
    class Meta():
        model = Ijazah
        fields = ['strata','bidang','asalSekolah','asalNegara','fileIjazah','tahunLulus','keterangan']
        # exclude = ['dosen',]
        widgets={
            'dosen' : forms.HiddenInput(),
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # for fieldname in ['username', 'password1', 'password2']:
        #     self.fields[fieldname].help_text = None
        self.helper = FormHelper()
        self.helper.layout = Layout(
            Row(
                Column('strata', css_class='form-group col-md-2 mb-0'),
                Column('bidang', css_class='form-group col-md-10 mb-0'),
                css_class='form-row'
            ),
            Row(
                Column('asalSekolah', css_class='form-group col-md-6 mb-0'),
                Column('asalNegara', css_class='form-group col-md-6 mb-0'),
                css_class='form-row'
            ),
            Row(
                Column('fileIjazah', css_class='form-group col-md-6 mb-0'),
                Column('tahunLulus', css_class='form-group col-md-6 mb-0'),
                css_class='form-row'
            ),
            Row(
                Column('keterangan', css_class='form-group col-md-12 mb-0'),
                css_class='form-row'
            ),
        )
        self.fields['asalNegara'] = forms.CharField(max_length=15, validators=[RegexValidator(REGEX_CODE['alfabet'], message=PESAN['alfabet'])])
        self.fields['tahunLulus'] = forms.IntegerField(validators=[MinValueValidator(1990, message=PESAN['mintahun']), MaxValueValidator(current_year(),message=PESAN['maxtahun'])], required=False)

class UpdateIjazahForm(forms.ModelForm):
    tahunLulus = forms.IntegerField(validators=[MinValueValidator(1990, message=PESAN['mintahun']), MaxValueValidator(current_year(),message=PESAN['maxtahun'])], required=False)
    asalNegara = forms.CharField(max_length=100, validators=[RegexValidator(REGEX_CODE['alfabet'], message=PESAN['alfabet'])], required=False)    
    class Meta():
        model = Ijazah
        exclude = ['dosen']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # for fieldname in ['username', 'password1', 'password2']:
        #     self.fields[fieldname].help_text = None
        self.helper = FormHelper()
        self.helper.layout = Layout(
            Row(
                Column('strata', css_class='form-group col-md-2 mb-0'),
                Column('bidang', css_class='form-group col-md-10 mb-0'),
                css_class='form-row'
            ),
            Row(
                Column('asalSekolah', css_class='form-group col-md-6 mb-0'),
                Column('asalNegara', css_class='form-group col-md-6 mb-0'),
                css_class='form-row'
            ),
            Row(
                Column('fileIjazah', css_class='form-group col-md-6 mb-0'),
                Column('tahunLulus', css_class='form-group col-md-6 mb-0'),
                css_class='form-row'
            ),
            Row(
                Column('keterangan', css_class='form-group col-md-12 mb-0'),
                css_class='form-row'
            ),
        )
        self.fields['asalNegara'] = forms.CharField(max_length=15, validators=[RegexValidator(REGEX_CODE['alfabet'], message=PESAN['alfabet'])])
        self.fields['tahunLulus'] = forms.IntegerField(validators=[MinValueValidator(1990, message=PESAN['mintahun']), MaxValueValidator(current_year(),message=PESAN['maxtahun'])], required=False)

class CreateSKJabatanForm(forms.ModelForm):
    """
        Form yang digunakan untuk membuat sk jabatan untuk setiap dosen
        View = CreateSKJabatanView
    """

    class Meta():
        model = SKJabfung
        fields = ['jabatan','fileSK','tanggalSK']
        # exclude = ['pemilik',]
        widgets={
            'pemilik' : forms.HiddenInput(),
            'tanggalSK' : forms.SelectDateWidget(years=TAHUN, months=BULAN),
            # 'tanggalSK' : DatePickerInput(),
        } 

    def __init__(self, *args, **kwargs):
        self.id = kwargs.pop('id_dosen', None)
        super(CreateSKJabatanForm, self).__init__(*args, **kwargs)
        # for fieldname in ['username', 'password1', 'password2']:
        #     self.fields[fieldname].help_text = None
        self.helper = FormHelper()
        self.helper.layout = Layout(
            Row(
                Column('jabatan', css_class='form-group col-md-8 mb-0'),
                Column('fileSK', css_class='form-group col-md-4 mb-0'),
                css_class='form-row'
            ),
            Row(
                Column('tanggalSK', css_class='form-group col-md-12 mb-0'),
                css_class='form-row'
            ),
        )

    def clean(self):
        cd = super().clean()
        jab = cd.get('jabatan')
        id_pemilik = self.id
        qs = SKJabfung.objects.filter(pemilik_id = id_pemilik, jabatan_id = jab)
        if len(qs) > 0:
            raise forms.ValidationError('Dosen sudah memiliki sk jabatan yang sama')

class UpdateSKJabatanForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        self.id = kwargs.pop('id_dosen', None)
        super(UpdateSKJabatanForm, self).__init__(*args, **kwargs)

    class Meta():
        model = SKJabfung
        exclude = ['pemilik']
        widgets={
            'tanggalSK' : forms.SelectDateWidget(years=TAHUN, months=BULAN),
        }
    
    def clean(self):
        cd = super().clean()
        jab = cd.get('jabatan')
        curr_jab = self.instance.pk
        id_pemilik = self.id
        qs = SKJabfung.objects.filter(pemilik_id=id_pemilik, jabatan_id=jab)
        qs = qs.exclude(pk=curr_jab)
        if len(qs) > 0:
            raise forms.ValidationError('Dosen sudah memiliki sk jabatan yang sama')

class CreateJabatanForm(forms.ModelForm):
    jabatan = forms.CharField(max_length=20, validators=[RegexValidator(REGEX_CODE['alfabet'], message=PESAN['alfabet'])],)
    class Meta():
        model = Jabfung
        fields = ['jabatan']
    
    def clean(self):
        cd = super().clean()
        curr_jabatan = cd.get('jabatan')
        qs = Jabfung.objects.filter(jabatan=curr_jabatan)
        if len(qs)>0:
            raise forms.ValidationError('Jabatan sudah ada')
        

class UpdateJabatanForm(forms.ModelForm):
    jabatan = forms.CharField(max_length=20, validators=[RegexValidator(REGEX_CODE['alfabet'], message=PESAN['alfabet'])],)
    class Meta():
        model = Jabfung
        fields = ['jabatan']

    def clean(self):
        cd = super().clean()
        curr_jabatan = cd.get('jabatan')
        qs = Jabfung.objects.filter(jabatan=curr_jabatan)
        if self.instance:
            qs = qs.exclude(pk=self.instance.pk)
        if len(qs)>0:
            raise forms.ValidationError('Jabatan sudah ada')

class CreateGolonganForm(forms.ModelForm):
    golongan = forms.CharField(max_length=5, validators=[RegexValidator(REGEX_CODE['alfabet'], message=PESAN['alfabet'])],)
    class Meta():
        model = Golongan
        fields = ['golongan','pangkat']

        
        
class UpdateGolonganForm(forms.ModelForm):
    golongan = forms.CharField(max_length=5, validators=[RegexValidator(REGEX_CODE['alfabet'], message=PESAN['alfabet'])],)
    class Meta():
        model = Golongan
        fields = ['golongan','pangkat']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            Field('golongan', css_class="form-group"),
            Field('pangkat', css_class="form_group"),
            FormActions(
                Submit('submit', 'Create'),
                Button('cancel', 'Cancel'),
                css_class = "form-group"
            ),
        )

    def clean(self):
        cd = super().clean()
        curr_golongan = cd.get('golongan')
        curr_pangkat = cd.get('pangkat')
        qs = Golongan.objects.filter(golongan=curr_golongan)
        qs2 = Golongan.objects.filter(pangkat=curr_pangkat)
        if self.instance:
            qs = qs.exclude(pk=self.instance.pk)
            qs2 = qs2.exclude(pk=self.instance.pk)
        if len(qs)>0:
            raise forms.ValidationError('golongan sudah ada')
        if len(qs2)>0:
            raise forms.ValidationError('pangkat sudah ada')

class CreateStatusForm(forms.ModelForm):
    status = forms.CharField(max_length=20, validators=[RegexValidator(REGEX_CODE['alfabet'], message=PESAN['alfabet'])],)
    class Meta():
        model = Status
        fields = ['status']
    
    def clean(self):
        cd = super().clean()
        curr_status = cd.get('status')
        qs = Status.objects.filter(status=curr_status)
        if len(qs)>0:
            raise forms.ValidationError('Status sudah ada')

class UpdateStatusForm(forms.ModelForm):
    status = forms.CharField(max_length=20, validators=[RegexValidator(REGEX_CODE['alfabet'], message=PESAN['alfabet'])],)
    class Meta():
        model = Status
        fields = ['status']

    def clean(self):
        cd = super().clean()
        curr_status = cd.get('status')
        qs = Status.objects.filter(status=curr_status)
        qs = qs.exclude(pk=self.instance.pk)
        if len(qs)>0:
            raise forms.ValidationError('Status sudah ada')
    

class CreateLabKBKForm(forms.ModelForm):
    namaLab = forms.CharField(max_length=20, validators=[RegexValidator(REGEX_CODE['alfabet'], message=PESAN['alfabet'])],)
    keterangan = forms.CharField(max_length=100, validators=[RegexValidator(REGEX_CODE['alfabet'], message=PESAN['alfabet'])], required=False)    
    class Meta():
        model = LabKBK
        fields = ['namaLab','keterangan','deskripsi']
    
    def clean(self):
        cd = super().clean()
        curr_lab = cd.get('namaLab')
        qs = LabKBK.objects.filter(namaLab=curr_lab)
        if len(qs)>0:
            raise forms.ValidationError('Lab sudah ada')

class UpdateLabKBKForm(forms.ModelForm):
    namaLab = forms.CharField(max_length=20, validators=[RegexValidator(REGEX_CODE['alfabet'], message=PESAN['alfabet'])],)
    keterangan = forms.CharField(max_length=100, validators=[RegexValidator(REGEX_CODE['alfabet'], message=PESAN['alfabet'])], required=False)    
    class Meta():
        model = LabKBK
        fields = ['namaLab','keterangan','deskripsi']
    
    def clean(self):
        cd = super().clean()
        curr_lab = cd.get('namaLab')
        qs = LabKBK.objects.filter(namaLab=curr_lab)
        if self.instance:
            qs = qs.exclude(pk=self.instance.pk)
        if len(qs)>0:
            raise forms.ValidationError('Lab sudah ada')

class CreateStrataForm(forms.ModelForm):
    class Meta():
        model = Strata
        fields = ['strata','keterangan']

    def clean(self):
        cd = super().clean()
        curr_strata = cd.get('strata')
        qs = Strata.objects.filter(strata=curr_strata)
        if len(qs)>0:
            raise forms.ValidationError('Strata sudah ada')

class UpdateStrataForm(forms.ModelForm):
    class Meta():
        model = Strata
        fields = ['strata','keterangan']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            Field('strata', css_class="form-group"),
            Field('keterangan', css_class="form_group"),
            FormActions(
                Submit('submit', 'Save'),
                Button('cancel', 'Cancel'),
                css_class = "form-group"
            ),
        )

    def clean(self):
        cd = super().clean()
        curr_strata = cd.get('strata')
        qs = Strata.objects.filter(strata=curr_strata)
        if self.instance:
            qs = qs.exclude(pk=self.instance.pk)
        if len(qs)>0:
            raise forms.ValidationError('Strata sudah ada')
    
