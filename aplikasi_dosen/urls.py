from django.contrib import admin
from django.urls import path, re_path, include
from django.conf import settings
from django.conf.urls.static import static
from dosen import views
from matkul import views as matkulview
from jadwalkuliah.views import index

urlpatterns = [
    path('su/', admin.site.urls, name='su'),
    path('admin/', include('users.urls')),
    path('staff/', include('dosen.urls')),
    path('matkul/', include('matkul.urls')),
    path("rps/", include('rps.urls')),
    path('jadwalkuliah/', index, name="index"),
    path('', views.homeView , name='home'),
    # urls untuk API
    re_path('api/(?P<version>(v1|v2))/matkul/', matkulview.ListMatkulsView.as_view(), name="matkuls-all"),
    re_path('api/(?P<version>(v1|v2))/pengampu/', matkulview.ListPengampuView.as_view(), name="pengampus-all"),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
