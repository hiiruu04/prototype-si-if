from django import forms
from django.forms import DateInput, DateField
from users.models import User
from matkul.models import Kurikulum, Matkul, KontrakPembelajaran, Pengampu
from .models import RPS, MetodePembelajaran, PengalamanBelajar, BahanKajian, Tim, CommentRPS, Anggota
from django.contrib.auth.forms import UserCreationForm
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Fieldset, ButtonHolder, Submit, Row, Column, Button, Field, Fieldset, Div, HTML
from crispy_forms.bootstrap import FormActions, TabHolder, Tab
from betterforms.multiform import MultiModelForm
from ckeditor.fields import RichTextField, CKEditorWidget
from django.core.validators import MaxValueValidator, MinValueValidator, RegexValidator
from dosen.utils import xint
from bs4 import BeautifulSoup
from dosen.models import Dosen
from dosen.views import get_data_dosen


PESAN = {
    'deskripsi_matkul': 'Tulis Deskripsi Matkul Maximal 5 Paragraf (LineBreak)',
    'referensi': 'Referensi lebih dari satu harus menggunakan numbering',
    'link': 'Masukkan url harus dengan format yang benar! tanpa http:// ataupun https://',
}

REGEX_CODE = {
    # 'deskripsi_matkul': '(\s*(<p>)([A-z]+([/(),.\'@%\":;+-]*.[A-z]+[/(),.\'@%\":;+-]?)*$)*(</p>)\s*){0,5}',
    'referensi': '.*\/ol>$',
    'link': '^(www\.)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$', 
}

def get_refference(id_rps):
    list_ref = ()
    refs = RPS.objects.get(id=id_rps).referensi
    if len(refs)<1:
        return list_ref
    tags = BeautifulSoup(refs, "html.parser")
    i = 0
    for tag in tags('li'):
        i += 1
        ref = "{}. {}".format(str(i), tag.get_text())
        ref_no = str(i)
        list_ref += ((ref_no,ref),)
    return list_ref

def mg_choices(id_rps):
    mg = ()
    max_mg = 16
    list_mg = list(range(1, max_mg+1)) # Default jika RPS belum memiliki bahan kajian
    list_bahan = BahanKajian.objects.filter(rps_id=id_rps) # Mendapat list bahan kajian dengan id rps
    for b in list_bahan:
        list_mg.remove(b.minggu_ke) # Menghapus minggu bahan kajian yang sudah ada di RPS
    for i in list_mg:
        mg += ((i,str(i)),)
    return mg

def no_tim_dosen():
    dosen_tim = Anggota.objects.all()
    list_dosen = []
    usr_gpm = User.objects.filter(groups__name="GPM")[0]
    usr_kaprodi = User.objects.filter(groups__name="Kaprodi")[0]
    gpm = Dosen.objects.get(user=usr_gpm)
    kaprodi = Dosen.objects.get(user=usr_kaprodi)
    list_dosen.append(gpm.id)
    list_dosen.append(kaprodi.id)
    if len(dosen_tim) > 0:
        for anggota in dosen_tim :
            dosen = anggota.dosen.id
            if dosen not in list_dosen :
                list_dosen.append(dosen)
    dosen_no_team = Dosen.objects.all().exclude(id__in=list_dosen)
    return dosen_no_team

def no_tim_rps(exclude=None):
    rps_tim = Tim.objects.all()

    if exclude :
        rps_tim = rps_tim.exclude(id=exclude)
    list_rps = []
    if len(rps_tim) > 0:
        for tim in rps_tim :
            data_rps = tim.rps.all()
            if len(data_rps) > 0:
                for rps in data_rps :
                    data = rps.id
                    list_rps.append(data)
    rps_no_team = RPS.objects.all().exclude(id__in=list_rps)
    return rps_no_team


def jenis_mg_choices(id_rps):
    jns = (("BLJ", 'Belajar'),("UTS", 'UTS'),("UAS", 'UAS'))
    list_bahan = BahanKajian.objects.filter(rps_id=id_rps) # Mendapat list bahan kajian dengan id rps
    for b in list_bahan:
        if b.is_uts is True:
            jns = jns[:1] + jns[2:] 
        if b.is_uas is True:
            jns = jns[:-1]
    return jns

def max_bobot(id_rps,id_bahan):
    list_bahan = BahanKajian.objects.filter(rps_id=id_rps)
    curr_bahan = BahanKajian.objects.get(id=id_bahan)
    sumofbobot = sum(xint(b.bobot) for b in list_bahan) - xint(curr_bahan.bobot)
    return 100-sumofbobot

class MaxVal(MaxValueValidator):
    message = 'Bobot yang anda berikan melebihi batas sisa (maksimal pemberian %(limit_value)s%).'

class MinVal(MinValueValidator):
    message = 'Bobot tidak boleh kurang dari 0%'


class PlotRPSform (forms.ModelForm):
    class Meta():
        model = RPS
        exclude = ()
        widgets = {}

class TolakForm (forms.ModelForm):
    class Meta():
        model = CommentRPS
        exclude = ['rps','checked']
        widgets = {
            'log_status': forms.HiddenInput(),
        }
    def __init__(self, *args, **kwargs): 
        super(TolakForm, self).__init__(*args, **kwargs)
        self.fields['body'] = forms.CharField(widget=forms.Textarea, required=True)


class RPSForm (forms.ModelForm):
    class Meta():
        model = RPS
        fields = ['deskripsi_matkul','referensi']


    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)   
        self.helper = FormHelper()
        self.helper.attrs = {'enctype':"multipart/form-data",'id': "edit-rps-form"}
        self.helper.layout = Layout(
            TabHolder(
                Tab('Deskripsi Matkul',
                    'deskripsi_matkul',
                ),
                Tab('Referensi',
                    'referensi'
                ),
                Tab('Bahan Kajian',
                    Row(
                        Column(
                            Field(
                                HTML("""{% include "manage/rps/rps_tabelbahan.html" %}""")
                        )),
                    ),
                ),
            ),
             FormActions(
                Submit('submit', 'Simpan'),
                Button('cancel', 'Batal')
            ),
        )
        self.fields['deskripsi_matkul'] = forms.CharField(
            # max_length = 100,
            widget = CKEditorWidget(config_name='nul_ckeditor'), 
            required = False
        )
        self.fields['referensi'] = forms.CharField(
            widget = CKEditorWidget(config_name='refference_ckeditor'), 
            # validators = [
            #     RegexValidator(
            #         REGEX_CODE['referensi'], 
            #         message = PESAN['referensi']
            #     )
            # ], 
            required = False
        )
    
    def clean(self, *args, **kwargs):
        cd = super().clean()
        curr_ref = cd.get('referensi')
        if "<p>" in str(curr_ref):
            raise forms.ValidationError("Referensi lebih dari satu harus menggunakan numbering!")


class PilihBahanForm (forms.ModelForm):
    pilih_minggu = forms.CharField(
        max_length=3,
    )

    def __init__(self, *args, **kwargs): 
        self.id = kwargs.pop('rps', None)
        id_rps = self.id 
        super(PilihBahanForm, self).__init__(*args, **kwargs)
        self.fields['minggu_ke'] = forms.IntegerField(widget=forms.Select(choices=mg_choices(id_rps)))
        self.fields['pilih_minggu'] = forms.CharField(max_length=3, widget=forms.Select(choices=jenis_mg_choices(id_rps)))
    
    def clean(self, *args, **kwargs):
        id_rps = self.id
        cd = super().clean()
        curr_pilih = cd.get('pilih_minggu')
        curr_minggu_ke = int(cd.get('minggu_ke'))
        qs = BahanKajian.objects.filter(rps_id=id_rps).order_by('minggu_ke')
        list_minggu = [bahan.minggu_ke for bahan in qs]
        list_true_uas = [bahan.is_uas for bahan in qs]
        print(curr_minggu_ke, curr_pilih, list_minggu, qs, id_rps)
        if not "UAS" in str(curr_pilih):
            if True in list_true_uas:
                mg_uas = BahanKajian.objects.get(rps_id=id_rps, is_uas=True).minggu_ke
                if curr_minggu_ke > mg_uas:
                    raise forms.ValidationError("Tidak dapat menambahkan minggu lebih dari minggu UAS, UAS berada di minggu ke-{}. \nHapus UAS terlebih dahulu jika ingin mengatur ulang letak minggu UAS.".format(mg_uas))
        if "UAS" in str(curr_pilih):
            if curr_minggu_ke < list_minggu[-1]:
                raise forms.ValidationError("UAS harus berada di akhir minggu")

    class Meta():
        model = BahanKajian
        fields = ['minggu_ke','pilih_minggu']
        

class BahanKajianForm (forms.ModelForm):    
    def __init__(self, *args, **kwargs): 
        id_rps = kwargs.pop('rps', None)
        id_bahan = kwargs.pop('bahan', None)
        super(BahanKajianForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.attrs = {'enctype':"multipart/form-data",'id': "userEditForm"}
        self.helper.layout = Layout(
            TabHolder(
                Tab('Kemampuan Akhir',
                    'kemampuan_akhir',
                    'referensi',
                ),
                Tab('Bahan Kajian',
                    'bahan_kajian',
                ),
                Tab('Metode pembelajaran',
                    'metode_pembelajaran',
                ),
                Tab('Waktu',
                    'waktu_tm',
                    'waktu_kt',
                    'waktu_km',
                ),
                Tab('Pengalaman belajar',
                    'pengalaman_belajar',
                ),
                Tab('Penilaian',
                    'kriteria_indikator',
                    'bobot',
                ),
                css_id='tabs',
            ),
             FormActions(
                Submit('submit', 'Simpan'),
                Button('cancel', 'Batal')
            ),
        )
        self.fields['bobot'] = forms.IntegerField(validators=[MinValueValidator(0), MaxValueValidator(max_bobot(id_rps,id_bahan)),], label='Bobot (%)')

        

    class Meta():
        model = BahanKajian
        fields = ['kemampuan_akhir','bahan_kajian','metode_pembelajaran','waktu_tm','waktu_kt','waktu_km','pengalaman_belajar','kriteria_indikator','bobot','referensi']
        # exclude = ['rps', 'minggu_ke','is_uts','is_uas']
        widgets = {
            'waktu': forms.HiddenInput(),
            'metode_pembelajaran': forms.CheckboxSelectMultiple(),
            'pengalaman_belajar': forms.CheckboxSelectMultiple(),

        }
        labels = {
            'kemampuan_akhir': 'Kemampuan Akhir Tiap Tahapan Pembelajaran',
            'bahan_kajian': 'Bahan Kajian/ Pokok Bahasan',
            'metode_pembelajaran': 'Metode Pembelajaran',
            'waktu_tm': 'Waktu (menit) TM',
            'waktu_kt': 'Waktu (menit) KT',
            'waktu_km': 'Waktu (menit) KM',
            'pengalaman_belajar': 'Pengalaman Belajar Mahasiswa',
            'kriteria_indikator': 'Kriteria & Indikator',
            'bobot': 'Bobot (%)',
        
        }
        
    def clean(self):
        cd = super().clean()
        curr_status = cd.get('bobot')
        if curr_status == "":
            raise forms.ValidationError('Bobot tidak boleh kosong')
        curr_ref = cd.get('bahan_kajian')
        if "<p>" in str(curr_ref):
            raise forms.ValidationError("Bahan kajian harus menggunakan bullet 1-2 level!")


def matkul_choice():
    data=[]
    matkuls = Matkul.objects.all()
    for matkul in matkuls:
        mk = []
        id_matkul = matkul.id
        nama = matkul.nama_matkul
        mk.append(id_matkul)
        mk.append(nama)
        data.append(mk)
    return data

class CreateTimForm(forms.ModelForm):
    class Meta():
        model = Tim
        fields = ['rps','namaTim']
    
    def __init__(self, *args, **kwargs): 
        # id_tim = kwargs.pop('tim', None)
        super(CreateTimForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.attrs = {'enctype':"multipart/form-data",}
        self.helper.layout = Layout(
                Row(
                    Div('namaTim',
                    css_class="form-group col-md-6 mb-0"),
                Div(
                    Div(
                        Div(
                            'rps',
                            css_class='card-body'
                        ),
                        css_class="card scrollbar scrollbar-primary",
                    ),
                    css_class="form-group col-md-6 mb-0"),
                ),
                FormActions(
                    Submit('submit', 'Simpan'),
                    Button('cancel', 'Batal')
                ),
            )
            
        self.fields['rps'] = forms.ModelMultipleChoiceField(
            queryset=no_tim_rps(),
            widget=forms.CheckboxSelectMultiple
        )
        self.fields['namaTim'] = forms.CharField()

    def clean(self):
        cd = super().clean()
        nama = cd.get('namaTim')
        qs = Tim.objects.filter(namaTim=nama)
        if len(qs) > 0:
            raise forms.ValidationError('Nama tim sudah ada')
        
class UpdateTimForm(forms.ModelForm):
    class Meta():
        model = Tim
        fields = ['rps', 'namaTim']

    def __init__(self, *args, **kwargs): 
        id_tim = kwargs.pop('id', None)
        super(UpdateTimForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.attrs = {'enctype':"multipart/form-data",}
        self.helper.layout = Layout(
            Row(
                Div('namaTim',
                    css_class="form-group col-md-6 mb-0"),
                Div(
                    Div(
                        Div(
                            'rps',
                            css_class='card-body'
                        ),
                        css_class="card scrollbar scrollbar-primary",
                    ),
                    css_class="form-group col-md-6 mb-0"),
                ),
                FormActions(
                    Submit('submit', 'Simpan'),
                    Button('cancel', 'Batal')
                ),
            )
        self.fields['rps'] = forms.ModelMultipleChoiceField(
            queryset=no_tim_rps(exclude=id_tim),
            widget=forms.CheckboxSelectMultiple
        )

    def clean(self):
        cd = super().clean()
        nama = cd.get('namaTim')
        qs = Tim.objects.filter(namaTim=nama)
        qs = qs.exclude(pk=self.instance.pk)
        if len(qs) > 0:
            raise forms.ValidationError('Nama tim sudah ada')

class CreateAnggotaForm(forms.ModelForm):
    class Meta():
        model = Anggota
        fields = ['dosen',]

    def __init__(self, *args, **kwargs): 
        # id_tim = kwargs.pop('tim', None)
        super(CreateAnggotaForm, self).__init__(*args, **kwargs)
        self.fields['dosen'] = forms.ModelChoiceField(queryset=no_tim_dosen())


class MetodeForm(forms.ModelForm):
    class Meta():
        model = MetodePembelajaran
        exclude = ()
        widgets = {}


class PengalamanForm(forms.ModelForm):
    class Meta():
        model = PengalamanBelajar
        exclude = ()
        widgets = {}
