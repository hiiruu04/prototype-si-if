from django.urls import path
from . import views as rps_views  
from .views import ListTimView, CreateTimView, UpdateTimView, CreateAnggotaView, DeleteTimView, DetailTimView, CreateCommentView, UpdateKetuaTimView, DeleteAnggotaView
from dosen.utils import createDocx


urlpatterns = [
    # path('list-rps/', rps_views.RPSListView.as_view(), name='rps_list'),
    # path('create-rps/', rps_views.RPSCreateView.as_view(), name='rps_create'),
    # path('delete-rps/', rps_views.RPSDeleteView.as_view(), name='rps_delete'),
    # path('update-rps/', rps_views.RPSUpdateView.as_view(), name='rps_update'),

    path('<pk>/download-rps/', rps_views.RPSToDoc.as_view(), name='rps_download'),    

    path('<pk>/detail-rps/', rps_views.RPSDetailView_v2.as_view(), name='rps_detail'),    
    path('<pk>/arsip_loading_page/', rps_views.Loading_Arsip.as_view(), name='arsip_loading_page'),    
    path('<pk>/create-comment-rps/', rps_views.CreateCommentView.as_view(), name='rps_comment'),    

    path('<pk>/edit-rps/', rps_views.RPSEditView.as_view(), name='rps_edit'),
    path('<pk>/create-bahan/', rps_views.BahanKajianCreateView.as_view(), name='bahan_create'),
    path('<pk>/<pk_mg>/update-bahan/', rps_views.BahanKajianUpdateView.as_view(), name='bahan_update'),
    path('<pk>/<pk_mg>/delete-bahan/', rps_views.BahanKajianDeleteView.as_view(), name='bahan_delete'),

    path("list-metode/", rps_views.MetodeListView.as_view(), name="metode_list"),
    path("create-metode/", rps_views.MetodeCreateView.as_view(), name="metode_create"),
    path("<pk>/update-metode/", rps_views.MetodeUpdateView.as_view(), name="metode_update"),
    path("<pk>/delete-metode/", rps_views.MetodeDeleteView.as_view(), name="metode_delete"),
    path("list-pengalaman/", rps_views.PengalamanListView.as_view(), name="pengalaman_list"),
    path("create-pengalaman/", rps_views.PengalamanCreateView.as_view(), name="pengalaman_create"),
    path("<pk>/update-pengalaman/", rps_views.PengalamanUpdateView.as_view(), name="pengalaman_update"),
    path("<pk>/delete-pengalaman/", rps_views.PengalamanDeleteView.as_view(), name="pengalaman_delete"),
    path("list-arsip/", rps_views.ArsipListView.as_view(), name="arsip_list"),
    path("<pk>/download-arsip/", rps_views.ArsipDownloadView.as_view(), name="arsip_download"),

    path("list-tim/", ListTimView.as_view(), name="tim_list"),
    path("create-tim/", CreateTimView.as_view(), name="tim_create"),
    path("detail/<pk>", DetailTimView.as_view(), name="tim_detail"),
    path("update/<pk>", UpdateTimView.as_view(), name="tim_update"),
    path("<tim>/add-anggota/", CreateAnggotaView.as_view(), name="tim_add"),
    path("<tim>/del-anggota/<pk>", DeleteAnggotaView.as_view(), name="tim_del"),
    path("<tim>/change-ketua/", UpdateKetuaTimView.as_view(), name="tim_change_ketua"),
    path("delete/<pk>/", DeleteTimView.as_view(), name="tim_delete"),
]