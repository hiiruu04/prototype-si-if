from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import Group
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.shortcuts import render, redirect, get_object_or_404, get_list_or_404
from django.views.generic import DetailView, CreateView, DeleteView, ListView, UpdateView, TemplateView, View
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic.list import ListView
from django.urls import reverse, reverse_lazy
from django.conf import settings
from django.db.models import F
from users.models import User
from matkul.models import Kurikulum, Matkul, KontrakPembelajaran, Pengampu, CPMK
from dosen.models import Dosen
from .models import RPS, MetodePembelajaran, PengalamanBelajar, BahanKajian, Tim, CommentRPS, Anggota, ArsipRPS
from .forms import PlotRPSform, TolakForm, RPSForm, BahanKajianForm, PilihBahanForm, CreateTimForm, CreateAnggotaForm, UpdateTimForm,  MetodeForm, PengalamanForm
from dosen.views import OwnerMixin, OwnerEditMixin, PerfGroup
from dosen.utils import xstr, xint, createDocx, loadDocx, docx_replace_regex, docx_replace_regex_list, regex, doc_row, minggu_pra_bahan, ref_replace_regex, downloadDocx, loadDocx16, docx_replace_16, docx_replace_16_list
from getter.get import get_data_matkul, get_data_dosen, get_data_anggota, get_anggota, get_data_rps, get_data_tim, get_tim, get_arsip
import datetime
import time
from bs4 import BeautifulSoup


class LoginRequiredMixin(LoginRequiredMixin):
    login_url = "login"

# ------------------------------- #
#   Fungsi Untuk Kelas RPSToDoc   #
# ------------------------------- #

def replace_referensi(document, rps, bahan):
    pass

def len_topik(bahan):
    list_topik = bahan.list_bahan_parse()
    if len(list_topik) is 0: return 0
    if len(list_topik) is 1: return 11
    if len(list_topik) is 2: return 22
    if len(list_topik) is 3: return 33
    if len(list_topik) is 4: return 44
    if len(list_topik) is 5: return 55

def replace_topik(bahan):
    t_len = len_topik(bahan)
    list_topik = bahan.list_bahan_parse()
    list_topik_list = []
    b = 0
    sub = 0
    for i in range(0,t_len):
        if i in range(0,55,11):
            list_topik_list.append(list_topik[b]['bahan'])
            b += 1
            sub = 0
        else:
            if sub in range(0,len(list_topik[b-1]['sub-bahan'])):
                list_topik_list.append(list_topik[b-1]['sub-bahan'][sub])
                sub += 1
            else:
                list_topik_list.append("_NOTHING_")
    return list_topik_list
                

def get_metode_from_bahan(bahan):
    metode = ""
    metode_pembelajaran = MetodePembelajaran.objects.filter(bahankajian=bahan)
    firstloop = True
    for m in metode_pembelajaran:
        if firstloop:
                metode = m.metode
                firstloop = False
        else:
            metode += " dan " 
            metode += m.metode
    return metode

def get_pengalaman_from_bahan(bahan):
    pengalaman = PengalamanBelajar.objects.filter(bahankajian=bahan)
    list_pengalaman = [p.pengalaman_belajar for p in pengalaman]
    return list_pengalaman

def get_kemampuanref(bahan):
    list_data = []
    if bahan.kemampuan_akhir:
        list_data.append(bahan.kemampuan_akhir)
        if bahan.referensi:
            list_data.append("")
            list_refbahan = bahan.list_refbahan_parse()
            for refbahan in list_refbahan:
                list_data.append(refbahan['text'])
    return list_data


def replace_bahan(document, rps, list_bahan, is_uts):
    list_pra_uts = minggu_pra_bahan(list_bahan, "pra_uts")    
    list_pra_uas = minggu_pra_bahan(list_bahan, "pra_uas")
    if is_uts is True:
        mg_uts = BahanKajian.objects.get(rps_id=rps.id, is_uts=True).minggu_ke
        k=0
        for bahan in list_bahan:
            metode = get_metode_from_bahan(bahan)
            pengalaman = get_pengalaman_from_bahan(bahan)
            if bahan.minggu_ke is list_pra_uts[0]:
                ok = docx_replace_regex(document, regex('Mg_1'), xstr(bahan.minggu_ke))
                ok = docx_replace_regex_list(document, regex('Kemampuan1'), get_kemampuanref(bahan), 6)
                ok = docx_replace_regex_list(document, regex('Bahan1'), replace_topik(bahan), 55)
                ok = docx_replace_regex(document, regex('Metode1_'), metode)
                ok = docx_replace_regex(document, regex('TM1_'), "TM : "+xstr(bahan.waktu_tm))
                ok = docx_replace_regex(document, regex('KT1_'), "KT : "+xstr(bahan.waktu_kt))
                ok = docx_replace_regex(document, regex('KM1_'), "KM : "+xstr(bahan.waktu_km))
                ok = docx_replace_regex_list(document, regex('Belajar1'), pengalaman, 5)
                ok = docx_replace_regex_list(document, regex('Kriteria1'), bahan.list_kriteria_parse(), 5)
                ok = docx_replace_regex(document, regex('Bobot1_'), xstr(bahan.bobot))
            elif bahan.minggu_ke in list_pra_uts[1:-1]:
                ok = docx_replace_regex(document, regex('Mg_2'), xstr(bahan.minggu_ke))
                ok = docx_replace_regex_list(document, regex('Kemampuan2'), get_kemampuanref(bahan), 6)
                ok = docx_replace_regex_list(document, regex('Bahan2'), replace_topik(bahan), 55)
                ok = docx_replace_regex(document, regex('Metode2_'), metode)
                ok = docx_replace_regex(document, regex('TM2_'), "TM : "+xstr(bahan.waktu_tm))
                ok = docx_replace_regex(document, regex('KT2_'), "KT : "+xstr(bahan.waktu_kt))
                ok = docx_replace_regex(document, regex('KM2_'), "KM : "+xstr(bahan.waktu_km))
                ok = docx_replace_regex_list(document, regex('Belajar2'), pengalaman, 5)
                ok = docx_replace_regex_list(document, regex('Kriteria2'), bahan.list_kriteria_parse(), 5)
                ok = docx_replace_regex(document, regex('Bobot2_'), xstr(bahan.bobot))
                k+=1
            elif bahan.minggu_ke is list_pra_uts[-1]:
                ok = docx_replace_regex(document, regex('Mg_3'), xstr(bahan.minggu_ke))
                ok = docx_replace_regex_list(document, regex('Kemampuan3'), get_kemampuanref(bahan), 6)
                ok = docx_replace_regex_list(document, regex('Bahan3'), replace_topik(bahan), 55)
                ok = docx_replace_regex(document, regex('Metode3_'), metode)
                ok = docx_replace_regex(document, regex('TM3_'), "TM : "+xstr(bahan.waktu_tm))
                ok = docx_replace_regex(document, regex('KT3_'), "KT : "+xstr(bahan.waktu_kt))
                ok = docx_replace_regex(document, regex('KM3_'), "KM : "+xstr(bahan.waktu_km))
                ok = docx_replace_regex_list(document, regex('Belajar3'), pengalaman, 5)
                ok = docx_replace_regex_list(document, regex('Kriteria3'), bahan.list_kriteria_parse(), 5)
                ok = docx_replace_regex(document, regex('Bobot3_'), xstr(bahan.bobot))
            
    mg_uas = BahanKajian.objects.get(rps_id=rps.id, is_uas=True).minggu_ke
    k=0    
    for bahan in list_bahan:
        metode = get_metode_from_bahan(bahan)        
        pengalaman = get_pengalaman_from_bahan(bahan)
        if bahan.minggu_ke is list_pra_uas[0]:
            # ok = docx_replace_regex(document, regex('Mg_1'), xstr(bahan.minggu_ke)-1)
            ok = docx_replace_regex(document, regex('Mg_1'), xstr(bahan.minggu_ke))
            ok = docx_replace_regex_list(document, regex('Kemampuan1'), get_kemampuanref(bahan), 6)
            ok = docx_replace_regex_list(document, regex('Bahan1'), replace_topik(bahan), 55)
            ok = docx_replace_regex(document, regex('Metode1_'), metode)
            ok = docx_replace_regex(document, regex('TM1_'), "TM : "+xstr(bahan.waktu_tm))
            ok = docx_replace_regex(document, regex('KT1_'), "KT : "+xstr(bahan.waktu_kt))
            ok = docx_replace_regex(document, regex('KM1_'), "KM : "+xstr(bahan.waktu_km))
            ok = docx_replace_regex_list(document, regex('Belajar1'), pengalaman, 5)
            ok = docx_replace_regex_list(document, regex('Kriteria1'), bahan.list_kriteria_parse(), 5)
            ok = docx_replace_regex(document, regex('Bobot1_'), xstr(bahan.bobot))
        elif bahan.minggu_ke in list_pra_uas[1:-1] and is_uts is True:
            # ok = docx_replace_regex(document, regex('Mg_2'), xstr(bahan.minggu_ke)-1)
            ok = docx_replace_regex(document, regex('Mg_2'), xstr(bahan.minggu_ke))
            ok = docx_replace_regex_list(document, regex('Kemampuan2'), get_kemampuanref(bahan), 6)
            ok = docx_replace_regex_list(document, regex('Bahan2'), replace_topik(bahan), 55)
            ok = docx_replace_regex(document, regex('Metode2_'), metode)
            ok = docx_replace_regex(document, regex('TM2_'), "TM : "+xstr(bahan.waktu_tm))
            ok = docx_replace_regex(document, regex('KT2_'), "KT : "+xstr(bahan.waktu_kt))
            ok = docx_replace_regex(document, regex('KM2_'), "KM : "+xstr(bahan.waktu_km))
            ok = docx_replace_regex_list(document, regex('Belajar2'), pengalaman, 5)
            ok = docx_replace_regex_list(document, regex('Kriteria2'), bahan.list_kriteria_parse(), 5)
            ok = docx_replace_regex(document, regex('Bobot2_'), xstr(bahan.bobot))
        elif bahan.minggu_ke in list_pra_uas[1:-1] and is_uts is False:
            ok = docx_replace_regex(document, regex('Mg_2'), xstr(bahan.minggu_ke))
            ok = docx_replace_regex_list(document, regex('Kemampuan2'), get_kemampuanref(bahan), 6)
            ok = docx_replace_regex_list(document, regex('Bahan2'), replace_topik(bahan), 55)
            ok = docx_replace_regex(document, regex('Metode2_'), metode)
            ok = docx_replace_regex(document, regex('TM2_'), "TM : "+xstr(bahan.waktu_tm))
            ok = docx_replace_regex(document, regex('KT2_'), "KT : "+xstr(bahan.waktu_kt))
            ok = docx_replace_regex(document, regex('KM2_'), "KM : "+xstr(bahan.waktu_km))
            ok = docx_replace_regex_list(document, regex('Belajar2'), pengalaman, 5)
            ok = docx_replace_regex_list(document, regex('Kriteria2'), bahan.list_kriteria_parse(), 5)
            ok = docx_replace_regex(document, regex('Bobot2_'), xstr(bahan.bobot))
            k+=1
        elif bahan.minggu_ke is list_pra_uas[-1]:
            # ok = docx_replace_regex(document, regex('Mg_3'), xstr(bahan.minggu_ke)-1)
            ok = docx_replace_regex(document, regex('Mg_3'), xstr(bahan.minggu_ke))
            ok = docx_replace_regex_list(document, regex('Kemampuan3'), get_kemampuanref(bahan), 6)
            ok = docx_replace_regex_list(document, regex('Bahan3'), replace_topik(bahan), 55)
            ok = docx_replace_regex(document, regex('Metode3_'), metode)
            ok = docx_replace_regex(document, regex('TM3_'), "TM : "+xstr(bahan.waktu_tm))
            ok = docx_replace_regex(document, regex('KT3_'), "KT : "+xstr(bahan.waktu_kt))
            ok = docx_replace_regex(document, regex('KM3_'), "KM : "+xstr(bahan.waktu_km))
            ok = docx_replace_regex_list(document, regex('Belajar3'), pengalaman, 5)
            ok = docx_replace_regex_list(document, regex('Kriteria3'), bahan.list_kriteria_parse(), 5)
            ok = docx_replace_regex(document, regex('Bobot3_'), xstr(bahan.bobot))

# ------------------------------- #

# Create your views here.

class CreateCommentView (CreateView,LoginRequiredMixin):
    model = CommentRPS
    template_name = "manage/rps/comment-rps.html"
    form_class = TolakForm
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        rps = RPS.objects.get(matkul_id=self.kwargs['pk'])
        context['rps'] = rps
        return context

    def form_valid(self, form):
        c = {'form':form, }
        rps_id = RPS.objects.get(matkul_id=self.kwargs['pk']).id
        rps = RPS.objects.get(id=rps_id)
        rps.status = 0
        comment = form.save(commit=False)
        comment.rps = rps
        comment.body = form.cleaned_data['body']
        groups = {g.name for g in self.request.user.groups.all()}
        if 'GPM' in groups or 'Kaprodi' in groups:
            comment.log_status = self.request.user.groups.filter(name__in=['GPM','Kaprodi'])[0].name + " telah Menolak"
        comment.save()
        rps.save()
        return super(CreateCommentView, self).form_valid(form)

    def get_success_url(self):
        return reverse_lazy('rps_detail', kwargs={'pk': self.kwargs['pk']})

def create_arsip(rps):
    start = time.time()
    list_cpmk = [cpmk.cpmk for cpmk in rps.matkul.cpmk.all()]
    list_bahan = BahanKajian.objects.filter(rps=rps).order_by('minggu_ke')
    sumofbobot = sum(xint(b.bobot) for b in list_bahan)
    pengampus = []
    if Pengampu.objects.filter(mata_kuliah=rps.matkul):
        pengampus = Pengampu.objects.filter(mata_kuliah=rps.matkul)
    pengampu_s = ""
    for p in pengampus:
        if pengampu_s is "":
            pengampu_s = str(p)
        else:
            pengampu_s = "{}; {}".format(pengampu_s, str(p))

    document = loadDocx16()

    # Replacing content        
    # -----------------------------
    ok = docx_replace_16(document.tables[0].rows[2], regex('Matkul_'), xstr(rps.matkul.nama_matkul))
    # print("Matkul",":",ok)
    ok = docx_replace_16(document.tables[0].rows[2], regex('Kode_'), xstr(rps.matkul.kd_matkul))
    # print("Kode",":",ok)
    ok = docx_replace_16(document.tables[0].rows[2], regex('Sks_'), xstr(rps.matkul.sks))
    # print("SKS",":",ok)
    ok = docx_replace_16(document.tables[0].rows[2], regex('Semester_'), xstr(rps.matkul.semester))
    # print("Semester",":",ok)
    ok = docx_replace_16(document.tables[0].rows[3], regex('Dosen_'), pengampu_s)
    # print("Dosen",":",ok)
    ok = docx_replace_16_list(4, document, regex('Capaian'), list_cpmk, 5)
    # print("Capaian",":",ok)
    ok = docx_replace_16_list(5, document, regex('Deskripsi'), rps.list_deskripsi_parse(), 5)
    # print("Deskripsi",":",ok)
    ok = ref_replace_regex(document, rps.list_referensi_parse())
    print("Referensi",":",ok)

    ok = replace_rps(document, rps)
    # print("Bahan Mingguan",":",ok)

    # is_uts = [bahan.is_uts for bahan in list_bahan]
    # if True in is_uts:
    #     replace_bahan(document, rps, list_bahan, True)            
    # else:
    #     replace_bahan(document, rps, list_bahan, False)

    ok = docx_replace_16(document.tables[0].rows[25], regex('Bobot_Total'), xstr(sumofbobot))
    # print("Bobot Total",":",ok)

    tahun = datetime.date.today().year

    createDocx(document, tahun, rps)
    # print("Waktu Total",":",time.time()-start)

def rps_validator(rps):
    data_validator = {'is_valid': False, 'invalid_list': []}
    if get_data_rps(rps)['deskripsi'] is "-":
        data_validator['invalid_list'].append('Deskripsi Singkat Mata Kuliah belum diisi')
    if get_data_rps(rps)['referensi'] is "-":
        data_validator['invalid_list'].append('Referensi belum diisi')
    mg_uts = [bahan['minggu'] for bahan in get_data_rps(rps)['kajian'] if bahan['is_uts'] is True]
    mg_uas = [bahan['minggu'] for bahan in get_data_rps(rps)['kajian'] if bahan['is_uas'] is True]
    if not mg_uas:
        data_validator['invalid_list'].append('Belum ada Ujian Akhir Semester')
    if get_data_rps(rps)['sisa'] > 8 and not mg_uts and mg_uas:
        data_validator['invalid_list'].append('Jika tidak ada UTS, jumlah minggu sebelum UAS tidak boleh kurang dari 7')
    elif get_data_rps(rps)['sisa'] > 9 and mg_uts and mg_uas:
        if mg_uts and mg_uas and  get_data_rps(rps)['sisa'] is 14:
            data_validator['invalid_list'].append('Bahan Kajian mingguan belum ada, silahkan diisi!')        
        elif mg_uts[0] < 5 or (mg_uas[0]-mg_uts[0]) < 5:
            data_validator['invalid_list'].append('Jika ada UTS, jumlah minggu sebelum UTS ataupun UAS tidak boleh kurang dari 4')        
    if get_data_rps(rps)['bobot'] is not 100:
        data_validator['invalid_list'].append('Jumlah bobot harus 100')
    urut_mg = [bahan['minggu'] for bahan in get_data_rps(rps)['kajian']]
    #TAMBAHAN FIX 16
    if get_data_rps(rps)['sisa'] > 0:
        data_validator['invalid_list'].append('Bahan Kajian Mingguan harus lengkap, sejumlah 7 pertemuan sebelum UTS dan 7 pertemuan setelah UTS')
    #####
    for i in range(0,len(urut_mg)-1):
        if urut_mg[i+1] - urut_mg[i] is not 1:
            data_validator['invalid_list'].append('Minggu harus urut')
            break
    konten_mg = [bahan['kajian'] for bahan in get_data_rps(rps)['kajian']] # asumsi kajian tidak boleh kosong
    # if "-" in konten_mg:
    #     data_validator['invalid_list'].append('Masih terdapat konten yang kosong (None) pada minggu tertentu')
    if len(data_validator['invalid_list']) is 0:
        data_validator['is_valid'] = True
        return data_validator 
    return data_validator 


class RPSToDoc (DetailView, LoginRequiredMixin):
    model = RPS
    context_object_name = 'rps'

    def get_object(self):
        try:
            rps = RPS.objects.get(matkul_id=self.kwargs['pk']) # berdasarkan id RPS, saat ganti one2one, diganti matkul.id
        except:
            raise Http404("No rps found matching the query")
        return rps

    def get(self, request, *args, **kwargs):
        rps = self.get_object()
        # list_cpmk = [cpmk.cpmk for cpmk in rps.matkul.cpmk.all()]
        # list_bahan = BahanKajian.objects.filter(rps=rps).order_by('minggu_ke')
        # sumofbobot = sum(xint(b.bobot) for b in list_bahan)
        # pengampus = []
        # if Pengampu.objects.filter(mata_kuliah=rps.matkul):
        #     pengampus = Pengampu.objects.filter(mata_kuliah=rps.matkul)
        # pengampu_s = ""
        # for p in pengampus:
        #     if pengampu_s is "":
        #         pengampu_s = str(p.dosen_pengampu)
        #     else:
        #         pengampu_s = "{}, {}".format(pengampu_s, str(p.dosen_pengampu))
        # groups_name = {g.name for g in self.request.user.groups.all()}

        # document = loadDocx(list_bahan)

        # # Replacing content        
        # # -----------------------------
        # ok = docx_replace_regex(document, regex('Matkul_'), xstr(rps.matkul.nama_matkul))
        # ok = docx_replace_regex(document, regex('Kode_'), xstr(rps.matkul.kd_matkul))
        # ok = docx_replace_regex(document, regex('Sks_'), xstr(rps.matkul.sks))
        # ok = docx_replace_regex(document, regex('Semester_'), xstr(rps.matkul.semester))
        # ok = docx_replace_regex(document, regex('Dosen_'), pengampu_s)
        # ok = docx_replace_regex_list(document, regex('Capaian'), list_cpmk, 5)
        # ok = docx_replace_regex_list(document, regex('Deskripsi'), rps.list_deskripsi_parse(), 5)
        # ok = ref_replace_regex(document, rps.list_referensi_parse())

        # is_uts = [bahan.is_uts for bahan in list_bahan]
        # if True in is_uts:
        #     replace_bahan(document, rps, list_bahan, True)            
        # else:
        #     replace_bahan(document, rps, list_bahan, False)

        # ok = docx_replace_regex(document, regex('Bobot_Total'), xstr(sumofbobot))


        # if Anggota.objects.filter(tim__rps=rps, dosen__user=self.request.user).exists():
        #     anggota = Anggota.objects.get(tim__rps=rps, dosen__user=self.request.user)
        
        tahun_rps = rps.matkul.kurikulum.tahun
        if rps.matkul.kurikulum.tahun < datetime.date.today().year:
            tahun_rps = datetime.date.today().year
        file_name = rps.matkul.nama_matkul
        
        # rps_response = createDocx(document, file_name)
        rps_response = downloadDocx(tahun_rps, file_name)
        if rps_response:
            return rps_response
        return HttpResponse("Not found")

class RPSDetailView (DetailView, View, LoginRequiredMixin):
    """
        Class yang digunakan untuk menampilkan detail rps oleh tim dosen
    """
    model = RPS
    template_name = "manage/detail-rps.html"
    # context_object_name = 'rps'
    # success_url = "rps_detail"

    def get_object(self):
        try:
            rps = RPS.objects.get(matkul_id=self.kwargs['pk']) # berdasarkan id RPS, saat ganti one2one, diganti matkul.id
        except:
            raise Http404("No rps found matching the query")
        return rps

    def post(self, request, pk):
        rps = self.get_object()
        status = request.POST.get('change_status')
        rps.status = int(status)
        if rps.status is 3:
            create_arsip(rps)
            print("TERBUAT")
            matkul = rps.matkul
            tahun = datetime.date.today().year
            if not ArsipRPS.objects.filter(matkul=matkul,tahun=tahun).exists():
                ArsipRPS.objects.create(
                    matkul = matkul,
                    tahun = tahun,
                    fileArsip = 'arsip/{}/{}.docx'.format(str(tahun), str(matkul)),
                )
            else:
                arsip = ArsipRPS.objects.get(matkul=matkul, tahun=tahun)
                arsip.matkul = matkul
                arsip.tahun = tahun
                arsip.fileArsip = 'arsip/{}/{}.docx'.format(str(tahun), str(matkul))
        rps.save()
        return redirect ('rps_detail', pk=rps.matkul.id)
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        rps = self.get_object()
        data = get_data_rps(rps)
        context['rps'] = data
        # list_bahan = []
        # cpmks = []
        # try:
        #     list_bahan = BahanKajian.objects.filter(rps=rps).order_by('minggu_ke')
        #     cpmks = [cpmk for cpmk in Matkul.objects.get(id=rps.matkul_id).cpmk.all()]
        # except IndexError:
        #     raise Http404("No bahan kajian found matching the query")
        # context['list_bahan'] = list_bahan
        # sumofbobot = sum(xint(b.bobot) for b in list_bahan)
        # context['jumlah_bobot'] = sumofbobot
        # context['cpmk'] = cpmks
        # max_mg = 16
        # context['sisa_bahan'] = max_mg - len(list_bahan)
        # # pengampus = get_list_or_404(Pengampu, mata_kuliah=rps.matkul)
        pengampus = []
        if Pengampu.objects.filter(mata_kuliah=rps.matkul):
            pengampus = Pengampu.objects.filter(mata_kuliah=rps.matkul)
        pengampu_s = ""
        for p in pengampus:
            if pengampu_s is "":
                pengampu_s = str(p)
            else:
                pengampu_s = "{}; {}".format(pengampu_s, str(p))
        context['pengampu'] = pengampu_s  
        groups_name = {g.name for g in self.request.user.groups.all()}
        context['groups'] = groups_name  
        anggota = None
        if Anggota.objects.filter(tim__rps=rps, dosen__user=self.request.user).exists():
            anggota = Anggota.objects.get(tim__rps=rps, dosen__user=self.request.user)
        context['anggota'] = anggota
        context['is_rps_valid'] = rps_validator(rps)['is_valid']
        context['invalid_list'] = rps_validator(rps)['invalid_list']
        return context
    
class RPSEditView (UpdateView, LoginRequiredMixin):
    """
        Class yang digunakan untuk mengedit rps oleh tim dosen
    """
    model = RPS
    form_class = RPSForm
    template_name = "manage/rps/form-edit-rps.html"
    # success_url = reverse_lazy('rps_list')
    # permission_required = "rps.change_rps"

    def get_success_url(self):
        return reverse_lazy('rps_detail', kwargs={'pk': self.kwargs['pk']})

    def get_object(self):
        try:
            rps = RPS.objects.get(matkul_id=self.kwargs['pk']) # berdasarkan id RPS, saat ganti one2one, diganti matkul.id
        except:
            raise Http404("No rps found matching the query")
        return rps

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        rps = self.get_object()
        list_bahan = BahanKajian.objects.filter(rps_id=rps.id).order_by('minggu_ke')
        context['list_bahan'] = list_bahan
        sumofbobot = sum(xint(b.bobot) for b in list_bahan)
        context['jumlah_bobot'] = sumofbobot
        max_mg = 16
        context['sisa_bahan'] = max_mg - list_bahan.count()
        return context
    
class BahanKajianCreateView (CreateView, LoginRequiredMixin):
    """
        Class yang digunakan untuk membuat bahan kajian pada rps oleh tim dosen
    """
    model = BahanKajian
    form_class = PilihBahanForm
    template_name = "manage/rps/form-create-bahan-kajian.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        rps = RPS.objects.get(matkul_id=self.kwargs['pk'])
        context["rps"] = rps
        return context

    def get_form_kwargs(self):
        kwargs = super(BahanKajianCreateView, self).get_form_kwargs()
        rps_id = RPS.objects.get(matkul_id=self.kwargs['pk']).id
        # kwargs['rps'] = self.kwargs['pk'] # memasukkan rps_id ke kwargs
        kwargs['rps'] = rps_id
        return kwargs

    def form_valid(self, form):
        c = {'form': form, }
        bahan = form.save(commit=False)
        pilihan = form.cleaned_data['pilih_minggu']
        rps_id = RPS.objects.get(matkul_id=self.kwargs['pk']).id
        bahan.rps = RPS.objects.get(id=rps_id)
        if pilihan == "UTS":
            bahan.is_uts = True
        elif pilihan == "UAS":
            bahan.is_uts = False
            bahan.is_uas = True
        else:
            bahan.is_uts = False
            bahan.is_uas = False
        bahan.save()
        if bahan.is_uts or bahan.is_uas:
            self.success_url = reverse_lazy('rps_edit', kwargs={'pk': bahan.rps.matkul.id})+'#'+str(bahan.minggu_ke)
        else:
            self.success_url = reverse_lazy('bahan_update', kwargs={'pk': bahan.rps.matkul.id, 'pk_mg': str(bahan.minggu_ke)}) # setelah one2one, ganti matkul id
        return super(BahanKajianCreateView, self).form_valid(form)

class BahanKajianUpdateView (UpdateView, LoginRequiredMixin):
    """
        Class yang digunakan untuk mengupdate bahan kajian pada rps oleh tim dosen
    """
    model = BahanKajian
    form_class = BahanKajianForm
    template_name = "manage/rps/form-update-bahan-kajian.html"
    # success_url = reverse_lazy('rps_edit')
    # success_url = reverse_lazy('rps_list')
    # permission_required = "rps.change_rps"
    
    def get_success_url(self):
        # rps_id = self.get_object().rps.id
        matkul_id = self.get_object().rps.matkul.id
        minggu_ke = self.get_object().minggu_ke
        return reverse_lazy('rps_edit', kwargs={'pk': matkul_id})+'#'+str(minggu_ke)

    def get_object(self):
        try:
            bahan = BahanKajian.objects.get(rps__matkul_id=self.kwargs['pk'], minggu_ke=self.kwargs['pk_mg']) # berdasarkan id RPS, saat ganti one2one, diganti matkul.id
        except:
            raise Http404("No bahan kajian found matching the query")
        return bahan

    def get_form_kwargs(self):
        kwargs = super(BahanKajianUpdateView, self).get_form_kwargs()
        rps_id = RPS.objects.get(matkul_id=self.kwargs['pk']).id
        kwargs['rps'] = rps_id # memasukkan rps_id ke kwargs
        kwargs['bahan'] = self.get_object().id # memasukkan bahan_id ke kwargs
        return kwargs

class BahanKajianDeleteView (DeleteView, LoginRequiredMixin):
    """
        Class yang digunakan untuk menghapus bahan kajian pada rps oleh tim dosen
    """
    model = BahanKajian
    template_name = "manage/rps/form-delete-bahan-kajian.html"
    # success_url = reverse_lazy('rps_edit')
    # success_url = reverse_lazy('rps_list')
    # permission_required = "rps.change_rps"
    
    def get_success_url(self):
        # rps_id = self.get_object().rps.id
        matkul_id = self.get_object().rps.matkul.id
        return reverse_lazy('rps_edit', kwargs={'pk': matkul_id})+'#tabel-bahan'

    def get_object(self):
        try:
            bahan = BahanKajian.objects.get(rps__matkul_id=self.kwargs['pk'], minggu_ke=self.kwargs['pk_mg']) # berdasarkan id RPS, saat ganti one2one, diganti matkul.id
        except:
            raise Http404("No bahan kajian found matching the query")
        return bahan

# TIM
class DetailTimView(TemplateView, LoginRequiredMixin):
    # model = Tim
    template_name = "manage/rps/tim/tim-detail.html"
    ubah_button = False

    def post(self, request, pk):
        id_tim = self.kwargs['pk']
        # curr_tim = get_object_or_404(Tim, id=id_tim)
        # tims = get_data_tim(curr_tim)
        # tims.ketua.is_ketua = False
        id_anggota = request.POST.get('ketua')
        anggota = Anggota.objects.get(id=id_anggota)
        anggota.is_ketua = True
        anggota.save() 
        return redirect ('tim_detail', pk=id_tim)

    def post2(self, request, pk):
        context = super().get_context_data(**kwargs)
        tim_id = self.kwargs['pk']
        ubah_button = request.POST.get('ubah_button')
        context["ubah_button"] = ubah_button
        return redirect ('tim_detail', pk=tim_id)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if 'pk' in self.kwargs:
            id_tim = self.kwargs['pk']
            curr_tim = get_object_or_404(Tim, id=id_tim)
            tims = get_data_tim(curr_tim)
            context["tim"] = tims
        return context

class ListTimView(ListView, LoginRequiredMixin):
    model = Tim
    template_name = 'manage/rps/tim/tim-list.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["tims"] = get_tim()
        return context
    
# Ketua
class CreateTimView(CreateView, LoginRequiredMixin):
    model = Tim
    form_class = CreateTimForm
    template_name = 'manage/rps/tim/tim-create.html'

    def get_success_url(self):
        tim_id = self.object.id
        return reverse('tim_add', kwargs={'tim':tim_id})

class UpdateTimView(UpdateView, LoginRequiredMixin):
    model = Tim
    form_class = UpdateTimForm
    template_name = 'manage/rps/tim/tim-update.html'
    success_url = reverse_lazy('tim_list')

    def get_form_kwargs(self):
        kwargs = super(UpdateTimView, self).get_form_kwargs()
        kwargs['id'] = self.kwargs['pk'] # memasukkan rps_id ke kwargs
        return kwargs

class DeleteTimView(DeleteView, LoginRequiredMixin):
    model = Tim
    template_name = 'manage/rps/tim/tim-delete.html'
    success_url = reverse_lazy('tim_detail')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if 'pk' in self.kwargs:
            id_tim = self.kwargs['pk']
            tim = get_object_or_404(Tim, id=id_tim)
            data = get_data_tim(tim)
            context['tim'] = data
        return context
    
    def get_success_url(self, **kwargs):
        return reverse_lazy('tim_list')

class CreateAnggotaView(CreateView, LoginRequiredMixin):
    model = Tim
    form_class = CreateAnggotaForm
    template_name = 'manage/rps/tim/tim-create-anggota.html'
    success_url = reverse_lazy('tim_detail')

    def dispatch(self, request, *args, **kwargs):
        # curr_tim = Tim.objects.get(id=int(self.kwargs['tim']))
        self.tim = None
        if 'tim' in self.kwargs:
            id_tim = self.kwargs['tim']
            curr_tim = Tim.objects.get(id=id_tim)
            self.tim = curr_tim
        return super().dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        c = {'form':form, }
        tims = form.save(commit=False)
        tims.tim = self.tim
        tims.save()
        return super(CreateAnggotaView, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["tim"] = self.tim
        return context

    def get_success_url(self):
        return reverse_lazy('tim_detail', kwargs={'pk': self.kwargs['tim']})

class UpdateKetuaTimView(TemplateView, LoginRequiredMixin):
    template_name = 'manage/rps/tim/tim-update-ketua.html'
    success_url = reverse_lazy('tim_detail')

    def dispatch(self, request, *args, **kwargs):
        # curr_tim = Tim.objects.get(id=int(self.kwargs['tim']))
        id_tim = self.kwargs['tim']
        curr_tim = Tim.objects.get(id=id_tim)
        ketua = Anggota.objects.filter(tim=curr_tim, is_ketua=True)
        anggota = get_anggota(curr_tim)
        curr_ketua = "-"
        if len(ketua) > 0 :
            curr_ketua = ketua
        self.tim = curr_tim
        self.ketua = curr_ketua[0]
        self.anggota = anggota
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["ketua"] = self.ketua
        context["tim"] = self.tim
        context["anggotas"] = self.anggota
        return context

    def post(self, request, tim):
        tim_id = self.kwargs['tim']
        id_anggota = request.POST.get('ketua')
        anggota = Anggota.objects.get(id=id_anggota)
        anggota.is_ketua = True
        anggota.save()
        ketua = self.ketua
        ketua.is_ketua = False
        ketua.save()
        return redirect ('tim_detail', pk=tim_id)

    def get_success_url(self):
        return reverse_lazy('tim_detail', kwargs={'tim': self.kwargs['tim']})

class DeleteAnggotaView(DeleteView, LoginRequiredMixin):
    model = Anggota
    template_name = 'manage/rps/tim/tim-delete-anggota.html'
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if 'pk' in self.kwargs:
            id = self.kwargs['pk']
            anggota = get_object_or_404(Anggota, id=id)
            data = get_data_anggota(anggota)
            context['anggota'] = data
        return context
    
    def get_success_url(self, **kwargs):
        if 'tim' in self.kwargs:
            id = self.kwargs['tim']
            return reverse_lazy('tim_detail', kwargs={'pk':id})
        else:
            return reverse_lazy('tim_list')


class MetodeListView(ListView, LoginRequiredMixin):
    """
        Class yang digunakan untuk menampilkan daftar metode pembelajaran
    """
    model = MetodePembelajaran
    template_name = "manage/rps/metode/list-metode.html"

class MetodeCreateView(CreateView, LoginRequiredMixin):
    """
        Class yang digunakan untuk membuat metode pembelajaran baru
    """
    form_class = MetodeForm
    template_name = "manage/rps/metode/form-create-metode.html"
    success_url = reverse_lazy('metode_list')
    # permission_required = "rps.add_metodepembelajaran"

class MetodeUpdateView(UpdateView, LoginRequiredMixin):
    """
        Class yang digunakan untuk mengedit metode pembelajaran yang sudah ada
    """
    model = MetodePembelajaran
    form_class = MetodeForm
    template_name = "manage/rps/metode/form-edit-metode.html"
    success_url = reverse_lazy('metode_list')
    # permission_required = "rps.change_metodepembelajaran"

class MetodeDeleteView(DeleteView, LoginRequiredMixin):
    """
        Class yang digunakan untuk menghapus metode pembelajaran yang ada
    """
    model = MetodePembelajaran
    template_name = "manage/rps/metode/delete-metode.html"
    success_url = reverse_lazy('metode_list')
    # permission_required = "rps.delete_metodepembelajaran"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        metode = self.get_object()
        context['metode'] = metode.__str__()
        return context


class PengalamanListView(ListView, LoginRequiredMixin):
    """
        Class yang digunakan untuk menampilkan daftar pengalamana belajar mahasiswa
    """
    model = PengalamanBelajar
    template_name = "manage/rps/pengalaman/list-pengalaman.html"

class PengalamanCreateView(CreateView, LoginRequiredMixin):
    """
        Class yang digunakan untuk membuat pengalamana belajar mahasiswa baru
    """
    form_class = PengalamanForm
    template_name = "manage/rps/pengalaman/form-create-pengalaman.html"
    success_url = reverse_lazy('pengalaman_list')
    # permission_required = "rps.add_pengalamanbelajar"

class PengalamanUpdateView(UpdateView, LoginRequiredMixin):
    """
        Class yang digunakan untuk mengedit pengalamana belajar mahasiswa yang sudah ada
    """
    model = PengalamanBelajar
    form_class = PengalamanForm
    template_name = "manage/rps/pengalaman/form-edit-pengalaman.html"
    success_url = reverse_lazy('pengalaman_list')
    # permission_required = "rps.change_pengalamanabelajar"

class PengalamanDeleteView(DeleteView, LoginRequiredMixin):
    """
        Class yang digunakan untuk menghapus pengalamana belajar mahasiswa yang ada
    """
    model = PengalamanBelajar
    template_name = "manage/rps/pengalaman/delete-pengalaman.html"
    success_url = reverse_lazy('pengalaman_list')
    # permission_required = "rps.delete_mpengalamanabelajar"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        pengalaman = self.get_object()
        context['pengalaman'] = pengalaman.__str__()
        return context


class ArsipListView(ListView, LoginRequiredMixin):
    """
        Class yang digunakan untuk menampilkan daftar arsip rps
    """
    model = ArsipRPS
    template_name = "manage/rps/arsip/list-arsip.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["arsips"] = get_arsip()
        return context
    
class ArsipDownloadView(DetailView, LoginRequiredMixin):
    """
        Class yang digunakan untuk mendownload arsip rps
    """
    model = ArsipRPS
    # template_name = "manage/rps/arsip/download-arsip.html"
    # success_url = reverse_lazy('arsip_list')

    def get(self, request, *args, **kwargs):
        arsip_rps = self.get_object()
        tahun_rps = arsip_rps.tahun
        file_name = arsip_rps.matkul.nama_matkul
        
        # rps_response = createDocx(document, file_name)
        rps_response = downloadDocx(tahun_rps, file_name)
        if rps_response:
            return rps_response
        return HttpResponse("Not found")



# Mencoba Detail RPS Baru + Edit RPS
class RPSDetailView_v2 (DetailView, View, LoginRequiredMixin):
    """
        Class yang digunakan untuk menampilkan detail rps oleh tim dosen
    """
    model = RPS
    template_name = "manage/detail-rps v2.html"

    def get_object(self):
        try:
            rps = RPS.objects.get(matkul_id=self.kwargs['pk']) # berdasarkan id RPS, saat ganti one2one, diganti matkul.id
        except:
            raise Http404("No rps found matching the query")
        return rps

    def post(self, request, pk):
        start = time.time()
        rps = self.get_object()
        old_status = rps.status
        status = request.POST.get('change_status')
        rps.status = int(status)
        #Log RPS
        if rps.status is 1 and old_status is 0:
            CommentRPS.objects.create(rps = rps, log_status = "Ketua Tim Menyetujui")
        elif rps.status is 2 and old_status is 1:
            CommentRPS.objects.create(rps = rps, log_status = "Ketua GPM Menyetujui")
        elif rps.status is 3 and old_status is 2:
            CommentRPS.objects.create(rps = rps, log_status = "Kaprodi Menyetujui")
            CommentRPS.objects.create(rps = rps, log_status = "RPS {} telah diarsipkan dengan versi {} terupdate".format(rps.matkul.nama_matkul,datetime.date.today().year))
        elif rps.status is 0 and old_status is 1:
            CommentRPS.objects.create(rps = rps, log_status = "Ketua Tim Membatalkan Persetujuannya")
        elif rps.status is 1 and old_status is 2:
            CommentRPS.objects.create(rps = rps, log_status = "Ketua GPM Membatalkan Persetujuannya")
        elif rps.status is 2 and old_status is 3:
            CommentRPS.objects.create(rps = rps, log_status = "Kaprodi Membatalkan Persetujuannya")
        if rps.status is 3:
            create_arsip(rps)
            print("TERBUAT")
            matkul = rps.matkul
            tahun = datetime.date.today().year
            if not ArsipRPS.objects.filter(matkul=matkul,tahun=tahun).exists():
                ArsipRPS.objects.create(
                    matkul = matkul,
                    tahun = tahun,
                    fileArsip = 'arsip/{}/{}.docx'.format(str(tahun), str(matkul)),
                )
            else:
                arsip = ArsipRPS.objects.get(matkul=matkul, tahun=tahun)
                arsip.matkul = matkul
                arsip.tahun = tahun
                arsip.fileArsip = 'arsip/{}/{}.docx'.format(str(tahun), str(matkul))
        rps.save()
        print("Waktu Total",":",time.time()-start,"second")
        return redirect ('rps_detail', pk=rps.matkul.id)
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        rps = self.get_object()
        data = get_data_rps(rps)
        context['rps'] = data
        pengampus = []
        if Pengampu.objects.filter(mata_kuliah=rps.matkul):
            pengampus = Pengampu.objects.filter(mata_kuliah=rps.matkul)
        pengampu_s = ""
        for p in pengampus:
            if pengampu_s is "":
                pengampu_s = str(p)
            else:
                pengampu_s = "{}; {}".format(pengampu_s, str(p))
        context['pengampu'] = pengampu_s  
        groups_name = {g.name for g in self.request.user.groups.all()}
        context['groups'] = groups_name  
        anggota = None
        if Anggota.objects.filter(tim__rps=rps, dosen__user=self.request.user).exists():
            anggota = Anggota.objects.get(tim__rps=rps, dosen__user=self.request.user)
        context['anggota'] = anggota
        context['is_rps_valid'] = rps_validator(rps)['is_valid']
        context['invalid_list'] = rps_validator(rps)['invalid_list']
        if rps.referensi:
            context['est_wkt'] = len(rps.referensi)/2
        # print("Panjang Karakter Field:",len(rps.referensi))
        print("Panjang Karakter Field:",0)
        return context

class Loading_Arsip (DetailView, LoginRequiredMixin):
    """
        Class yang digunakan untuk menampilkan detail rps oleh tim dosen
    """
    model = RPS
    template_name = "manage/detail-rps-loading.html"

    def get_object(self):
        try:
            rps = RPS.objects.get(matkul_id=self.kwargs['pk']) # berdasarkan id RPS, saat ganti one2one, diganti matkul.id
        except:
            raise Http404("No rps found matching the query")
        return rps
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        rps = self.get_object()
        if rps.status is 3:
            create_arsip(rps)
            print("TERBUAT")
            matkul = rps.matkul
            tahun = datetime.date.today().year
            if not ArsipRPS.objects.filter(matkul=matkul,tahun=tahun).exists():
                ArsipRPS.objects.create(
                    matkul = matkul,
                    tahun = tahun,
                    fileArsip = 'arsip/{}/{}.docx'.format(str(tahun), str(matkul)),
                )
            else:
                arsip = ArsipRPS.objects.get(matkul=matkul, tahun=tahun)
                arsip.matkul = matkul
                arsip.tahun = tahun
                arsip.fileArsip = 'arsip/{}/{}.docx'.format(str(tahun), str(matkul))
        data = get_data_rps(rps)
        context['rps'] = data
        return context


def replace_rps(document, rps):
    start = time.time()
    list_bahan = BahanKajian.objects.filter(rps=rps).order_by('minggu_ke')
    k=0    
    for bahan in list_bahan:
        if bahan.is_uts:
            k += 1
        elif bahan.is_uas:
            k += 1
        else:
            k += 1
            metode = get_metode_from_bahan(bahan)        
            pengalaman = get_pengalaman_from_bahan(bahan)
            ok = docx_replace_16(document.tables[0].rows[k+8], regex('Mg_{}_'.format(k)), xstr(bahan.minggu_ke))
            ok = docx_replace_16_list(k+8, document, regex('Kemampuan{}'.format(k)), get_kemampuanref(bahan), 6)
            ok = docx_replace_16_list(k+8, document, regex('Bahan{}'.format(k)), replace_topik(bahan), 55)
            ok = docx_replace_16(document.tables[0].rows[k+8], regex('Metode{}_'.format(k)), metode)
            ok = docx_replace_16(document.tables[0].rows[k+8], regex('TM{}_'.format(k)), "TM : "+xstr(bahan.waktu_tm))
            ok = docx_replace_16(document.tables[0].rows[k+8], regex('KT{}_'.format(k)), "KT : "+xstr(bahan.waktu_kt))
            ok = docx_replace_16(document.tables[0].rows[k+8], regex('KM{}_'.format(k)), "KM : "+xstr(bahan.waktu_km))
            ok = docx_replace_16_list(k+8, document, regex('Belajar{}'.format(k)), pengalaman, 5)
            ok = docx_replace_16_list(k+8, document, regex('Kriteria{}'.format(k)), bahan.list_kriteria_parse(), 5)
            ok = docx_replace_16(document.tables[0].rows[k+8], regex('Bobot{}_'.format(k)), xstr(bahan.bobot))
    return time.time() - start