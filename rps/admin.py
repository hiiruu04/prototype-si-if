from django.contrib import admin
from .models import RPS, BahanKajian, Tim, MetodePembelajaran, PengalamanBelajar, ArsipRPS, CommentRPS


# Register your models here.

admin.site.register(Tim)
admin.site.register(MetodePembelajaran)
admin.site.register(PengalamanBelajar)
admin.site.register(ArsipRPS)
admin.site.register(CommentRPS)

class BahanKajianInline(admin.TabularInline):
    model = BahanKajian

@admin.register(RPS)
class RPSAdmin(admin.ModelAdmin):
    inlines = [BahanKajianInline,]