from django.db import models
from matkul.models import Kurikulum, Matkul, KontrakPembelajaran, Pengampu
from dosen.models import Dosen 
from ckeditor.fields import RichTextField
from dosen.utils import xstr, current_year, rename_and_path
from dosen.storage import OverwriteStorage
from bs4 import BeautifulSoup
import re

import datetime

def year_choices():
    years = ()
    i = 1
    if datetime.date.today().month>6:
        i = 2
    kurikulum_sebelumnya = Kurikulum.objects.all().order_by('-tahun')[0]
    least = kurikulum_sebelumnya.tahun - 5
    for y in range(least, datetime.date.today().year+i):
        years += ((y,str(y)),)
    return years

def file_path(instance, filename):
    return "arsip/{}/{}.doc".format(instance.tahun, instance.matkul.nama_matkul)

class ArsipRPS (models.Model):
    matkul = models.ForeignKey(Matkul, on_delete=models.SET_NULL, blank=True, null=True)
    tahun = models.IntegerField(choices=year_choices(), default=current_year())
    fileArsip = models.FileField(max_length=100, storage=OverwriteStorage(), upload_to=file_path)
    
    def __str__(self):
        return "{}-{}".format(str(self.tahun), str(self.matkul))

class RPS (models.Model):
    matkul = models.OneToOneField(Matkul, on_delete=models.CASCADE)
    # tahun = models.IntegerField(choices=year_choices(), default=current_year())
    # capaian_pembelajaran = RichTextField(config_name='numbering_ckeditor', null=True, blank=True)  
    deskripsi_matkul = RichTextField(config_name='nul_ckeditor', null=True, blank=True)
    referensi = RichTextField(config_name='refference_ckeditor', null=True, blank=True) # (format, ul, BIU, undoredo, link)
    status = models.PositiveIntegerField(default=0)

    def __str__(self):
        nama_rps = "RPS " + self.matkul.nama_matkul
        return nama_rps

    def list_deskripsi_parse(self):
        list_deskripsi = []
        if self.deskripsi_matkul:
            tags = BeautifulSoup(self.deskripsi_matkul, "html.parser")
            list_deskripsi = [p.get_text() for p in tags.find_all("p")]
        return list_deskripsi

    def list_referensi_parse(self):
        list_referensi = []
        if self.referensi:
            tags = BeautifulSoup(self.referensi, "html.parser")
            ref_ke = 0
            for tag in str(tags).split('\n'):
                if re.compile(r'<li>').search(tag):
                    tag = re.sub(r"</?li>", "", tag)
                    tag = re.sub(r"[\t\r\n\f\xa0]", "", tag)
                    tag = re.sub(r"\s\s+", "", tag)
                    text = re.sub(r"</?em>", "", tag) 
                    normal1 = re.sub(r"<em>([A-z0-9]*[.,\"\'\(\)]*\s*)*</em>([A-z0-9]*[.,\"\'\(\)]*\s*)*", "", tag)
                    normal2 = None
                    em = None
                    if re.compile(r'<em>').search(tag):
                        normal2 = re.sub(r"^([A-z0-9]*[.,\"\'\(\)]*\s*)*<em>([A-z0-9]*[.,\"\'\(\)]*\s*)*</em>", "", tag)
                        em = re.sub(r"^([A-z0-9]*[.,\"\'\(\)]*\s*)*<em>", "", tag)
                        em = re.sub(r"</em>([A-z0-9]*[.,\"\'\(\)]*\s*)*", "", em)
                    ref = {
                        'text': text,
                        'normal1': normal1,
                        'em': em,
                        'normal2': normal2,
                    }
                    list_referensi.append(ref)
                    ref_ke += 1
        return list_referensi


class MetodePembelajaran (models.Model):
    metode = models.CharField(max_length=100)
    kegiatan_dosen = models.TextField(null=True, blank=True) 
    kegiatan_mahasiswa = models.TextField(null=True, blank=True) 
    ciri_umum = models.TextField(null=True, blank=True) 

    def __str__(self):
        return self.metode

class PengalamanBelajar (models.Model):
    pengalaman_belajar = models.CharField(max_length=50)
    penjelasan = models.TextField(null=True, blank=True)

    def __str__(self):
        return self.pengalaman_belajar

class BahanKajian (models.Model):
    rps = models.ForeignKey(RPS, on_delete=models.CASCADE)
    minggu_ke = models.PositiveIntegerField()
    kemampuan_akhir = models.TextField(null=True, blank=True)
    referensi = RichTextField(config_name='nul_ckeditor', null=True, blank=True) 
    bahan_kajian = RichTextField(config_name='nonnumbering+_ckeditor', null=True, blank=True)
    metode_pembelajaran = models.ManyToManyField(MetodePembelajaran, null=True, blank=True)
    waktu_tm = models.PositiveIntegerField(null=True, blank=True)
    waktu_kt = models.PositiveIntegerField(null=True, blank=True)
    waktu_km = models.PositiveIntegerField(null=True, blank=True)
    pengalaman_belajar = models.ManyToManyField(PengalamanBelajar, null=True, blank=True)
    kriteria_indikator = RichTextField(config_name='nul_ckeditor', null=True, blank=True)
    bobot = models.PositiveIntegerField(null=True, blank=True)
    is_uts = models.BooleanField(default=False)
    is_uas = models.BooleanField(default=False)

    def waktu(self):
        tm = self.waktu_tm
        kt = self.waktu_kt
        km = self.waktu_km
        return "TM: {}\nKT: {}\nKM: {}".format(xstr(tm), xstr(kt), xstr(km))
    
    def list_kriteria_parse(self):
        list_kriteria = []
        if self.kriteria_indikator:
            tags = BeautifulSoup(self.kriteria_indikator, "html.parser")
            list_kriteria = [p.get_text() for p in tags.find_all("p")]
        return list_kriteria

    def list_refbahan_parse(self):
        list_refbahan = []
        if self.referensi:
            tags = BeautifulSoup(self.referensi, "html.parser")
            refbahan_ke = 0
            for p in tags.find_all("p"):
                if re.compile(r'\[[1-9][0-9]*\]').search(p.get_text()):
                    ref = p.get_text()
                    refbahan = {}
                    refbahan['text'] = ref
                    ref = re.sub(r"\].*", "", ref)
                    ref = re.sub(r"\[", "", ref)
                    refbahan['ref'] = int(ref)
                    list_refbahan.append(refbahan)
        return list_refbahan
    
    def list_bahan_parse(self):
        list_bahan = []
        if self.bahan_kajian:
            tags = BeautifulSoup(self.bahan_kajian, "html.parser")
            level = 0
            topik_ke = 0
            for tag in str(tags).split('\n'):
                if re.compile(r'<ul>').search(tag):
                    level += 1
                if re.compile(r'</ul>').search(tag):
                    level -= 1
                if level is 1:
                    if re.compile(r'<li>').search(tag):
                        tag = re.sub(r"</?li>", "", tag)
                        tag = re.sub(r"[\t\r\n\f\xa0]", "", tag)
                        tag = re.sub(r"\s\s+", "", tag)
                        list_sub = []
                        topik = {
                            'bahan': tag,
                            'sub-bahan': list_sub,
                        }
                        list_bahan.append(topik)
                        topik_ke += 1
                if level is 2:
                    if re.compile(r'<li>').search(tag):
                        tag = re.sub(r"</?li>", "", tag)
                        tag = re.sub(r"[\t\r\n\f\xa0]", "", tag)
                        tag = re.sub(r"\s\s+", "", tag)
                        list_bahan[topik_ke-1]['sub-bahan'].append(tag)
        return list_bahan

    def __str__(self):
        nama_kajian = "minggu ke-" + str(self.minggu_ke) + " " + self.rps.matkul.nama_matkul
        return nama_kajian

    class Meta:
        unique_together = ('rps', 'minggu_ke')

class Tim (models.Model):
    rps = models.ManyToManyField(RPS, null=True, blank=True)
    namaTim = models.CharField(max_length=50, default="namaTim")

    def __str__(self):
        return self.namaTim.__str__()

class Anggota(models.Model):
    dosen = models.ForeignKey(Dosen, on_delete=models.CASCADE)
    tim = models.ForeignKey(Tim, on_delete=models.CASCADE)
    is_ketua = models.BooleanField(default=False)

    def __str__(self):
        return self.dosen.__str__()

class CommentRPS (models.Model):
    rps = models.ForeignKey(RPS, on_delete=models.CASCADE)
    created_date = models.DateTimeField(auto_now_add=True)
    body = models.TextField(null=True, blank=True)
    log_status = models.CharField(max_length=100, null=True, blank=True)
    checked = models.ManyToManyField(Dosen, null=True, blank=True)